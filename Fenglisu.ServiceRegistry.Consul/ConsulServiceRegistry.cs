﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Services.Registry;
using Consul;

namespace Aspire.ServiceRegistry.Consul
{
    public class ConsulServiceRegistry : IServiceRegistry
    {
        private readonly IConsulClient _consul;

        public ConsulServiceRegistry(IConsulClient client)
        {
            _consul = client;
        }

        public async Task<ServiceInformation> Register(string serviceName, string serviceHost, int servicePort, string healthCheckUrl = null)
        {
            var uriString = $"http://{serviceHost}:{servicePort}";
            var serviceId = GetServiceId(serviceName, serviceHost, servicePort);

            var registry = new AgentServiceRegistration
            {
                ID = serviceId,
                Name = serviceName,
                Address = serviceHost,
                Port = servicePort,
                Check = new AgentServiceCheck
                {
                    HTTP = healthCheckUrl ?? uriString.TrimEnd('/') + "/healthcheck",
                    Interval = TimeSpan.FromSeconds(5)
                }
            };
            await _consul.Agent.ServiceRegister(registry);

            return new ServiceInformation
            {
                Id = registry.ID,
                Name = registry.Name,
                Address = registry.Address,
                Port = registry.Port
            };
        }

        public async Task Deregister(string serviceId)
        {
            await _consul.Agent.ServiceDeregister(serviceId);
        }

        public async Task<IList<ServiceInformation>> FindServices(string name)
        {
            var result = await _consul.Health.Service(name, "", true);
            return result.Response.Select(i => new ServiceInformation
            {
                Id = i.Service.ID,
                Name = i.Service.Service,
                Address = i.Service.Address,
                Port = i.Service.Port
            }).ToList();
        }

        private string GetServiceId(string serviceName, string serviceHost, int servicePort)
        {
            return $"{serviceName}_{serviceHost.Replace('.', '_')}_{servicePort}";
        }
    }
}
