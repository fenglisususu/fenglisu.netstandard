﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using Core.Extensions;
using Core.Services.Registry;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Server.Features;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Aspire.ServiceRegistry.Consul
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseConsulServiceRegistry(this IApplicationBuilder app)
        {
            var options = app.ApplicationServices.GetRequiredService<IOptions<ServiceRegistryOptions>>().Value;
            List<Uri> addresses;

            if (options.ServiceEndpoints != null && options.ServiceEndpoints.Length > 0)
            {
                addresses = options.ServiceEndpoints.Select(i => new Uri(i)).ToList();
            }
            else
            {
                // 如果取服务器监听地址的话有可能会出现http://*:80这样无法解析的Uri，现暂时在配置文件中固定写一个端口号，将来再研究更加灵活的服务注册方式
                //FeatureCollection features = (FeatureCollection)app.Properties["server.Features"];
                //addresses = features.Get<IServerAddressesFeature>().Addresses.Select(i => new Uri(i));
                addresses = NetworkHelper.GetLocalAddresses().Select(i => new Uri($"http://{i}:{options.ServicePort}")).ToList();
            }

            var serviceRegistry = app.ApplicationServices.GetRequiredService<IServiceRegistry>();
            var applicationLifetime = app.ApplicationServices.GetRequiredService<IApplicationLifetime>();
            foreach (var address in addresses)
            {
                var serviceInformation = serviceRegistry.Register(options.ServiceName, address.Host, address.Port, options.HealthCheckUrl).Result;
                applicationLifetime.ApplicationStopped.Register(() =>
                {
                    serviceRegistry.Deregister(serviceInformation.Id);
                });
            }

            return app;
        }
    }
}
