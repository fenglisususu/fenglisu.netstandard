﻿using System;
using Aspire.Consul;
using Core.Services.Registry;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Aspire.ServiceRegistry.Consul
{
    public static class ConsulBuilderExtensions
    {
        public static ConsulBuilder AddConsulServiceRegistry(this ConsulBuilder builder, Action<ServiceRegistryOptions> options)
        {
            builder.Services.Configure(options);
            builder.Services.AddSingleton<IServiceRegistry, ConsulServiceRegistry>();

            return builder;
        }

        public static ConsulBuilder AddConsulServiceRegistry(this ConsulBuilder builder, IConfiguration configuration)
        {
            builder.Services.Configure<ServiceRegistryOptions>(configuration);
            builder.Services.AddSingleton<IServiceRegistry, ConsulServiceRegistry>();

            return builder;
        }
    }
}
