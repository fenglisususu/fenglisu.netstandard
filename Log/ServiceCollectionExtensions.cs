﻿using System;
using Core.Logging;
using Microsoft.Extensions.DependencyInjection;

namespace Aspire.Log4Net
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddLog4NetLogger(this IServiceCollection services,
            Action<Log4NetOptions> options)
        {
            if (options == null)
                throw new ArgumentNullException(nameof(options));

            services.Configure(options);
            services.AddSingleton<ILogger, Log4Logger>();

            return services;
        }
    }
}
