﻿using System;
using System.IO;
using System.Reflection;
using Core.Logging;
using log4net;
using log4net.Repository;
using Microsoft.Extensions.Options;

namespace Aspire.Log4Net
{
    /// <summary>
    /// log4net日志实现类
    /// </summary>
    public class Log4Logger : ILogger
    {
        private static readonly ILoggerRepository LoggerRepository;

        static Log4Logger()
        {
            LoggerRepository = LogManager.CreateRepository("Aspire");
        }

        public Log4Logger(IOptions<Log4NetOptions> optionsAccessor)
        {
            var options = optionsAccessor.Value;

            //加载配置文件
            log4net.Config.XmlConfigurator.ConfigureAndWatch(LoggerRepository, new FileInfo(options.ConfigPath));

            GlobalContext.Properties["log4net:AppTag"] = "Aspire.SYS";

            //实例化日志
            Log = LogManager.GetLogger(LoggerRepository.Name, MethodBase.GetCurrentMethod().DeclaringType);
        }

        /// <summary>
        /// 写入调试日志
        /// </summary>
        /// <param name="message">日志信息</param>
        public void Debug(object message)
        {
            Log.Debug(message);
        }

        /// <summary>
        /// 写入调试日志
        /// </summary>
        /// <param name="message">日志信息</param>
        /// <param name="exception">异常</param>
        public void Debug(object message, Exception exception)
        {
            Log.Debug(message, exception);
        }

        /// <summary>
        /// 写入信息日志
        /// </summary>
        /// <param name="message">日志信息</param>
        public void Info(object message)
        {
            Log.Info(message);
        }

        /// <summary>
        /// 写入信息日志
        /// </summary>
        /// <param name="message">日志信息</param>
        /// <param name="exception">异常</param>
        public void Info(object message, Exception exception)
        {
            Log.Info(message, exception);
        }

        /// <summary>
        /// 写入警告日志
        /// </summary>
        /// <param name="message">日志信息</param>
        public void Warn(object message)
        {
            Log.Warn(message);
        }

        /// <summary>
        /// 写入警告日志
        /// </summary>
        /// <param name="message">日志信息</param>
        /// <param name="exception">异常</param>
        public void Warn(object message, Exception exception)
        {
            Log.Warn(message, exception);
        }

        /// <summary>
        /// 写入错误日志
        /// </summary>
        /// <param name="message">日志信息</param>
        public void Error(object message)
        {
            Log.Error(message);
        }

        /// <summary>
        /// 写入错误日志
        /// </summary>
        /// <param name="message">日志信息</param>
        /// <param name="exception">异常</param>
        public void Error(object message, Exception exception)
        {
            Log.Error(message, exception);
        }

        /// <summary>
        /// 写入致命错误日志
        /// </summary>
        /// <param name="message">日志信息</param>
        public void Fatal(object message)
        {
            Log.Fatal(message);
        }

        /// <summary>
        /// 写入致命错误日志
        /// </summary>
        /// <param name="message">日志信息</param>
        /// <param name="exception">异常</param>
        public void Fatal(object message, Exception exception)
        {
            Log.Fatal(message, exception);
        }

        #region ILogger 成员

        private ILog Log { get; set; }

        private Type _type;
        /// <summary>
        /// 
        /// </summary>
        public Type Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
                Log = LogManager.GetLogger(_type);
            }
        }

        #endregion
    }
}
