﻿namespace Aspire.Log4Net
{
    public class Log4NetOptions
    {
        public string ConfigPath { get; set; } = "log4net.config";
    }
}
