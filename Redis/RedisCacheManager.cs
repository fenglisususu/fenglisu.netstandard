﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Caching;
using Core.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RedLockNet.SERedis;
using RedLockNet.SERedis.Configuration;
using StackExchange.Redis;

namespace Aspire.RedisCache
{
    /// <summary>
    /// Redis缓存
    /// </summary>
    public class RedisCacheManager : ICacheManager
    {
        private readonly ILogger _logger;
        public readonly ConnectionMultiplexer Connection;
        public readonly IDatabase Database;
        private RedLockFactory _redLockFactory;

        public RedisCacheManager(ILogger logger, IOptions<RedisOptions> optionsAccessor)
        {
            _logger = logger;

            var options = optionsAccessor.Value;
            if (options.Endpoints == null || options.Endpoints.Length == 0)
                throw new ArgumentNullException(nameof(options.Endpoints));

            var configurationOptions = new ConfigurationOptions
            {
                AllowAdmin = options.AllowAdmin,
                AbortOnConnectFail = options.AbortOnConnectFail,
                ConnectTimeout = options.ConnectTimeout,
                SyncTimeout = options.SyncTimeout,
                DefaultDatabase = options.Db
            };
            foreach (var endpoint in options.Endpoints)
            {
                configurationOptions.EndPoints.Add(endpoint);
            }

            Connection = ConnectionMultiplexer.Connect(configurationOptions);
            Connection.IncludeDetailInExceptions = true;
            Connection.ErrorMessage += ConnectionOnErrorMessage;
            Connection.InternalError += ConnectionOnInternalError;

            Database = Connection.GetDatabase(options.Db);

            var multiplexers = new List<RedLockMultiplexer> { Connection };
            _redLockFactory = RedLockFactory.Create(multiplexers);
            
        }

        private void ConnectionOnInternalError(object sender, InternalErrorEventArgs e)
        {
            _logger.Error(e.Exception.Message, e.Exception);
        }

        private void ConnectionOnErrorMessage(object sender, RedisErrorEventArgs e)
        {
            _logger.Error(e.EndPoint.AddressFamily + e.Message);
        }

        public T Get<T>(string key)
        {
            var value = Database.StringGet(key);
            try
            {
                if (value.HasValue)
                    return JsonConvert.DeserializeObject<T>(value);
                return default(T);
            }
            catch (Exception exception)
            {
                _logger.Debug(exception.Message, exception);
                return default(T);
            }
        }

        public T Get<T>(string key, Func<T> func)
        {
            try
            {
                T value;
                var obj = Database.StringGet(key);
                if (obj.IsNull)
                {
                    if (func != null)
                    {
                        value = func.Invoke();
                        Database.StringSet(key, JsonConvert.SerializeObject(value));
                        return value;
                    }
                    return default(T);
                }
                value = JsonConvert.DeserializeObject<T>(obj);
                return value;
            }
            catch (Exception exception)
            {
                _logger.Error(exception.Message, exception);
                return default(T);
            }
        }

        public void Set(string key, object value, int minutes = 0)
        {
            var serializedValue = JsonConvert.SerializeObject(value);
            TimeSpan? timeSpan = null;
            if (minutes > 0)
            {
                timeSpan = TimeSpan.FromMinutes(minutes);
            }
            Database.StringSet(key, serializedValue, timeSpan);
        }

        public bool IsSet(string key)
        {
            return Database.KeyExists(key);
        }

        public List<T> GetByPattern<T>(string pattern)
        {
            var list = new List<T>();

            var endPoints = Connection.GetEndPoints();
            if (endPoints.Length > 0)
            {

                var server = Connection.GetServer(endPoints[0]);
                var keys = server.Keys(pattern: string.Concat('*', pattern, '*'), pageSize: 100);

                foreach (var redisKey in keys)
                {
                    var t = Get<T>(redisKey);
                    list.Add(t);
                }
            }

            return list;
        }

        public void Remove(string key)
        {
            if (key != null)
                Database.KeyDelete(key);
        }

        public void RemoveByPattern(string pattern)
        {
            var endPoints = Connection.GetEndPoints();
            if (endPoints.Length > 0)
            {
                var server = Connection.GetServer(endPoints[0]);
                var keys = server.Keys(pattern: string.Concat('*', pattern, '*'), pageSize: 100);
                Database.KeyDelete(keys.ToArray());
            }
        }

        public void HashSet(string key, string hashkey, object value)
        {
            var serializedValue = JsonConvert.SerializeObject(value);
            Database.HashSet(key, hashkey, serializedValue);
        }

        public void HashSet<T>(string key, List<T> values) where T : Iid
        {
            var list = new List<HashEntry>();
            foreach (var value in values)
            {
                HashEntry e = new HashEntry(value.Id.ToString(), JsonConvert.SerializeObject(value));
                list.Add(e);
            }
            Database.HashSet(key, list.ToArray());
        }

        public void HashRemove(string key, string hashkey)
        {
            Database.HashDelete(key, hashkey);
        }

        public void HashRemove(string key, List<string> hashkeys)
        {
            var hashs = hashkeys.Select(s => (RedisValue)s).ToArray();
            Database.HashDelete(key, hashs);
        }

        public T HashGet<T>(string key, string hashkey)
        {
            if (string.IsNullOrWhiteSpace(hashkey)) return default(T);

            var value = Database.HashGet(key, hashkey);
            try
            {
                if (value.HasValue)
                    return JsonConvert.DeserializeObject<T>(value);
                return default(T);
            }
            catch (Exception exception)
            {
                _logger.Debug(exception.Message, exception);
                return default(T);
            }
        }
        public List<T> HashGet<T>(string key, List<string> hashkeys)
        {
            var result = new List<T>();
            var values = Database.HashGetAll(key);
            try
            {
                foreach (var hashEntry in values)
                {
                    if (hashEntry.Value.HasValue)
                        if (hashkeys.Contains(hashEntry.Name))
                            result.Add(JsonConvert.DeserializeObject<T>(hashEntry.Value));
                }
                return result;
            }
            catch (Exception exception)
            {
                _logger.Debug(exception.Message, exception);
                return new List<T>();
            }
        }
        public List<T> HashGet<T>(string key)
        {
            var result = new List<T>();
            var values = Database.HashGetAll(key);
            try
            {
                foreach (var hashEntry in values)
                {
                    if (hashEntry.Value.HasValue)
                        result.Add(JsonConvert.DeserializeObject<T>(hashEntry.Value));
                }
                return result;
            }
            catch (Exception exception)
            {
                _logger.Debug(exception.Message, exception);
                return new List<T>();
            }
        }

        public bool HashExists(string key, string hashkey)
        {
            return Database.HashExists(key, hashkey);
        }

        public long Increment(string key, int value = 1)
        {
            return Database.StringIncrement(key, value);
        }

        public void Lock(string resource, TimeSpan expiry, Action action)
        {
            var wait = TimeSpan.FromMilliseconds(50);
            var retry = TimeSpan.FromSeconds(2);
            using (var redLock = _redLockFactory.CreateLock(resource, expiry, wait, retry))
            {
                if (redLock.IsAcquired)
                {
                    action();
                }
            }
        }

        public string StringGet(string key)
        {
            var value = Database.StringGet(key);
            return value;
        }

        public void StringSet(string key, string data, int minutes = 0)
        {
            TimeSpan? timeSpan = null;
            if (minutes > 0)
            {
                timeSpan = TimeSpan.FromMinutes(minutes);
            }
            Database.StringSet(key, data, timeSpan);
        }
    }
}
