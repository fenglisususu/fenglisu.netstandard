﻿using Core.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Aspire.EFRepository
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddEFRepository<T>(this IServiceCollection services) where T : DbContext
        {
            services.AddTransient(typeof(IRepository<>), typeof(EntityFrameworkRepository<>));

            var contextType = typeof(EntityFrameworkRepositoryContext<>).MakeGenericType(typeof(T));
            services.AddScoped(typeof(IRepositoryContext), contextType);

            return services;
        }
    }
}
