﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Core.Repositories;
using Microsoft.EntityFrameworkCore;
using SortOrder = Core.Repositories.SortOrder;


namespace Aspire.EFRepository
{
    /// <summary>
    /// EF仓储实现类
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class EntityFrameworkRepository<TEntity> : Repository<TEntity> where TEntity : class, IEntity
    {
        private readonly IEntityFrameworkRepositoryContext _efContext;

        private DbSet<TEntity> _dbSet;

        private static readonly Expression<Func<TEntity, DateTime?>> DefaultSortPredicate = i => i.CreationTime;
        private const string DefaultSortField = "CreationTime";
        private const SortOrder DefaultSortOrder = SortOrder.Descending;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public EntityFrameworkRepository(IRepositoryContext context)
            : base(context)
        {
            _efContext = context as IEntityFrameworkRepositoryContext;
        }

        /// <summary>
        /// 数据库上下文
        /// </summary>
        protected IEntityFrameworkRepositoryContext EFContext
        {
            get
            {
                return _efContext;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        protected DbContext DbContext
        {
            get { return _efContext.Context; }
        }
        /// <summary>
        /// 
        /// </summary>
        public override IRepositoryContext Context
        {
            get { return _efContext; }
        }

        /// <summary>
        /// dbset
        /// </summary>
        protected virtual DbSet<TEntity> DbSet
        {
            get
            {
                return _dbSet ?? (_dbSet = _efContext.Context.Set<TEntity>());

            }
        }



        #region Private Methods

        private MemberExpression GetMemberInfo(LambdaExpression lambda)
        {
            if (lambda == null)
                throw new ArgumentNullException("lambda");

            MemberExpression memberExpr = null;

            if (lambda.Body.NodeType == ExpressionType.Convert)
            {
                memberExpr =
                    ((UnaryExpression)lambda.Body).Operand as MemberExpression;
            }
            else if (lambda.Body.NodeType == ExpressionType.MemberAccess)
            {
                memberExpr = lambda.Body as MemberExpression;
            }

            if (memberExpr == null)
                throw new ArgumentException("method");

            return memberExpr;
        }

        private string GetEagerLoadingPath(Expression<Func<TEntity, dynamic>> eagerLoadingProperty)
        {
            MemberExpression memberExpression = this.GetMemberInfo(eagerLoadingProperty);
            var parameterName = eagerLoadingProperty.Parameters.First().Name;
            var memberExpressionStr = memberExpression.ToString();
            var path = memberExpressionStr.Replace(parameterName + ".", "");
            return path;
        }
        private void CheckPaginationParams(int pageNumber, int pageSize)
        {
            if (pageNumber <= 0) throw new ArgumentOutOfRangeException("pageNumber", pageNumber, "当前页码必须大于0");
            if (pageSize <= 0) throw new ArgumentOutOfRangeException("pageSize", pageSize, "每页记录数量必须大于0");
        }

        private void CheckLimitParams(int offset, int limit)
        {
            if (offset < 0) throw new ArgumentOutOfRangeException("offset", offset, "偏移量必须大于或等于0");
            if (limit <= 0) throw new ArgumentOutOfRangeException("limit", limit, "记录数量必须大于0");
        }

        private IQueryable<TEntity> GetIncludedQuery(params string[] eagerLoadingPaths)
        {
            if (eagerLoadingPaths != null &&
                eagerLoadingPaths.Length > 0)
            {
                var eagerLoadingPath = eagerLoadingPaths[0];
                var dbQuery = DbSet.Include(eagerLoadingPath);
                for (int i = 1; i < eagerLoadingPaths.Length; i++)
                {
                    eagerLoadingPath = eagerLoadingPaths[i];
                    dbQuery = dbQuery.Include(eagerLoadingPath);
                }
                return dbQuery;
            }

            return DbSet;
        }
        #endregion

        protected override IEnumerable<TEntity> DoFindAll<TKey>(Expression<Func<TEntity, bool>> queryPredicate, Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder)
        {
            var query = queryPredicate != null ? DbSet.Where(queryPredicate) : DbSet;
            switch (sortOrder)
            {
                case SortOrder.Ascending:
                    query = sortPredicate == null ? query.OrderBy(DefaultSortPredicate) : query.OrderBy(sortPredicate);
                    break;
                case SortOrder.Descending:
                    query = sortPredicate == null ? query.OrderByDescending(DefaultSortPredicate) : query.OrderByDescending(sortPredicate);
                    break;
            }
            return query.AsNoTracking().ToList();
        }

        protected override IEnumerable<TEntity> DoFindAll(Expression<Func<TEntity, bool>> queryPredicate, string sortField, string sortOrder)
        {
            var query = queryPredicate != null ? DbSet.Where(queryPredicate) : DbSet;
            if (string.IsNullOrWhiteSpace(sortField))
                sortField = DefaultSortField;

            switch (sortOrder.ToSortOrder())
            {
                case SortOrder.Ascending:
                    query = query.OrderBy(u => GetPropertyValue(u, sortField));
                    break;
                case SortOrder.Descending:
                    query = query.OrderByDescending(u => GetPropertyValue(u, sortField));
                    break;
            }

            //query = query.OrderBy(u => GetPropertyValue(u, sortField));

            return query.AsNoTracking().ToList();
        }
        private static object GetPropertyValue(object obj, string property)
        {
            System.Reflection.PropertyInfo propertyInfo = obj.GetType().GetProperty(property);
            return propertyInfo.GetValue(obj, null);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        protected override PagedResult<TEntity> DoFindAll<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, int pageNumber,
            int pageSize)
        {
            CheckPaginationParams(pageNumber, pageSize);

            var results = DoFindAllByLimit(queryPredicate, sortPredicate, sortOrder, (pageNumber - 1) * pageSize, pageSize);
            var recordCount = DoGetCount(queryPredicate);

            return new PagedResult<TEntity>(pageNumber, pageSize, recordCount, results);
        }

        protected override PagedResult<TResult> DoFindAll<TKey, TResult>(Expression<Func<TEntity, bool>> queryPredicate, Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            Expression<Func<TEntity, TResult>> selector, int pageNumber, int pageSize)
        {
            CheckPaginationParams(pageNumber, pageSize);

            var results = DoFindAllByLimit(queryPredicate, sortPredicate, sortOrder, selector, (pageNumber - 1) * pageSize, pageSize);
            var recordCount = DoGetCount(queryPredicate);

            return new PagedResult<TResult>(pageNumber, pageSize, recordCount, results);
        }

        protected override PagedResult<TEntity> DoFindAll(Expression<Func<TEntity, bool>> queryPredicate, string sortField, string sortOrder, int pageNumber,
            int pageSize)
        {
            CheckPaginationParams(pageNumber, pageSize);

            var results = DoFindAllByLimit(queryPredicate, sortField, sortOrder, (pageNumber - 1) * pageSize, pageSize);
            var recordCount = DoGetCount(queryPredicate);

            return new PagedResult<TEntity>(pageNumber, pageSize, recordCount, results);
        }

        protected override PagedResult<TResult> DoFindAll<TResult>(Expression<Func<TEntity, bool>> queryPredicate, string sortField, string sortOrder, Expression<Func<TEntity, TResult>> selector, int pageNumber, int pageSize)
        {
            CheckPaginationParams(pageNumber, pageSize);

            var results = DoFindAllByLimit(queryPredicate, sortField, sortOrder, selector, (pageNumber - 1) * pageSize, pageSize);
            var recordCount = DoGetCount(queryPredicate);

            return new PagedResult<TResult>(pageNumber, pageSize, recordCount, results);
        }

        protected override IEnumerable<TEntity> DoFindAllByLimit<TKey>(Expression<Func<TEntity, bool>> queryPredicate, Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, int offset,
            int limit)
        {
            CheckLimitParams(offset, limit);

            var query = queryPredicate != null ? DbSet.Where(queryPredicate) : DbSet;

            if (sortOrder == SortOrder.Unspecified) sortOrder = DefaultSortOrder;

            switch (sortOrder)
            {
                case SortOrder.Ascending:
                    query = sortPredicate == null ? query.OrderBy(DefaultSortPredicate) : query.OrderBy(sortPredicate);
                    break;
                case SortOrder.Descending:
                    query = sortPredicate == null ? query.OrderByDescending(DefaultSortPredicate) : query.OrderByDescending(sortPredicate);
                    break;
            }
            return query.Skip(offset).Take(limit).AsNoTracking().ToList();
        }

        protected override IEnumerable<TResult> DoFindAllByLimit<TKey, TResult>(Expression<Func<TEntity, bool>> queryPredicate, Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            Expression<Func<TEntity, TResult>> selector, int offset, int limit)
        {
            CheckLimitParams(offset, limit);

            var query = queryPredicate != null ? DbSet.Where(queryPredicate) : DbSet;

            if (sortOrder == SortOrder.Unspecified) sortOrder = DefaultSortOrder;

            switch (sortOrder)
            {
                case SortOrder.Ascending:
                    query = sortPredicate == null ? query.OrderBy(DefaultSortPredicate) : query.OrderBy(sortPredicate);
                    break;
                case SortOrder.Descending:
                    query = sortPredicate == null ? query.OrderByDescending(DefaultSortPredicate) : query.OrderByDescending(sortPredicate);
                    break;
            }
            return query.Skip(offset).Take(limit).AsNoTracking().Select(selector).ToList();
        }

        protected override IEnumerable<TEntity> DoFindAllByLimit(Expression<Func<TEntity, bool>> queryPredicate, string sortField, string sortOrder, int offset,
            int limit)
        {
            CheckLimitParams(offset, limit);

            var query = queryPredicate != null ? DbSet.Where(queryPredicate) : DbSet;

            if (string.IsNullOrWhiteSpace(sortField))
                sortField = DefaultSortField;
            //query = query.OrderByExt(sortField );
            switch (sortOrder.ToSortOrder())
            {
                case SortOrder.Ascending:
                    query = query.OrderByExt(sortField);
                    break;
                case SortOrder.Descending:
                    query = query.OrderByDescendingExt(sortField);
                    break;
            }
            //query = query.OrderBy(string.Concat(sortField, " ", sortOrder));

            return query.Skip(offset).Take(limit).AsNoTracking().ToList();
        }

        protected IEnumerable<TResult> DoFindAllByLimit<TResult>(Expression<Func<TEntity, bool>> queryPredicate, string sortField, string sortOrder, Expression<Func<TEntity, TResult>> selector, int offset, int limit)
        {
            CheckLimitParams(offset, limit);

            var query = queryPredicate != null ? DbSet.Where(queryPredicate) : DbSet;

            if (string.IsNullOrWhiteSpace(sortField))
                sortField = DefaultSortField;
            switch (sortOrder.ToSortOrder())
            {
                case SortOrder.Ascending:
                    query = query.OrderByExt(sortField);
                    break;
                case SortOrder.Descending:
                    query = query.OrderByDescendingExt(sortField);
                    break;
            }

            return query.Skip(offset).Take(limit).AsNoTracking().Select(selector).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        protected override IEnumerable<TEntity> DoFindAll<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            string[] eagerLoadingPaths = null;
            if (eagerLoadingProperties != null && eagerLoadingProperties.Length > 0)
            {
                eagerLoadingPaths = eagerLoadingProperties.Select(i => GetEagerLoadingPath(i)).ToArray();
            }
            return DoFindAll(queryPredicate, sortPredicate, sortOrder, eagerLoadingPaths);
        }

        protected override IEnumerable<TEntity> DoFindAll(Expression<Func<TEntity, bool>> queryPredicate, string sortField, string sortOrder,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            string[] eagerLoadingPaths = null;
            if (eagerLoadingProperties != null && eagerLoadingProperties.Length > 0)
            {
                eagerLoadingPaths = eagerLoadingProperties.Select(i => GetEagerLoadingPath(i)).ToArray();
            }
            return DoFindAll(queryPredicate, sortField, sortOrder, eagerLoadingPaths);
        }

        protected override IEnumerable<TEntity> DoFindAll<TKey>(Expression<Func<TEntity, bool>> queryPredicate, Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            params string[] eagerLoadingPaths)
        {
            var dbQuery = GetIncludedQuery(eagerLoadingPaths);
            IQueryable<TEntity> queryable = queryPredicate != null ? dbQuery.Where(queryPredicate) : dbQuery;

            switch (sortOrder)
            {
                case SortOrder.Ascending:
                    queryable = sortPredicate == null ? queryable.OrderBy(DefaultSortPredicate) : queryable.OrderBy(sortPredicate);
                    break;
                case SortOrder.Descending:
                    queryable = sortPredicate == null ? queryable.OrderByDescending(DefaultSortPredicate) : queryable.OrderByDescending(sortPredicate);
                    break;
            }
            return queryable.AsNoTracking().ToList();
        }

        protected override IEnumerable<TEntity> DoFindAll(Expression<Func<TEntity, bool>> queryPredicate, string sortField, string sortOrder,
            params string[] eagerLoadingPaths)
        {
            var dbQuery = GetIncludedQuery(eagerLoadingPaths);
            IQueryable<TEntity> queryable = queryPredicate != null ? dbQuery.Where(queryPredicate) : dbQuery;

            //queryable = queryable.OrderBy(string.Concat(sortField, " ", sortOrder));


            switch (sortOrder.ToSortOrder())
            {
                case SortOrder.Ascending:
                    queryable = queryable.OrderByExt(sortField);
                    break;
                case SortOrder.Descending:
                    queryable = queryable.OrderByDescendingExt(sortField);
                    break;
            }

            return queryable.AsNoTracking().ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        protected override PagedResult<TEntity> DoFindAll<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, int pageNumber,
            int pageSize, params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            CheckPaginationParams(pageNumber, pageSize);

            var results = DoFindAllByLimit(queryPredicate, sortPredicate, sortOrder, (pageNumber - 1) * pageSize, pageSize,
                eagerLoadingProperties);
            var recordCount = DoGetCount(queryPredicate);

            return new PagedResult<TEntity>(pageNumber, pageSize, recordCount, results);
        }

        protected override PagedResult<TEntity> DoFindAll(Expression<Func<TEntity, bool>> queryPredicate, string sortField, string sortOrder, int pageNumber,
            int pageSize, params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            CheckPaginationParams(pageNumber, pageSize);

            var results = DoFindAllByLimit(queryPredicate, sortField, sortOrder, (pageNumber - 1) * pageSize, pageSize,
                eagerLoadingProperties);
            var recordCount = DoGetCount(queryPredicate);

            return new PagedResult<TEntity>(pageNumber, pageSize, recordCount, results);
        }

        protected override PagedResult<TEntity> DoFindAll(Expression<Func<TEntity, bool>> queryPredicate, string sortString, int pageNumber, int pageSize,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            CheckPaginationParams(pageNumber, pageSize);

            var results = DoFindAllByLimit(queryPredicate, sortString, (pageNumber - 1) * pageSize, pageSize,
                eagerLoadingProperties);
            var recordCount = DoGetCount(queryPredicate);

            return new PagedResult<TEntity>(pageNumber, pageSize, recordCount, results);
        }

        protected override PagedResult<TEntity> DoFindAll<TKey>(Expression<Func<TEntity, bool>> queryPredicate, Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, int pageNumber,
            int pageSize, params string[] eagerLoadingPaths)
        {
            CheckPaginationParams(pageNumber, pageSize);

            var results = DoFindAllByLimit(queryPredicate, sortPredicate, sortOrder, (pageNumber - 1) * pageSize, pageSize,
                eagerLoadingPaths);
            var recordCount = DoGetCount(queryPredicate);

            return new PagedResult<TEntity>(pageNumber, pageSize, recordCount, results);
        }

        protected override PagedResult<TEntity> DoFindAll<TKey1, TKey2>(Expression<Func<TEntity, bool>> queryPredicate, Expression<Func<TEntity, TKey1>> sortPredicate, SortOrder sortOrder,
            Expression<Func<TEntity, TKey2>> thenBySortPredicate, SortOrder thenBySortOrder, int pageNumber, int pageSize,
            params string[] eagerLoadingPaths)
        {
            CheckPaginationParams(pageNumber, pageSize);

            var results = DoFindAllByLimit(queryPredicate, sortPredicate, sortOrder, thenBySortPredicate, thenBySortOrder, (pageNumber - 1) * pageSize, pageSize,
                eagerLoadingPaths);
            var recordCount = DoGetCount(queryPredicate);

            return new PagedResult<TEntity>(pageNumber, pageSize, recordCount, results);
        }

        protected override PagedResult<TEntity> DoFindAll(Expression<Func<TEntity, bool>> queryPredicate, string sortField, string sortOrder, int pageNumber,
            int pageSize, params string[] eagerLoadingPaths)
        {
            CheckPaginationParams(pageNumber, pageSize);

            var results = DoFindAllByLimit(queryPredicate, sortField, sortOrder, (pageNumber - 1) * pageSize, pageSize,
                eagerLoadingPaths);
            var recordCount = DoGetCount(queryPredicate);

            return new PagedResult<TEntity>(pageNumber, pageSize, recordCount, results);
        }

        protected override IEnumerable<TEntity> DoFindAllByLimit<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, int offset,
            int limit, params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            string[] eagerLoadingPaths = null;
            if (eagerLoadingProperties != null && eagerLoadingProperties.Length > 0)
            {
                eagerLoadingPaths = eagerLoadingProperties.Select(i => GetEagerLoadingPath(i)).ToArray();
            }

            return DoFindAllByLimit(queryPredicate, sortPredicate, sortOrder, offset, limit, eagerLoadingPaths);
        }

        protected override IEnumerable<TEntity> DoFindAllByLimit(Expression<Func<TEntity, bool>> queryPredicate, string sortField, string sortOrder, int offset,
            int limit, params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            string[] eagerLoadingPaths = null;
            if (eagerLoadingProperties != null && eagerLoadingProperties.Length > 0)
            {
                eagerLoadingPaths = eagerLoadingProperties.Select(i => GetEagerLoadingPath(i)).ToArray();
            }

            return DoFindAllByLimit(queryPredicate, sortField, sortOrder, offset, limit, eagerLoadingPaths);
        }

        protected override IEnumerable<TEntity> DoFindAllByLimit(Expression<Func<TEntity, bool>> queryPredicate, string sortString, int offset,
            int limit, params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            string[] eagerLoadingPaths = null;
            if (eagerLoadingProperties != null && eagerLoadingProperties.Length > 0)
            {
                eagerLoadingPaths = eagerLoadingProperties.Select(i => GetEagerLoadingPath(i)).ToArray();
            }

            return DoFindAllByLimit(queryPredicate, sortString, offset, limit, eagerLoadingPaths);
        }

        protected override IEnumerable<TEntity> DoFindAllByLimit<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, int offset,
            int limit, params string[] eagerLoadingPaths)
        {
            CheckLimitParams(offset, limit);

            var dbQuery = GetIncludedQuery(eagerLoadingPaths);
            IQueryable<TEntity> queryable = queryPredicate != null ? dbQuery.Where(queryPredicate) : dbQuery;

            if (queryPredicate != null)
                queryable = queryable.Where(queryPredicate);

            if (sortOrder == SortOrder.Unspecified) sortOrder = DefaultSortOrder;

            switch (sortOrder)
            {
                case SortOrder.Ascending:
                    queryable = sortPredicate == null ? queryable.OrderBy(DefaultSortPredicate) : queryable.OrderBy(sortPredicate);
                    break;
                case SortOrder.Descending:
                    queryable = sortPredicate == null ? queryable.OrderByDescending(DefaultSortPredicate) : queryable.OrderByDescending(sortPredicate);
                    break;
            }

            return queryable.Skip(offset).Take(limit).AsNoTracking().ToList();
        }

        protected override IEnumerable<TEntity> DoFindAllByLimit<TKey1, TKey2>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey1>> sortPredicate, SortOrder sortOrder, Expression<Func<TEntity, TKey2>> thenBySortPredicate, SortOrder thenBySortOrder, int offset,
            int limit, params string[] eagerLoadingPaths)
        {
            CheckLimitParams(offset, limit);

            var dbQuery = GetIncludedQuery(eagerLoadingPaths);
            IQueryable<TEntity> queryable = queryPredicate != null ? dbQuery.Where(queryPredicate) : dbQuery;

            if (sortOrder == SortOrder.Unspecified) sortOrder = DefaultSortOrder;

            IOrderedQueryable<TEntity> orderedQueryable;
            switch (sortOrder)
            {
                default:
                    orderedQueryable = sortPredicate == null ? queryable.OrderBy(DefaultSortPredicate) : queryable.OrderBy(sortPredicate);
                    break;
                case SortOrder.Descending:
                    orderedQueryable = sortPredicate == null ? queryable.OrderByDescending(DefaultSortPredicate) : queryable.OrderByDescending(sortPredicate);
                    break;
            }

            if (thenBySortOrder == SortOrder.Unspecified) thenBySortOrder = DefaultSortOrder;

            switch (thenBySortOrder)
            {
                case SortOrder.Ascending:
                    orderedQueryable = thenBySortPredicate == null
                        ? orderedQueryable.ThenBy(DefaultSortPredicate)
                        : orderedQueryable.ThenBy(thenBySortPredicate);
                    break;
                case SortOrder.Descending:
                    orderedQueryable = thenBySortPredicate == null
                        ? orderedQueryable.ThenByDescending(DefaultSortPredicate)
                        : orderedQueryable.ThenByDescending(thenBySortPredicate);
                    break;
            }

            queryable = orderedQueryable;

            return queryable.Skip(offset).Take(limit).AsNoTracking().ToList();
        }

        protected override IEnumerable<TEntity> DoFindAllByLimit(Expression<Func<TEntity, bool>> queryPredicate, string sortField, string sortOrder, int offset,
            int limit, params string[] eagerLoadingPaths)
        {
            CheckLimitParams(offset, limit);

            var dbQuery = GetIncludedQuery(eagerLoadingPaths);
            IQueryable<TEntity> queryable = queryPredicate != null ? dbQuery.Where(queryPredicate) : dbQuery;

            if (string.IsNullOrWhiteSpace(sortField))
                sortField = DefaultSortField;
            switch (sortOrder.ToSortOrder())
            {
                case SortOrder.Ascending:
                    queryable = queryable.OrderByExt(sortField);
                    break;
                case SortOrder.Descending:
                    queryable = queryable.OrderByDescendingExt(sortField);
                    break;
            }
            //queryable = queryable.OrderBy(string.Concat(sortField, " ", sortOrder));

            return queryable.Skip(offset).Take(limit).AsNoTracking().ToList();
        }

        protected override IEnumerable<TEntity> DoFindAllByLimit(Expression<Func<TEntity, bool>> queryPredicate, string sortString, int offset,
            int limit, params string[] eagerLoadingPaths)
        {
            CheckLimitParams(offset, limit);

            var dbQuery = GetIncludedQuery(eagerLoadingPaths);
            IQueryable<TEntity> queryable = queryPredicate != null ? dbQuery.Where(queryPredicate) : dbQuery;

            queryable = queryable.OrderByExt(string.IsNullOrWhiteSpace(sortString) ? DefaultSortField : sortString);

            return queryable.Skip(offset).Take(limit).AsNoTracking().ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        protected override TEntity DoFindById(Guid id)
        {
            return DoFindSelect(i => i.Id == id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        protected override int DoGetCount(Expression<Func<TEntity, bool>> queryPredicate)
        {
            if (queryPredicate != null)
                return DbSet.Count(queryPredicate);
            return DbSet.Count();
        }

        #region Find
        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        protected override TEntity DoFindSelect(Expression<Func<TEntity, bool>> queryPredicate)
        {
            if (queryPredicate != null)
                return DbSet.FirstOrDefault(queryPredicate);
            return DbSet.FirstOrDefault();
        }

        protected override TResult DoFindSelect<TResult>(Expression<Func<TEntity, bool>> queryPredicate, Expression<Func<TEntity, TResult>> selector)
        {
            if (queryPredicate != null)
                return DbSet.Where(queryPredicate).Select(selector).FirstOrDefault();
            return DbSet.Select(selector).FirstOrDefault();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        protected override TEntity DoFindSelect(Expression<Func<TEntity, bool>> queryPredicate, params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            string[] eagerLoadingPaths = null;
            if (eagerLoadingProperties != null && eagerLoadingProperties.Length > 0)
            {
                eagerLoadingPaths = eagerLoadingProperties.Select(i => GetEagerLoadingPath(i)).ToArray();
            }
            return DoFindSelect(queryPredicate, eagerLoadingPaths);
        }

        protected override TEntity DoFindSelect(Expression<Func<TEntity, bool>> queryPredicate, params string[] eagerLoadingPaths)
        {
            IQueryable<TEntity> queryable;
            if (eagerLoadingPaths != null &&
                eagerLoadingPaths.Length > 0)
            {
                var eagerLoadingPath = eagerLoadingPaths[0];
                var dbQuery = DbSet.Include(eagerLoadingPath);
                for (int i = 1; i < eagerLoadingPaths.Length; i++)
                {
                    eagerLoadingPath = eagerLoadingPaths[i];
                    dbQuery = dbQuery.Include(eagerLoadingPath);
                }
                queryable = dbQuery.AsQueryable();
            }
            else
                queryable = DbSet.AsQueryable();

            if (queryPredicate != null)
                queryable = queryable.Where(queryPredicate);

            return queryable.FirstOrDefault();
        }

        protected override TResult DoFindSelect<TResult>(Expression<Func<TEntity, bool>> queryPredicate, Expression<Func<TEntity, TResult>> selector, params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            string[] eagerLoadingPaths = null;
            if (eagerLoadingProperties != null && eagerLoadingProperties.Length > 0)
            {
                eagerLoadingPaths = eagerLoadingProperties.Select(i => GetEagerLoadingPath(i)).ToArray();
            }
            IQueryable<TEntity> queryable;
            if (eagerLoadingPaths != null &&
                eagerLoadingPaths.Length > 0)
            {
                var eagerLoadingPath = eagerLoadingPaths[0];
                var dbQuery = DbSet.Include(eagerLoadingPath);
                for (int i = 1; i < eagerLoadingPaths.Length; i++)
                {
                    eagerLoadingPath = eagerLoadingPaths[i];
                    dbQuery = dbQuery.Include(eagerLoadingPath);
                }
                queryable = dbQuery.AsQueryable();
            }
            else
                queryable = DbSet.AsQueryable();

            if (queryPredicate != null)
                queryable = queryable.Where(queryPredicate);

            var resultQuery = queryable.Select(selector);

            return resultQuery.FirstOrDefault();
        }

        protected override TEntity DoFindFirst<TKey>(Expression<Func<TEntity, bool>> queryPredicate, Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            string[] eagerLoadingPaths = null;
            if (eagerLoadingProperties != null && eagerLoadingProperties.Length > 0)
            {
                eagerLoadingPaths = eagerLoadingProperties.Select(i => GetEagerLoadingPath(i)).ToArray();
            }
            IQueryable<TEntity> queryable;
            if (eagerLoadingPaths != null &&
                eagerLoadingPaths.Length > 0)
            {
                var eagerLoadingPath = eagerLoadingPaths[0];
                var dbQuery = DbSet.Include(eagerLoadingPath);
                for (int i = 1; i < eagerLoadingPaths.Length; i++)
                {
                    eagerLoadingPath = eagerLoadingPaths[i];
                    dbQuery = dbQuery.Include(eagerLoadingPath);
                }
                queryable = dbQuery.AsQueryable();
            }
            else
                queryable = DbSet.AsQueryable();

            if (sortOrder == SortOrder.Unspecified) sortOrder = DefaultSortOrder;

            switch (sortOrder)
            {
                case SortOrder.Ascending:
                    queryable = sortPredicate == null ? queryable.OrderBy(DefaultSortPredicate) : queryable.OrderBy(sortPredicate);
                    break;
                case SortOrder.Descending:
                    queryable = sortPredicate == null ? queryable.OrderByDescending(DefaultSortPredicate) : queryable.OrderByDescending(sortPredicate);
                    break;
            }

            if (queryPredicate != null)
                return queryable.FirstOrDefault(queryPredicate);
            return queryable.FirstOrDefault();
        }

        protected override TEntity DoFindLast<TKey>(Expression<Func<TEntity, bool>> queryPredicate, Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            string[] eagerLoadingPaths = null;
            if (eagerLoadingProperties != null && eagerLoadingProperties.Length > 0)
            {
                eagerLoadingPaths = eagerLoadingProperties.Select(i => GetEagerLoadingPath(i)).ToArray();
            }
            IQueryable<TEntity> queryable;
            if (eagerLoadingPaths != null &&
                eagerLoadingPaths.Length > 0)
            {
                var eagerLoadingPath = eagerLoadingPaths[0];
                var dbQuery = DbSet.Include(eagerLoadingPath);
                for (int i = 1; i < eagerLoadingPaths.Length; i++)
                {
                    eagerLoadingPath = eagerLoadingPaths[i];
                    dbQuery = dbQuery.Include(eagerLoadingPath);
                }
                queryable = dbQuery.AsQueryable();
            }
            else
                queryable = DbSet.AsQueryable();

            if (sortOrder == SortOrder.Unspecified) sortOrder = DefaultSortOrder;

            switch (sortOrder)
            {
                case SortOrder.Ascending:
                    queryable = sortPredicate == null ? queryable.OrderByDescending(DefaultSortPredicate) : queryable.OrderByDescending(sortPredicate);
                    break;
                case SortOrder.Descending:
                    queryable = sortPredicate == null ? queryable.OrderBy(DefaultSortPredicate) : queryable.OrderBy(sortPredicate);
                    break;
            }

            if (queryPredicate != null)
                return queryable.FirstOrDefault(queryPredicate);
            return queryable.FirstOrDefault();
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        protected override bool DoExists(Expression<Func<TEntity, bool>> queryPredicate)
        {
            if (queryPredicate != null)
                return DbSet.Any(queryPredicate);
            return DbSet.Any();
        }

        #region Sum
        protected override long DoSum(Expression<Func<TEntity, long>> expression, Expression<Func<TEntity, bool>> queryPredicate = null)
        {
            var query = DbSet.AsQueryable();
            if (queryPredicate != null)
            {
                query = query.Where(queryPredicate);
            }
            return query.Select(expression).DefaultIfEmpty().Sum();
        }

        protected override long? DoSum(Expression<Func<TEntity, long?>> expression, Expression<Func<TEntity, bool>> queryPredicate = null)
        {
            var query = DbSet.AsQueryable();
            if (queryPredicate != null)
            {
                query = query.Where(queryPredicate);
            }
            return query.Sum(expression);
        }

        protected override int DoSum(Expression<Func<TEntity, int>> expression, Expression<Func<TEntity, bool>> queryPredicate = null)
        {
            var query = DbSet.AsQueryable();
            if (queryPredicate != null)
            {
                query = query.Where(queryPredicate);
            }
            return query.Select(expression).DefaultIfEmpty().Sum();
        }

        protected override int? DoSum(Expression<Func<TEntity, int?>> expression, Expression<Func<TEntity, bool>> queryPredicate = null)
        {
            var query = DbSet.AsQueryable();
            if (queryPredicate != null)
            {
                query = query.Where(queryPredicate);
            }
            return query.Sum(expression);
        }

        protected override decimal DoSum(Expression<Func<TEntity, decimal>> expression, Expression<Func<TEntity, bool>> queryPredicate = null)
        {
            var query = DbSet.AsQueryable();
            if (queryPredicate != null)
            {
                query = query.Where(queryPredicate);
            }
            return query.Select(expression).DefaultIfEmpty().Sum();
        }

        protected override decimal? DoSum(Expression<Func<TEntity, decimal?>> expression, Expression<Func<TEntity, bool>> queryPredicate = null)
        {
            var query = DbSet.AsQueryable();
            if (queryPredicate != null)
            {
                query = query.Where(queryPredicate);
            }
            return query.Sum(expression);
        }

        protected override List<T> DoGroup<T>(Func<TEntity, T> expression, Expression<Func<TEntity, bool>> queryPredicate = null)
        {
            var query = DbSet.AsQueryable();
            if (queryPredicate != null)
            {
                query = query.Where(queryPredicate);
            }

            var g = query.GroupBy(expression);
            return g.Select(s => s.Key).ToList();
        }


        #endregion

        protected override PagedResult<Guid> DoFindIds<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, int pageNumber,
            int pageSize)
        {
            CheckPaginationParams(pageNumber, pageSize);

            var query = queryPredicate != null ? DbSet.Where(queryPredicate) : DbSet;
            switch (sortOrder)
            {
                case SortOrder.Ascending:
                    query = sortPredicate == null ? query.OrderBy(DefaultSortPredicate) : query.OrderBy(sortPredicate);
                    break;
                case SortOrder.Descending:
                    query = sortPredicate == null
                        ? query.OrderByDescending(DefaultSortPredicate)
                        : query.OrderByDescending(sortPredicate);
                    break;
            }
            var results = query.Skip((pageNumber - 1) * pageSize).Take(pageSize).Select(i => i.Id).ToArray();
            var recordCount = DoGetCount(queryPredicate);

            return new PagedResult<Guid>(pageNumber, pageSize, recordCount, results);
        }

        #region Insert, Update, Delete
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        protected override bool DoAdd(TEntity entity, bool isCommit = true)
        {
            _efContext.RegisterNew(entity);
            return !isCommit || _efContext.Commit();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        protected override bool DoDelete(TEntity entity, bool isCommit = true)
        {
            _efContext.RegisterDeleted(entity);
            return !isCommit || _efContext.Commit();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        protected override bool DoUpdate(TEntity entity, bool isCommit = true)
        {
            _efContext.RegisterModified(entity);
            return !isCommit || _efContext.Commit();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        protected override bool DoAdd(IEnumerable<TEntity> entities, bool isCommit = true)
        {
            _efContext.RegisterNew(entities);
            return !isCommit || _efContext.Commit();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        protected override bool DoUpdate(IEnumerable<TEntity> entities, bool isCommit = true)
        {
            foreach (var entity in entities)
            {
                _efContext.RegisterModified(entity);
            }
            return !isCommit || _efContext.Commit();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        protected override bool DoDelete(IEnumerable<TEntity> entities, bool isCommit = true)
        {
            _efContext.RegisterDeleted(entities);
            return !isCommit || _efContext.Commit();
        }
        #endregion
    }



}
