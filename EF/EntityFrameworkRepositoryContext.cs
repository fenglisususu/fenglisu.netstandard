﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Core.Contexts;
using Core.Logging;
using Core.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Aspire.EFRepository
{
    /// <summary>
    /// 
    /// </summary>
    public class EntityFrameworkRepositoryContext<T> : RepositoryContext, IEntityFrameworkRepositoryContext where T : DbContext
    {
        private readonly ThreadLocal<DbContext> _localCtx;

        private readonly ILogger _l;

        public EntityFrameworkRepositoryContext(ILogger l, IServiceProvider provider)
        {
            _l = l;
            var t = provider.GetService(typeof(T));
            _localCtx = new ThreadLocal<DbContext>(() => (DbContext)t);
        }

        /// <summary>
        /// Commits this instance.
        /// </summary>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public override bool Commit()
        {
            if (!Committed)
            {
                if (SaveChanges() > 0)
                {
                    Committed = true;
                    return true;
                }
                else
                {
                    Committed = false;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private int SaveChanges()
        {
            int times = 0;
            int count = -1;
            bool saveFailed;
            do
            {
                times++;

                saveFailed = false;
                try
                {
                    count = _localCtx.Value.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    _l.Error(ex.Message, ex);
                    _l.Error("DbUpdateConcurrencyException 数据库更新并发异常");
                    ex.Entries.Single().Reload();
                    throw;
                }
                catch (Exception e)
                {
                    _l.Error(e.Message, e);
                    throw;
                }
                if (times > 4)
                {
                    saveFailed = false;
                }
            } while (saveFailed);
            return count;
        }

        public override void Rollback()
        {
            Committed = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (!Committed)
                    Commit();
                _localCtx.Value.Dispose();
                _localCtx.Dispose();

            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// Registers the new.
        /// </summary>
        /// <typeparam name="TEntity">The type of the t entity.</typeparam>
        /// <param name="obj">The object.</param>
        public override void RegisterNew<TEntity>(TEntity obj)
        {
            if (obj.Record == 0)
                obj.Record = 1;
            if (obj.ModifyTime == DateTime.MinValue)
                obj.ModifyTime = DateTime.Now;
            if (obj.CreationTime == DateTime.MinValue)
                obj.CreationTime = DateTime.Now;
            if (obj.CreatorId == Guid.Empty)
                obj.CreatorId = UserContext.Current.Id;
            if (obj.ModifyUserId == Guid.Empty)
                obj.ModifyUserId = UserContext.Current.Id;
            _localCtx.Value.Set<TEntity>().Add(obj);
            // _localCtx.Value.Entry(obj).State = EntityState.Added;
            Committed = false;
        }

        /// <summary>
        /// Registers the new.
        /// </summary>
        /// <typeparam name="TEntity">The type of the t entity.</typeparam>
        /// <param name="entities">The entities.</param>
        public override void RegisterNew<TEntity>(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                //_localCtx.Value.Entry(entity).State = EntityState.Added;
                if (entity.Record == 0)
                    entity.Record = 1;
                if (entity.CreationTime == DateTime.MinValue)
                    entity.CreationTime = DateTime.Now;
                if (entity.ModifyTime == DateTime.MinValue)
                    entity.ModifyTime = DateTime.Now;
                entity.CreatorId = UserContext.Current.Id;
                entity.ModifyUserId = UserContext.Current.Id;
            }

            _localCtx.Value.Set<TEntity>().AddRange(entities);

            Committed = false;
        }

        /// <summary>
        /// Registers the modified.
        /// </summary>
        /// <typeparam name="TEntity">The type of the t entity.</typeparam>
        /// <param name="obj">The object.</param>
        public override void RegisterModified<TEntity>(TEntity obj)
        {
            _localCtx.Value.Entry(obj).State = EntityState.Modified;//可能异常
            obj.ModifyTime = DateTime.Now;
            obj.ModifyUserId = UserContext.Current.Id;
            Committed = false;
        }

        /// <summary>
        /// Registers the deleted.
        /// </summary>
        /// <typeparam name="TEntity">The type of the t entity.</typeparam>
        /// <param name="obj">The object.</param>
        public override void RegisterDeleted<TEntity>(TEntity obj)
        {
            _localCtx.Value.Entry(obj).State = EntityState.Deleted;
            Committed = false;
        }

        /// <summary>
        /// Registers the deleted.
        /// </summary>
        /// <typeparam name="TEntity">The type of the t entity.</typeparam>
        /// <param name="entities">The entities.</param>
        public override void RegisterDeleted<TEntity>(IEnumerable<TEntity> entities)
        {
            _localCtx.Value.Set<TEntity>().RemoveRange(entities);
            Committed = false;
        }

        public DbContext Context
        {
            get
            {
                return _localCtx.Value;
            }
        }
    }
}
