﻿using Core.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Aspire.EFRepository
{
    /// <summary>
    /// 
    /// </summary>
    public interface IEntityFrameworkRepositoryContext : IRepositoryContext
    {
        /// <summary>
        /// 
        /// </summary>
        DbContext Context { get; }
    }
}
