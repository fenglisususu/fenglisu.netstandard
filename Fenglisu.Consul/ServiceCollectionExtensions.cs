﻿using System;
using Consul;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Aspire.Consul
{
    public static class ServiceCollectionExtensions
    {
        public static ConsulBuilder AddConsul(this IServiceCollection services, Action<ConsulOptions> options)
        {
            if (options == null)
                throw new ArgumentNullException(nameof(options));

            return services.Configure(options).AddConsulInternal();
        }

        public static ConsulBuilder AddConsul(this IServiceCollection services, IConfiguration configuration)
        {
            return services.Configure<ConsulOptions>(configuration).AddConsulInternal();
        }

        private static ConsulBuilder AddConsulInternal(this IServiceCollection services)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            var builder = new ConsulBuilder(services);
            builder.Services.AddSingleton<IConsulClient>(provider => new ConsulClient(configuration =>
            {
                var options = provider.GetRequiredService<IOptions<ConsulOptions>>().Value;
                configuration.Address = new Uri(options.HttpEndpoint);
                if (!string.IsNullOrEmpty(configuration.Datacenter))
                    configuration.Datacenter = options.Datacenter;
            }));

            return builder;
        }
    }
}
