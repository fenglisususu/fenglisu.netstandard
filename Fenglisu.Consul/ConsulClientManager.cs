﻿using System;
using Consul;
using Microsoft.Extensions.Options;

namespace Aspire.Consul
{
    public class ConsulClientManager
    {
        private readonly ConsulOptions _options;
        private IConsulClient _client;

        public ConsulClientManager(IOptions<ConsulOptions> optionsAccessor)
        {
            _options = optionsAccessor.Value;
        }

        public IConsulClient GetClient()
        {
            if (_client == null)
            {
                _client = new ConsulClient(configuration =>
                {
                    configuration.Address = new Uri(_options.HttpEndpoint);
                    if (!string.IsNullOrEmpty(_options.Datacenter))
                        configuration.Datacenter = _options.Datacenter;
                });
            }

            return _client;
        }
    }
}
