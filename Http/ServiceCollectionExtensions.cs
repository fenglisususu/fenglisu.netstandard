﻿using Core.Services;
using Core.Services.LoadBalancer;
using Microsoft.Extensions.DependencyInjection;

namespace Aspire.Services.Http
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddHttpService(this IServiceCollection services, LoadBalancers loadBalancer = LoadBalancers.RoundRobin)
        {
            services.AddHttpContextAccessor();
            services.AddTransient<PolicyHttpClientHandler>();
            switch (loadBalancer)
            {
                case LoadBalancers.Random:
                    services.AddSingleton<ILoadBalancer, RandomLoadBalancer>();
                    break;
                case LoadBalancers.LocalFirst:
                    services.AddSingleton<ILoadBalancer, LocalFirstLoadBalancer>();
                    break;
                default:
                    services.AddSingleton<ILoadBalancer, RoundRobinLoadBalancer>();
                    break;
            }

            services.AddHttpClient("httpService")
                .ConfigurePrimaryHttpMessageHandler(provider => provider.GetService<PolicyHttpClientHandler>());
            services.AddSingleton<IClientFactory, HttpClientFactory>();

            return services;
        }
    }
}
