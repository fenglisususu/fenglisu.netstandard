﻿using System;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using Core.Services;
using Core.Services.LoadBalancer;
using Core.Services.Registry;
using Exceptionless;
using Newtonsoft.Json;
using Refit;

namespace Aspire.Services.Http
{
    public class HttpClientFactory : IClientFactory
    {
        private readonly IServiceRegistry _serviceRegistry;
        private readonly ILoadBalancer _loadBalancer;
        private readonly IHttpClientFactory _httpClientFactory;

        public HttpClientFactory(IServiceRegistry serviceRegistry, ILoadBalancer loadBalancer, IHttpClientFactory httpClientFactory)
        {
            _serviceRegistry = serviceRegistry;
            _loadBalancer = loadBalancer;
            _httpClientFactory = httpClientFactory;
        }

        public async Task<T> Create<T>() where T : IService
        {
            var attribute = typeof(T).GetCustomAttribute<ServiceNameAttribute>();
            if (attribute == null)
            {
                //throw new Exception("未设置服务名称");
                ExceptionlessClient.Default.SubmitException(new Exception("未设置服务名称"));
                return default(T);
            }

            var serviceName = attribute.Name;

            var services = await _serviceRegistry.FindServices(serviceName);
            if (services.Count == 0)
            {
                //throw new Exception(string.Format("未找到服务{0}的注册信息", serviceName));
                ExceptionlessClient.Default.SubmitException(new Exception($"未找到服务{serviceName}的注册信息"));
                return default(T);
            }

            var service = await _loadBalancer.Select(services);
            var httpClient = _httpClientFactory.CreateClient("httpService");
            httpClient.BaseAddress = new Uri($"http://{service.Address}:{service.Port}");

            return RestService.For<T>(httpClient, new RefitSettings
            {
                JsonSerializerSettings = new JsonSerializerSettings
                    {NullValueHandling = NullValueHandling.Ignore, ReferenceLoopHandling = ReferenceLoopHandling.Ignore}
            });
        }
    }
}
