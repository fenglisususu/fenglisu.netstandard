﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Core.Exceptions;
using Core.Logging;
using Core.Models;
using Exceptionless;
using Exceptionless.Logging;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Polly;
using Polly.Retry;

namespace Aspire.Services.Http
{
    public class PolicyHttpClientHandler : HttpClientHandler
    {
        private static readonly AsyncRetryPolicy _policy;
        private readonly ILogger _logger;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IConfiguration _configuration;

        static PolicyHttpClientHandler()
        {
            // 重试策略
            _policy = Policy.Handle<Exception>(i => !i.GetType().IsAssignableFrom(typeof(AspireException))).RetryAsync(3);
        }

        public PolicyHttpClientHandler(ILogger logger, IHttpContextAccessor httpContextAccessor, IConfiguration configuration)
        {
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var apiAddress = string.Format("{0} {1}://{2}:{3}{4}", request.Method, request.RequestUri.Scheme,
                request.RequestUri.Host, request.RequestUri.Port, request.RequestUri.PathAndQuery);

            _logger.Debug(string.Format("调用接口：{0}", apiAddress));

            if (_httpContextAccessor.HttpContext != null)
            {
                var requestHeaders = _httpContextAccessor.HttpContext.Request.Headers;
                IDictionary<string, string> headers = new Dictionary<string, string>();
                foreach (var pair in requestHeaders)
                {
                    var headerName = pair.Key;
                    if (headerName.EndsWith("token", StringComparison.CurrentCultureIgnoreCase))
                        headers.Add(headerName, requestHeaders[headerName]);
                }
                var appId = _configuration["AppId"];
                if (!string.IsNullOrEmpty(appId))
                    headers.Add("X-App-Id", appId);

                if (headers.Count > 0)
                {
                    foreach (var headerName in headers.Keys)
                    {
                        request.Headers.Add(headerName, headers[headerName]);
                    }
                }
            }
            _logger.Debug(string.Format("Headers: {0}", JsonConvert.SerializeObject(request.Headers, Formatting.Indented)));

            Stopwatch stopwatch = Stopwatch.StartNew();
            var response = await _policy.ExecuteAsync(() => base.SendAsync(request, cancellationToken));
            stopwatch.Stop();
            _logger.Debug(string.Format("接口{0}调用耗时：{1}ms", apiAddress, stopwatch.ElapsedMilliseconds));

            ExceptionlessClient.Default.CreateLog("HTTP", apiAddress, LogLevel.Debug)
                .SetProperty("Headers", request.Headers).SetProperty("Elapsed", stopwatch.ElapsedMilliseconds).Submit();

            return response;
        }
    }
}
