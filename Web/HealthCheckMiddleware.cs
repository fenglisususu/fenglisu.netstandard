﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Aspire.Web
{
    public class HealthCheckMiddleware
    {
        private readonly RequestDelegate _next;

        public HealthCheckMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            if (context.Request.Path == "/healthcheck")
            {
                context.Response.StatusCode = 200;
                await context.Response.WriteAsync("ok");
                return;
            }

            await _next(context);
        }
    }
}
