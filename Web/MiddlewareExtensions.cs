﻿using System;
using Microsoft.AspNetCore.Builder;

namespace Aspire.Web
{
    public static class MiddlewareExtensions
    {
        public static IApplicationBuilder UseTokenAuthentication(this IApplicationBuilder app)
        {
            if (app == null)
                throw new ArgumentNullException(nameof(app));

            return app.UseMiddleware<TokenAuthenticationMiddleware>();
        }

        public static IApplicationBuilder UseHealthCheck(this IApplicationBuilder app)
        {
            if (app == null)
                throw new ArgumentNullException(nameof(app));

            return app.UseMiddleware<HealthCheckMiddleware>();
        }
    }
}
