﻿using System.Security.Claims;
using System.Threading.Tasks;
using Core.Contexts;
using Core.Extensions;
using Core.Logging;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace Aspire.Web
{
    public class TokenAuthenticationMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly UserContextManager _contextManager;

        private readonly string _appId;

        public TokenAuthenticationMiddleware( RequestDelegate next, UserContextManager contextManager, IConfiguration configuration)
        {
            _next = next;
            _contextManager = contextManager;

            _appId = configuration["AppId"];
        }

        public async Task InvokeAsync(HttpContext context)
        {
            UserContext.Current.Clear();
            UserContext.Current.IsAuthenticated = false;

            LoadContextValuesFromCookie(context.Request.Cookies);
            LoadContextValuesFromHeader(context.Request.Headers);
            var userAgent = context.Request.Headers["User-Agent"];
            UserContext.Current.IsMobile = UserAgentHelper.IsMobile(userAgent);
            UserContext.Current.UserAgent = userAgent;

            if (!string.IsNullOrEmpty(UserContext.Current.Token))
            {
                _contextManager.LoadUserContext(UserContext.Current.Token);
                var result = await context.AuthenticateAsync(TokenDefaults.AuthenticationScheme);
                if (result?.Principal != null)
                {
                    context.User = result.Principal;
                }
            }

            await _next(context);
        }

        private void LoadContextValuesFromCookie(IRequestCookieCollection cookies)
        {
            foreach (string key in cookies.Keys)
            {
                var cookieValue = cookies[key];
                if (string.IsNullOrWhiteSpace(_appId))
                    UserContext.Current.SetProperty(key, cookieValue);
                else if (key.StartsWith(_appId))
                    UserContext.Current.SetProperty(key.Replace(_appId + "_", string.Empty), cookieValue);
            }
        }

        private void LoadContextValuesFromHeader(IHeaderDictionary headers)
        {
            foreach (var pair in headers)
            {
                var key = pair.Key;
                foreach (var value in pair.Value)
                {
                    if (string.IsNullOrWhiteSpace(_appId))
                        UserContext.Current.SetProperty(key, value);
                    else if (key.StartsWith(_appId))
                    {
                        UserContext.Current.SetProperty(key.Replace(_appId + "_", string.Empty), value);
                    }
                }
            }
        }
    }
}
