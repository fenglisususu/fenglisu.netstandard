﻿using System;
using System.Diagnostics;
using System.Text;
using Core.Exceptions;
using Core.Logging;
using Core.Models;
using Exceptionless;
using Exceptionless.Logging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;

namespace Aspire.Web
{
    public class LoggingActionFilter : IActionFilter, IExceptionFilter
    {
        private readonly IHostingEnvironment _env;
        private Stopwatch _stopwatch;
        private readonly Log _log = new Log();

        public LoggingActionFilter( IHostingEnvironment env)
        {
            _env = env;
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (_stopwatch == null)
            {
                _stopwatch = Stopwatch.StartNew();
            }
            else
            {
                _stopwatch.Restart();
            }
            _log.Request = new
            {
                Controller = context.Controller.GetType().Name,
                Action = context.ActionDescriptor.DisplayName,
                Parameters = context.ActionArguments
            };
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.Exception == null)
            {
                object response = "";
                var result = context.Result;
                if (result is ObjectResult objectResult)
                    response = objectResult.Value;
                else if (result is ViewResult viewResult)
                    response = new
                    {
                        viewResult.ViewName,
                        viewResult.Model,
                        viewResult.ViewData
                    };
                else if (result is JsonResult jsonResult)
                    response = jsonResult.Value;
                else if (result is ContentResult contentResult)
                    response = contentResult.Content;
                _log.Response = response;
                if (_stopwatch != null)
                {
                    _stopwatch.Stop();
                    _log.Elapsed = _stopwatch.ElapsedMilliseconds;
                }

                ExceptionlessClient.Default.CreateLog("URL", context.HttpContext.Request.Path.Value, LogLevel.Debug)
                    .SetProperty("Headers", context.HttpContext.Request.Headers).SetProperty("Request", _log.Request)
                    .SetProperty("Response", _log.Response)
                    .SetProperty("Elapsed", _log.Elapsed + "ms").Submit();
            }
        }

        public void OnException(ExceptionContext context)
        {
            var exception = context.Exception;
            object response;
            if (exception is AspireException aspireException)
            {
                response = Result.OutputFail(aspireException.Message, aspireException.Code);
            }
            else
            {
                if (_env.IsDevelopment())
                {
                    response = Result.OutputFail(exception.Message, "500");
                }
                else
                {
                    response = Result.OutputFail("服务器出错了", "500");
                }
            }

            _log.Response = response;

            context.Result = new JsonResult(response);
            context.ExceptionHandled = true;
            if (_stopwatch != null)
            {
                _stopwatch.Stop();
                _log.Elapsed = _stopwatch.ElapsedMilliseconds;
            }
            ExceptionlessClient.Default.CreateException(exception).Submit();
            ExceptionlessClient.Default.CreateLog("URL", context.HttpContext.Request.Path.Value, LogLevel.Error)
                .SetProperty("Request", _log.Request).SetProperty("Response", _log.Response)
                .SetProperty("Elapsed", _log.Elapsed + "ms").SetProperty("Exception", $"{exception.Message}{Environment.NewLine}{exception.StackTrace}").Submit();
        }

        class Log
        {
            public object Request { get; set; }
            public object Response { get; set; }
            public long Elapsed { get; set; }
        }
    }
}
