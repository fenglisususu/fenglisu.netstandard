﻿using Core.Exceptions;
using Core.Logging;
using Core.Models;
using Exceptionless;
using Exceptionless.Logging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Aspire.Web
{
    public class ExceptionLoggingFilterAttribute : ExceptionFilterAttribute
    {
        private readonly IHostingEnvironment _env;

        public ExceptionLoggingFilterAttribute(IHostingEnvironment env)
        {
            _env = env;
        }

        public override void OnException(ExceptionContext context)
        {
            var exception = context.Exception;
            ExceptionlessClient.Default.CreateLog("URL", context.HttpContext.Request.Path.Value, LogLevel.Error)
                .SetProperty("Request", context.ActionDescriptor.Parameters).Submit();
            //if (context.HttpContext.Request.IsAjaxRequest())
            //{
            if (exception is AspireException aspireException)
            {
                context.Result = new JsonResult(Result.OutputFail(aspireException.Message, aspireException.Code));
            }
            else
            {
                if (_env.IsDevelopment())
                {
                    context.Result = new JsonResult(Result.OutputFail(exception.Message, "500"));
                }
                else
                {
                    context.Result = new JsonResult(Result.OutputFail("服务器出错了", "500"));
                }
            }

            context.ExceptionHandled = true;
            //}
            base.OnException(context);
        }
    }
}
