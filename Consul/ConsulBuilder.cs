﻿using Microsoft.Extensions.DependencyInjection;

namespace Aspire.Consul
{
    public class ConsulBuilder
    {
        public IServiceCollection Services { get; }

        public ConsulBuilder(IServiceCollection services)
        {
            Services = services;
        }
    }
}
