﻿using System;
using Core.Logging;
using Newtonsoft.Json;

namespace Core.SMS
{
    public class NullSMSStorageProvider : ISMSStorageProvider
    {
        private readonly ILogger _logger;

        public NullSMSStorageProvider(ILogger logger)
        {
            _logger = logger;
        }

        public void Save(string productName, string content, string phoneNumbers, DateTime sendTime, bool success, string message, string response)
        {
            _logger.Info("Use Null storage provider: " + JsonConvert.SerializeObject(new
            {
                productName,
                content,
                phoneNumbers,
                sendTime,
                success,
                message,
                response
            }, Formatting.Indented));
        }
    }
}
