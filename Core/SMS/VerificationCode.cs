﻿namespace Core.SMS
{
    public class VerificationCode
    {

        public string ResNumber { get; set; }

        public string No { get; set; }

        public string Account { get; set; }
    }


    public class OrderToUser
    {
        //您的订单${orderno}（${gi}）状态已改变 ，详情：maiyaoonline.com/order/${orderid}。

        public string ResNumber { get; set; }

        public string Orderno { get; set; }

        public string Gi { get; set; }

        public string State { get; set; }
    }

    public class UserInfo
    {
        //已为您开通个人账户，账户：${acd}，密码：${pwd}，绑定手机：${zhmobile}，登陆地址： maiyaoonline.com 。详情请联系客服${kfinfo}。

        public string ResNumber { get; set; }

        public string Acd { get; set; }

        public string Pwd { get; set; }

        public string Zhmobile { get; set; }

        public string Kfinfo { get; set; }
    }

}
