﻿namespace Core.SMS
{
    public interface ISMSServer
    {
        /// <summary>
        /// 发送短信
        /// </summary>
        /// <param name="template">模板ID</param>
        /// <param name="parameters">模板参数值，以匿名对象方式传递</param>
        /// <param name="phoneNumbers">手机号码数组</param>
        /// <returns></returns>
        bool Send(string template, object parameters, params string[] phoneNumbers);

        string Name { get; }
    }
}
