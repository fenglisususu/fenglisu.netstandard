﻿
using System;

namespace Core.SMS
{
    public interface ISMSStorageProvider
    {
        void Save(string productName, string content, string phoneNumbers, DateTime sendTime, bool success, string message,
            string response);
    }
}
