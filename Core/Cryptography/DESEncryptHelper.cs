﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Core.Cryptography
{
    /// <summary>
    /// 定义访问数据加密标准 (DES) 算法的加密服务
    /// </summary>
    public class DESEncryptHelper
    {

        public static readonly Byte[] DesKey = { 5, 7, 8, 9, 0, 2, 1, 6 };

        public static readonly Byte[] DesVi = { 6, 9, 8, 5, 1, 6, 2, 8 };

        /// <summary>
        /// DES加密
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns>String.</returns>
        public static String Encrypt(String data)
        {

            var des = new DESCryptoServiceProvider();
            //des.GenerateKey();
            //des.Key
            des.Mode = CipherMode.CBC;

            Encoding utf = new UTF8Encoding();
            //var   DesKey1= Encoding.UTF8.GetBytes("57890216");
            ICryptoTransform encryptor = des.CreateEncryptor(DesKey, DesVi);

            byte[] bData = utf.GetBytes(data);

            byte[] bEnc = encryptor.TransformFinalBlock(bData, 0, bData.Length);

            return Convert.ToBase64String(bEnc);

        }


        /// <summary>
        /// DES解密
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns>String.</returns>
        public static String  Decrypt(String data)
        {

            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            des.Mode = CipherMode.CBC;
           
            Encoding utf = new UTF8Encoding();

            ICryptoTransform decryptor = des.CreateDecryptor(DesKey, DesVi);

             
            byte[] bEnc = Convert.FromBase64String(data);

            byte[] bDec = decryptor.TransformFinalBlock(bEnc, 0, bEnc.Length);

            return utf.GetString(bDec);

        }

        /// <summary>
        /// DES解密
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns>String.</returns>
        //public static string Decrypt(string pToDecrypt)
        //{
        //    //转义特殊字符
        //    //pToDecrypt = pToDecrypt.Replace("-", "+");
        //    //pToDecrypt = pToDecrypt.Replace("_", "/");
        //    //pToDecrypt = pToDecrypt.Replace("~", "=");
        //    byte[] inputByteArray = Convert.FromBase64String(pToDecrypt);

        //    using (DESCryptoServiceProvider des = new DESCryptoServiceProvider())
        //    {
        //        // des.Mode= CipherMode.CFB;
        //        des.Key = DesKey;// Encoding.UTF8.GetBytes("57890216");
        //        des.IV = DesVi;// Encoding.UTF8.GetBytes("69851628");
        //        //des.Key = DesKey;
        //        //des.IV = DesVi;
        //        System.IO.MemoryStream ms = new System.IO.MemoryStream();
        //        using (CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(), CryptoStreamMode.Write))
        //        {
        //            cs.Write(inputByteArray, 0, inputByteArray.Length);
        //            cs.FlushFinalBlock();
        //            cs.Close();
        //        }
        //        string str = Encoding.UTF8.GetString(ms.ToArray());
        //        ms.Close();
        //        return str;
        //    }
        //}
    }
}
