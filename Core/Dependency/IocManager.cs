﻿using Autofac;

namespace Core.Dependency
{
    public class IocManager
    {
        private static IContainer _container;

        public static T Resolve<T>()
        {
            return _container.Resolve<T>();
        }

        public static T Resolve<T>(string name)
        {
            return _container.ResolveNamed<T>(name);
        }

        public static void Register(IContainer container)
        {
            _container = container;
        }
    }
}
