﻿using System;

namespace Core.Logging
{

    /// <summary>
    /// 日志操作接口
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// 需要记录日志的类型
        /// </summary>
        Type Type { get; set; }
        /// <summary>
        /// 写入调试日志
        /// </summary>
        /// <param name="message">日志信息</param>
        void Debug( object message);
        /// <summary>
        /// 写入调试日志及异常
        /// </summary>
        /// <param name="message">日志信息</param>
        /// <param name="exception">异常</param>
        void Debug( object message, Exception exception);
        /// <summary>
        /// 写入信息日志
        /// </summary>
        /// <param name="message">日志信息</param>
        void Info( object message);
        /// <summary>
        /// 写入信息日志及异常
        /// </summary>
        /// <param name="message">日志信息</param>
        /// <param name="exception">异常</param>
        void Info( object message, Exception exception);
        /// <summary>
        /// 写入警告日志
        /// </summary>
        /// <param name="message">日志信息</param>
        void Warn( object message);
        /// <summary>
        /// 写入警告日志及异常
        /// </summary>
        /// <param name="message">日志信息</param>
        /// <param name="exception">异常</param>
        void Warn( object message, Exception exception);
        /// <summary>
        /// 写入错误日志
        /// </summary>
        /// <param name="message">日志信息</param>
        void Error( object message);
        /// <summary>
        /// 写入错误日志及异常
        /// </summary>
        /// <param name="message">日志信息</param>
        /// <param name="exception">异常</param>
        void Error( object message, Exception exception);
        /// <summary>
        /// 写入致命错误日志
        /// </summary>
        /// <param name="message">日志信息</param>
        void Fatal( object message);
        /// <summary>
        /// 写入致命错误日志及日常
        /// </summary>
        /// <param name="message">日志信息</param>
        /// <param name="exception">异常</param>
        void Fatal( object message, Exception exception);
    }
}
