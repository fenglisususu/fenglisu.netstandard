﻿using System;

namespace Core.Events
{
    public class EventEntity : IEventEntity
    {
        public string EventId { get; set; }
        public string EventType { get; set; }
        public string EventData { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
