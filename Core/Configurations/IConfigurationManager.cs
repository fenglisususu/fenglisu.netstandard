﻿using System.Threading.Tasks;

namespace Core.Configurations
{
    public interface IConfigurationManager
    {
        Task<string> GetString(string key);
        Task<int> GetInt(string key);
        Task<long> GetLong(string key);
        Task<double> GetDouble(string key);
        Task<decimal> GetDecimal(string key);
        Task<T> Get<T>(string key) where T : class;
    }
}
