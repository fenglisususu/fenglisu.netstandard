﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Core.Configurations
{
    public class DefaultConfigurationManager : IConfigurationManager
    {
        private readonly IConfiguration _configuration;

        public DefaultConfigurationManager(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public Task<string> GetString(string key)
        {
            return Task.FromResult(GetValue(key));
        }

        public Task<int> GetInt(string key)
        {
            var stringValue = GetValue(key);
            return Task.FromResult(Convert.ToInt32(stringValue));
        }

        public Task<long> GetLong(string key)
        {
            var stringValue = GetValue(key);
            return Task.FromResult(Convert.ToInt64(stringValue));
        }

        public Task<double> GetDouble(string key)
        {
            var stringValue = GetValue(key);
            return Task.FromResult(Convert.ToDouble(stringValue));
        }

        public Task<decimal> GetDecimal(string key)
        {
            var stringValue = GetValue(key);
            return Task.FromResult(Convert.ToDecimal(stringValue));
        }

        public Task<T> Get<T>(string key) where T : class
        {
            var stringValue = GetValue(key);
            T t = default(T);
            try
            {
                t = JsonConvert.DeserializeObject<T>(stringValue);
            }
            catch (Exception)
            {
            }
            return Task.FromResult(t);
        }

        public string GetValue(string key)
        {
            return _configuration[key];
        }
    }
}
