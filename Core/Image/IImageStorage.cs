﻿using System.Threading.Tasks;

namespace Core.Image
{
    public interface IImageStorage
    {
        /// <summary>
        /// 上传
        /// </summary>
        /// <param name="bytes">文件字节数组</param>
        /// <param name="fileExtension">文件扩展名</param>
        /// <returns></returns>
        ImageUploadResult Upload(byte[] bytes, string fileExtension);

        /// <summary>
        /// Uploads the asynchronous.
        /// </summary>
        /// <param name="bytes">The bytes.</param>
        /// <param name="fileExtension">The file extension.</param>
        /// <returns>Task&lt;ImageUploadResult&gt;.</returns>
        Task<ImageUploadResult> UploadAsync(byte[] bytes, string fileExtension);
    }

    /// <summary>
    /// 图片上传结果
    /// </summary>
    public class ImageUploadResult
    {
        /// <summary>
        /// 上传是否成功
        /// </summary>
        public bool State { get; set; }
        /// <summary>
        /// 错误信息
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// 文件名（不包含扩展名）
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// 文件访问路径
        /// </summary>
        public string FilePath { get; set; }
        /// <summary>
        /// 文件大小
        /// </summary>
        public long Size { get; set; }
    }
}
