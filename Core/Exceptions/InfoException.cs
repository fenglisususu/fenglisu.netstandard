﻿namespace Core.Exceptions
{
    /// <summary>
    /// Class InfoException.
    /// </summary>
    public class InfoException : CustomException
    {
        /// <summary>
        /// 使用指定的错误信息初始化 <see cref="InfoException" /> 类的新实例。
        /// </summary>
        /// <param name="message">描述错误的消息。</param>
        public InfoException(string message)
        {
            _message = message;
            this.Data.Add("ErrorNumber", "0000");
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="InfoException"/> class.
        /// </summary>
        /// <param name="errorNumber">The error number.</param>
        /// <param name="msgParams">The MSG parameters.</param>
        protected InfoException(string errorNumber, string[] msgParams)
        {
            this.Data.Add("ErrorNumber", errorNumber);
            this.Data.Add("MsgParams", msgParams);
        }
        private readonly string _message;
        public override string Message
        {
            get
            {
                if (string.IsNullOrEmpty(_message))
                    return base.Message;
                return _message;
            }
        }
    }
}
