﻿namespace Core.Exceptions
{
    /// <summary>
    /// Class ErrorException.
    /// </summary>
    public class ErrorException : FatalException
    {
        // Methods
        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorException"/> class.
        /// </summary>
        /// <param name="errorNumber">The error number.</param>
        /// <param name="parameters">The parameters.</param>
        public ErrorException(string errorNumber, params string[] parameters)
            : base(errorNumber, parameters)
        {
        }
    }

 

}
