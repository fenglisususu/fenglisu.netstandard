﻿using System;

namespace Core.Exceptions
{
    public class NoRecordException : AspireException
    {
        public NoRecordException() : base(NORECORD)
        {

        }

        public NoRecordException(string recordName) : base(string.Format("{0}不存在", recordName))
        {

        }

        public NoRecordException(string message, string code = "", Action action = null) : base(message, code, action)
        {
        }
    }
}
