﻿using System;

namespace Core.Exceptions
{

    /// <summary>
    /// Class CustomException.
    /// </summary>
    public abstract class CustomException : Exception
    {
        // Methods
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomException"/> class.
        /// </summary>
        protected CustomException()
        {
        }

        /// <summary>
        /// Gets the message.
        /// </summary>
        /// <returns>System.String.</returns>
        public string GetMessage()
        {
            return this.GetMessage();
            //return this.GetMessage(Culture.ZHCN);
        }

        /// <summary>
        /// Gets the message.
        /// </summary>
        /// <param name="culture">The culture.</param>
        /// <returns>System.String.</returns>
        //public string GetMessage(Culture culture)
        //{
        //    string str = Globalization.GetExceptionMessage(culture, this.ResourceKey, this.MsgParams);
        //    if (string.IsNullOrEmpty(str))
        //    {
        //        str = string.Join("\r\n", MsgParams);
        //      //  return this.ErrorNumber;
        //    }
        //    return string.Format("[ErrorNumber:{0}] {1}", this.ErrorNumber, str);
        //}

        // Properties
        /// <summary>
        /// Gets the error number.
        /// </summary>
        /// <value>The error number.</value>
        public string ErrorNumber
        {
            get
            {
                return (string)this.Data["ErrorNumber"];
            }
        }

        public override string Message
        {
            get
            {
                return this.GetMessage();
                //CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
                //if (this.Data.Contains("CultureInfo") && (this.Data["CultureInfo"] is CultureInfo))
                //{
                //    currentCulture = this.Data["CultureInfo"] as CultureInfo;
                //}
                //return this.GetMessage(Globalization.ConvertFrom(currentCulture));
            }
        }

        /// <summary>
        /// Gets the MSG parameters.
        /// </summary>
        /// <value>The MSG parameters.</value>
        public string[] MsgParams
        {
            get
            {
                return (string[])this.Data["MsgParams"];
            }
        }

        /// <summary>
        /// Gets the resource key.
        /// </summary>
        /// <value>The resource key.</value>
        public string ResourceKey
        {
            get
            {
                return (base.GetType().FullName + "." + this.ErrorNumber);
            }
        }
    }
}
