﻿using System;

namespace Core.Exceptions
{
    public class NoPermissionException : AspireException
    {
        public NoPermissionException() : base(NOPERMISSION)
        {

        }

        public NoPermissionException(string message, string code = "", Action action = null) : base(message, code, action)
        {
        }
    }
}
