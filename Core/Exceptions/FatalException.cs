﻿namespace Core.Exceptions
{
    /// <summary>
    /// Class FatalException.
    /// </summary>
    public abstract class FatalException : CustomException
    {
        // Methods
        /// <summary>
        /// Initializes a new instance of the <see cref="FatalException"/> class.
        /// </summary>
        /// <param name="errorNumber">The error number.</param>
        /// <param name="msgParams">The MSG parameters.</param>
        protected FatalException(string errorNumber, string[] msgParams)
        {
            this.Data.Add("ErrorNumber", errorNumber);
            this.Data.Add("MsgParams", msgParams);
        }
    }

 

}
