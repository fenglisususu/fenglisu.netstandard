﻿using System;

namespace Core.Exceptions
{
    public class AspireException : Exception
    {
        public const string NORECORD = "记录不存在";
        public const string NOPERMISSION = "访问权限不足";
        public const string COMMIT_FAIL = "数据库保存失败";

        public AspireException(string message, string code = "", Action action = null):
            base(message)
        {
            Code = code;
            Action = action;
        }
       // public new string Message { get; set; }
        public string Code { get; set; }
        public Action Action { get; set; }
    }
}
