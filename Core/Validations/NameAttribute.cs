﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Core.Validations
{
    /// <summary>
    /// 名称验证(中文/大小写字母/数字/下划线/半角小括号)
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter)]
    public class NameAttribute : ValidationAttribute
    {
        private static readonly Regex Regex = new Regex(@"^[a-zA-Z0-9_\(\)]+$", RegexOptions.Compiled);
        private static readonly Regex ChineseRegex = new Regex(@"^[\u4e00-\u9fa5a-zA-Z0-9_\(\)]+$", RegexOptions.Compiled);

        public bool IncludeChinese { get; set; }

        public NameAttribute()
        {
        }

        /// <summary>
        /// 是否包含中文
        /// </summary>
        /// <param name="includeChinese"></param>
        public NameAttribute(bool includeChinese)
        {
            IncludeChinese = includeChinese;
        }

        public override bool IsValid(object value)
        {
            if (value == null) return true;
            if (IncludeChinese)
                return ChineseRegex.IsMatch(value.ToString());
            return Regex.IsMatch(value.ToString());
        }
    }
}
