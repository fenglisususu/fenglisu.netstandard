﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Core.Validations
{
    /// <summary>
    /// 手机号码验证
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter)]
    public class MobileAttribute : ValidationAttribute
    {
        private static readonly Regex Regex = new Regex(@"^(0|\+?86|17951)?(13[0-9]|15[0-9]|17[0678]|18[0-9]|14[57])[0-9]{8}$", RegexOptions.Compiled);

        public override bool IsValid(object value)
        {
            return Regex.IsMatch(value.ToString());
        }
    }
}
