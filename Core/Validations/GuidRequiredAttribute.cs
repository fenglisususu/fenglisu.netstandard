﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Core.Validations
{
    /// <summary>
    /// Guid字段必需
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter)]
    public class GuidRequiredAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value == null)
                return false;
            if (!Guid.TryParse(value.ToString(), out var guid))
                return false;
            if (guid == Guid.Empty) return false;

            return true;
        }
    }
}
