﻿using System.Collections;
using System.Collections.Generic;

namespace Core.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class PagedResult<TEntity> : IEnumerable<TEntity>
    {
        private readonly int _pageNumber;
        private readonly int _pageSize;
        private readonly int _recordCount;
        private readonly int _pageCount;
        private readonly IEnumerable<TEntity> _results;

        /// <summary>
        /// 结果集合
        /// </summary>
        public IEnumerable<TEntity> Results { get { return _results; } }

        /// <summary>
        ///  当前页
        /// </summary>
        public int PageNumber { get { return _pageNumber; } }
        /// <summary>
        /// 每页记录数
        /// </summary>
        public int PageSize { get { return _pageSize; } }
        /// <summary>
        /// 总记录数
        /// </summary>
        public int RecordCount { get { return _recordCount; }}
        /// <summary>
        /// 总页数
        /// </summary>
        public int PageCount { get { return _pageCount; } }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="recordCount"></param>
        /// <param name="results"></param>
        public PagedResult(int pageNumber, int pageSize, int recordCount, IEnumerable<TEntity> results)
        {
            _pageNumber = pageNumber;
            _pageSize = pageSize;
            _recordCount = recordCount;
            _results = results;
            _pageCount = _recordCount%_pageSize == 0 ? _recordCount/_pageSize : (_recordCount/_pageSize + 1);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerator<TEntity> GetEnumerator()
        {
            return Results.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
