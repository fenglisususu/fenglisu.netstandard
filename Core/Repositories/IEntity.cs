﻿using System;

namespace Core.Repositories
{
    /// <summary>
    /// 实体接口
    /// </summary>
    public interface IEntity
    {
        /// <summary>
        /// 主键
        /// </summary>
        Guid Id { get; set; }

         /// <summary>
         /// 记录状态  -1 逻辑删除 ，1 有效  -2 删除
         /// </summary>
         int Record { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        DateTime CreationTime { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        DateTime ModifyTime { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        Guid CreatorId { get; set; }
        /// <summary>
        /// 修改人
        /// </summary>
        Guid ModifyUserId { get; set; }
        //EntityState EntityState { get; set; }
    }
}
