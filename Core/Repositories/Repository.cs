﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using Core.Logging;

namespace Core.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : class, IEntity
    {
        private readonly IRepositoryContext _context;
        private static readonly Expression<Func<TEntity, DateTime>> DefaultSortPredicate = i => i.CreationTime;
        private static readonly Expression<Func<TEntity, bool>> DefaultExpression;

        private ILogger _log;

        /// <summary>
        /// 
        /// </summary>
        protected Repository()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        protected Repository(IRepositoryContext context)
        {
            _context = context;
        }

        static Repository()
        {
            var entityType = typeof(TEntity);
            var p1 = Expression.Parameter(entityType, "r");

            var propertyType = entityType.GetProperty("Record");

            var ma = Expression.MakeMemberAccess(p1, propertyType);
            var eq = Expression.GreaterThanOrEqual(ma, Expression.Constant(1, typeof(Int32)));
            DefaultExpression = (Expression<Func<TEntity, bool>>)Expression.Lambda(eq, p1);
        }


        public virtual IRepositoryContext Context
        {
            get
            {
                return _context;
            }
        }

        #region Abstract Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <returns></returns>
        protected abstract IEnumerable<TEntity> DoFindAll<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortField"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        protected abstract IEnumerable<TEntity> DoFindAll(Expression<Func<TEntity, bool>> queryPredicate,
            string sortField, string sortOrder);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <returns></returns>
        protected abstract PagedResult<TEntity> DoFindAll<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, int pageNumber, int pageSize);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="selector"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <returns></returns>
        protected abstract PagedResult<TResult> DoFindAll<TKey, TResult>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            Expression<Func<TEntity, TResult>> selector, int pageNumber, int pageSize);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortField"></param>
        /// <param name="sortOrder"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        protected abstract PagedResult<TEntity> DoFindAll(Expression<Func<TEntity, bool>> queryPredicate,
            string sortField, string sortOrder, int pageNumber, int pageSize);

        protected abstract PagedResult<TResult> DoFindAll<TResult>(Expression<Func<TEntity, bool>> queryPredicate, string sortField, string sortOrder, Expression<Func<TEntity, TResult>> selector, int pageNumber, int pageSize);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <returns></returns>
        protected abstract IEnumerable<TEntity> DoFindAllByLimit<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, int offset, int limit);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="selector"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <returns></returns>
        protected abstract IEnumerable<TResult> DoFindAllByLimit<TKey, TResult>(
            Expression<Func<TEntity, bool>> queryPredicate, Expression<Func<TEntity, TKey>> sortPredicate,
            SortOrder sortOrder, Expression<Func<TEntity, TResult>> selector, int offset, int limit);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortField"></param>
        /// <param name="sortOrder"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        protected abstract IEnumerable<TEntity> DoFindAllByLimit(Expression<Func<TEntity, bool>> queryPredicate,
            string sortField, string sortOrder, int offset, int limit);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <returns></returns>
        protected abstract IEnumerable<TEntity> DoFindAll<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortField"></param>
        /// <param name="sortOrder"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        protected abstract IEnumerable<TEntity> DoFindAll(Expression<Func<TEntity, bool>> queryPredicate,
            string sortField, string sortOrder, params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <returns></returns>
        protected abstract IEnumerable<TEntity> DoFindAll<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, params string[] eagerLoadingPaths);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortField"></param>
        /// <param name="sortOrder"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <returns></returns>
        protected abstract IEnumerable<TEntity> DoFindAll(Expression<Func<TEntity, bool>> queryPredicate,
            string sortField, string sortOrder, params string[] eagerLoadingPaths);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <returns></returns>
        protected abstract PagedResult<TEntity> DoFindAll<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, int pageNumber, int pageSize,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortField"></param>
        /// <param name="sortOrder"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        protected abstract PagedResult<TEntity> DoFindAll(Expression<Func<TEntity, bool>> queryPredicate,
            string sortField, string sortOrder, int pageNumber, int pageSize,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortString"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        protected abstract PagedResult<TEntity> DoFindAll(Expression<Func<TEntity, bool>> queryPredicate,
            string sortString, int pageNumber, int pageSize,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <returns></returns>
        protected abstract PagedResult<TEntity> DoFindAll<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, int pageNumber, int pageSize,
            params string[] eagerLoadingPaths);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="thenBySortOrder"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <param name="thenBySortPredicate"></param>
        /// <returns></returns>
        protected abstract PagedResult<TEntity> DoFindAll<TKey1, TKey2>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey1>> sortPredicate, SortOrder sortOrder,
            Expression<Func<TEntity, TKey2>> thenBySortPredicate, SortOrder thenBySortOrder, int pageNumber,
            int pageSize, params string[] eagerLoadingPaths);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortField"></param>
        /// <param name="sortOrder"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <returns></returns>
        protected abstract PagedResult<TEntity> DoFindAll(Expression<Func<TEntity, bool>> queryPredicate,
            string sortField, string sortOrder, int pageNumber, int pageSize, params string[] eagerLoadingPaths);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <returns></returns>
        protected abstract IEnumerable<TEntity> DoFindAllByLimit<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, int offset, int limit,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortField"></param>
        /// <param name="sortOrder"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        protected abstract IEnumerable<TEntity> DoFindAllByLimit(Expression<Func<TEntity, bool>> queryPredicate,
            string sortField, string sortOrder, int offset, int limit,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortString"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        protected abstract IEnumerable<TEntity> DoFindAllByLimit(Expression<Func<TEntity, bool>> queryPredicate,
            string sortString, int offset,
            int limit, params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <returns></returns>
        protected abstract IEnumerable<TEntity> DoFindAllByLimit<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, int offset, int limit,
            params string[] eagerLoadingPaths);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="thenBySortPredicate"></param>
        /// <param name="thenBySortOrder"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <typeparam name="TKey1"></typeparam>
        /// <typeparam name="TKey2"></typeparam>
        /// <returns></returns>
        protected abstract IEnumerable<TEntity> DoFindAllByLimit<TKey1, TKey2>(
            Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey1>> sortPredicate, SortOrder sortOrder,
            Expression<Func<TEntity, TKey2>> thenBySortPredicate, SortOrder thenBySortOrder, int offset,
            int limit, params string[] eagerLoadingPaths);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortField"></param>
        /// <param name="sortOrder"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <returns></returns>
        protected abstract IEnumerable<TEntity> DoFindAllByLimit(Expression<Func<TEntity, bool>> queryPredicate,
            string sortField, string sortOrder, int offset, int limit,
            params string[] eagerLoadingPaths);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortString"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <returns></returns>
        protected abstract IEnumerable<TEntity> DoFindAllByLimit(Expression<Func<TEntity, bool>> queryPredicate,
            string sortString, int offset, int limit,
            params string[] eagerLoadingPaths);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        protected abstract TEntity DoFindById(Guid id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        protected abstract int DoGetCount(Expression<Func<TEntity, bool>> queryPredicate);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        protected abstract TEntity DoFindSelect(Expression<Func<TEntity, bool>> queryPredicate);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="selector"></param>
        /// <typeparam name="TResult"></typeparam>
        /// <returns></returns>
        protected abstract TResult DoFindSelect<TResult>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TResult>> selector);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        protected abstract TEntity DoFindSelect(Expression<Func<TEntity, bool>> queryPredicate,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <returns></returns>
        protected abstract TEntity DoFindSelect(Expression<Func<TEntity, bool>> queryPredicate,
            params string[] eagerLoadingPaths);

        protected abstract TResult DoFindSelect<TResult>(Expression<Func<TEntity, bool>> queryPredicate, Expression<Func<TEntity, TResult>> selector, params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        protected abstract TEntity DoFindFirst<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        protected abstract TEntity DoFindLast<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        protected abstract bool DoExists(Expression<Func<TEntity, bool>> queryPredicate);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        protected abstract long DoSum(Expression<Func<TEntity, long>> expression,
            Expression<Func<TEntity, bool>> queryPredicate = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        protected abstract long? DoSum(Expression<Func<TEntity, long?>> expression,
            Expression<Func<TEntity, bool>> queryPredicate = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        protected abstract int DoSum(Expression<Func<TEntity, int>> expression,
            Expression<Func<TEntity, bool>> queryPredicate = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        protected abstract int? DoSum(Expression<Func<TEntity, int?>> expression,
            Expression<Func<TEntity, bool>> queryPredicate = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        protected abstract decimal DoSum(Expression<Func<TEntity, decimal>> expression,
            Expression<Func<TEntity, bool>> queryPredicate = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        protected abstract decimal? DoSum(Expression<Func<TEntity, decimal?>> expression,
            Expression<Func<TEntity, bool>> queryPredicate = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        protected abstract List<T> DoGroup<T>(Func<TEntity, T> expression,
            Expression<Func<TEntity, bool>> queryPredicate = null);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <returns></returns>
        protected abstract PagedResult<Guid> DoFindIds<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, int pageNumber, int pageSize);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        protected abstract bool DoAdd(TEntity entity, bool isCommit = true);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        protected abstract bool DoDelete(TEntity entity, bool isCommit = true);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        protected abstract bool DoUpdate(TEntity entity, bool isCommit = true);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        protected abstract bool DoAdd(IEnumerable<TEntity> entities, bool isCommit = true);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        protected abstract bool DoUpdate(IEnumerable<TEntity> entities, bool isCommit = true);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        protected abstract bool DoDelete(IEnumerable<TEntity> entities, bool isCommit = true);

        #endregion

        public TEntity FindById(Guid id)
        {
            return DoFindById(id);
        }

        public TEntity Find(Expression<Func<TEntity, bool>> queryPredicate)
        {
            return DoFindSelect(GetExpression(queryPredicate));
        }

        public TEntity FindNoRecord(Expression<Func<TEntity, bool>> queryPredicate)
        {
            return DoFindSelect(queryPredicate);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        public TEntity Find(Expression<Func<TEntity, bool>> queryPredicate,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            return DoFindSelect(GetExpression(queryPredicate), eagerLoadingProperties);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <returns></returns>
        public TEntity Find(Expression<Func<TEntity, bool>> queryPredicate, params string[] eagerLoadingPaths)
        {
            return DoFindSelect(GetExpression(queryPredicate), eagerLoadingPaths);
        }

        public TEntity FindFirst<TKey>(Expression<Func<TEntity, bool>> queryPredicate, Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            return DoFindFirst(queryPredicate, sortPredicate, sortOrder, eagerLoadingProperties);
        }

        public TEntity FindLast<TKey>(Expression<Func<TEntity, bool>> queryPredicate, Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            return DoFindLast(queryPredicate, sortPredicate, sortOrder, eagerLoadingProperties);
        }

        public TResult FindSelect<TResult>(Expression<Func<TEntity, bool>> queryPredicate, Expression<Func<TEntity, TResult>> selector, params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            return DoFindSelect(GetExpression(queryPredicate), selector, eagerLoadingProperties);
        }

        public int GetCount()
        {
            return DoGetCount(GetExpression(null));
        }

        public int GetCount(Expression<Func<TEntity, bool>> queryPredicate)
        {
            return DoGetCount(GetExpression(queryPredicate));
        }

        public bool Exists(Expression<Func<TEntity, bool>> queryPredicate)
        {
            return DoExists(GetExpression(queryPredicate));
        }

        public long Sum(Expression<Func<TEntity, long>> expression,
            Expression<Func<TEntity, bool>> queryPredicate = null)
        {
            return DoSum(expression, GetExpression(queryPredicate));
        }

        public long? Sum(Expression<Func<TEntity, long?>> expression,
            Expression<Func<TEntity, bool>> queryPredicate = null)
        {
            return DoSum(expression, GetExpression(queryPredicate));
        }

        public int Sum(Expression<Func<TEntity, int>> expression, Expression<Func<TEntity, bool>> queryPredicate = null)
        {
            return DoSum(expression, GetExpression(queryPredicate));
        }

        public int? Sum(Expression<Func<TEntity, int?>> expression,
            Expression<Func<TEntity, bool>> queryPredicate = null)
        {
            return DoSum(expression, GetExpression(queryPredicate));
        }

        public decimal Sum(Expression<Func<TEntity, decimal>> expression,
            Expression<Func<TEntity, bool>> queryPredicate = null)
        {
            return DoSum(expression, GetExpression(queryPredicate));
        }

        public decimal? Sum(Expression<Func<TEntity, decimal?>> expression,
            Expression<Func<TEntity, bool>> queryPredicate = null)
        {
            return DoSum(expression, GetExpression(queryPredicate));
        }

        public List<T> Group<T>(Func<TEntity, T> expression, Expression<Func<TEntity, bool>> queryPredicate = null)
        {
            return DoGroup(expression, GetExpression(queryPredicate));
        }


        public PagedResult<Guid> FindIds<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, int pageNumber,
            int pageSize)
        {
            return DoFindIds(GetExpression(queryPredicate), sortPredicate, sortOrder, pageNumber, pageSize);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        protected virtual Expression<Func<TEntity, bool>> GetExpression(Expression<Func<TEntity, bool>> queryPredicate)
        {
            if (queryPredicate != null)
            {
                return DefaultExpression.And(queryPredicate);
            }
            return DefaultExpression;
        }

        public IEnumerable<TEntity> FindAll()
        {
            return DoFindAll(GetExpression(null), DefaultSortPredicate, SortOrder.Unspecified);
        }

        public PagedResult<TEntity> FindAll(int pageNumber, int pageSize)
        {
            return DoFindAll(GetExpression(null), DefaultSortPredicate, SortOrder.Unspecified, pageNumber, pageSize);
        }

        public IEnumerable<TEntity> FindAllByLimit(int offset, int limit)
        {
            return DoFindAllByLimit(GetExpression(null), DefaultSortPredicate, SortOrder.Unspecified, offset, limit);
        }

        public IEnumerable<TEntity> FindAll<TKey>(Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder)
        {
            return DoFindAll(GetExpression(null), sortPredicate, sortOrder);
        }

        public IEnumerable<TEntity> FindAll(string sortField, string sortOrder)
        {
            return DoFindAll(GetExpression(null), sortField, sortOrder);
        }

        public PagedResult<TEntity> FindAll<TKey>(Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            int pageNumber, int pageSize)
        {
            return DoFindAll(GetExpression(null), sortPredicate, sortOrder, pageNumber, pageSize);
        }

        public PagedResult<TEntity> FindAll(string sortField, string sortOrder, int pageNumber, int pageSize)
        {
            return DoFindAll(GetExpression(null), sortField, sortOrder, pageNumber, pageSize);
        }

        public IEnumerable<TEntity> FindAllByLimit<TKey>(Expression<Func<TEntity, TKey>> sortPredicate,
            SortOrder sortOrder, int offset, int limit)
        {
            return DoFindAllByLimit(GetExpression(null), sortPredicate, sortOrder, offset, limit);
        }

        public IEnumerable<TEntity> FindAllByLimit(string sortField, string sortOrder, int offset, int limit)
        {
            return DoFindAllByLimit(GetExpression(null), sortField, sortOrder, offset, limit);
        }

        public IEnumerable<TEntity> FindAll(Expression<Func<TEntity, bool>> queryPredicate)
        {
            return DoFindAll(GetExpression(queryPredicate), DefaultSortPredicate, SortOrder.Unspecified);
        }

        /// <summary>
        /// 查询不包括Record信息，-1数据也可以查询出来
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        public PagedResult<TEntity> FindAllNoRecord(Expression<Func<TEntity, bool>> queryPredicate, string sortField,
           string sortOrder, int pageNumber,
           int pageSize)
        {
            return DoFindAll(queryPredicate, sortField, sortOrder, pageNumber, pageSize);
        }
        /// <summary>
        /// 查询不包括Record信息，-1数据也可以查询出来
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        public IEnumerable<TEntity> FindAllNoRecord(Expression<Func<TEntity, bool>> queryPredicate)
        {
            return DoFindAll(queryPredicate, DefaultSortPredicate, SortOrder.Unspecified);
        }

        public PagedResult<TEntity> FindAll(Expression<Func<TEntity, bool>> queryPredicate, int pageNumber, int pageSize)
        {
            return DoFindAll(GetExpression(queryPredicate), DefaultSortPredicate, SortOrder.Unspecified, pageNumber,
                pageSize);
        }

        public PagedResult<TResult> FindAll<TResult>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TResult>> selector, int pageNumber, int pageSize)
        {
            return DoFindAll(GetExpression(queryPredicate), DefaultSortPredicate, SortOrder.Unspecified, selector,
                pageNumber, pageSize);
        }

        public IEnumerable<TEntity> FindAllByLimit(Expression<Func<TEntity, bool>> queryPredicate, int offset, int limit)
        {
            return DoFindAllByLimit(GetExpression(queryPredicate), DefaultSortPredicate, SortOrder.Unspecified, offset,
                limit);
        }

        public IEnumerable<TEntity> FindAll<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder)
        {
            return DoFindAll(GetExpression(queryPredicate), sortPredicate, sortOrder);
        }

        public IEnumerable<TEntity> FindAll(Expression<Func<TEntity, bool>> queryPredicate, string sortField,
            string sortOrder)
        {
            return DoFindAll(GetExpression(queryPredicate), sortField, sortOrder);
        }

        public PagedResult<TEntity> FindAll<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, int pageNumber,
            int pageSize)
        {
            return DoFindAll(GetExpression(queryPredicate), sortPredicate, sortOrder, pageNumber, pageSize);
        }

        public PagedResult<TResult> FindAll<TKey, TResult>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            Expression<Func<TEntity, TResult>> selector, int pageNumber, int pageSize)
        {
            return DoFindAll(GetExpression(queryPredicate), sortPredicate, sortOrder, selector, pageNumber, pageSize);
        }

        public PagedResult<TEntity> FindAll(Expression<Func<TEntity, bool>> queryPredicate, string sortField,
            string sortOrder, int pageNumber,
            int pageSize)
        {
            return DoFindAll(GetExpression(queryPredicate), sortField, sortOrder, pageNumber, pageSize);
        }

        public PagedResult<TResult> FindAll<TResult>(Expression<Func<TEntity, bool>> queryPredicate, string sortField,
            string sortOrder, Expression<Func<TEntity, TResult>> selector, int pageNumber, int pageSize)
        {
            return DoFindAll(GetExpression(queryPredicate), sortField, sortOrder, selector, pageNumber,
                pageSize);
        }

        public IEnumerable<TEntity> FindAllByLimit<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, int offset,
            int limit)
        {
            return DoFindAllByLimit(GetExpression(queryPredicate), sortPredicate, sortOrder, offset, limit);
        }

        public IEnumerable<TResult> FindAllByLimit<TKey, TResult>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            Expression<Func<TEntity, TResult>> selector, int offset, int limit)
        {
            return DoFindAllByLimit(GetExpression(queryPredicate), sortPredicate, sortOrder, selector, offset, limit);
        }

        public IEnumerable<TEntity> FindAllByLimit(Expression<Func<TEntity, bool>> queryPredicate, string sortField,
            string sortOrder, int offset,
            int limit)
        {
            return DoFindAllByLimit(GetExpression(queryPredicate), sortField, sortOrder, offset, limit);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        public IEnumerable<TEntity> FindAll(params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            return DoFindAll(GetExpression(null), DefaultSortPredicate, SortOrder.Unspecified, eagerLoadingProperties);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="eagerLoadingPaths"></param>
        /// <returns></returns>
        public IEnumerable<TEntity> FindAll(params string[] eagerLoadingPaths)
        {
            return DoFindAll(GetExpression(null), DefaultSortPredicate, SortOrder.Unspecified, eagerLoadingPaths);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        public IEnumerable<TEntity> FindAll(int pageNumber, int pageSize,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            return DoFindAll(GetExpression(null), DefaultSortPredicate, SortOrder.Unspecified, pageNumber, pageSize,
                eagerLoadingProperties);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <returns></returns>
        public IEnumerable<TEntity> FindAll(int pageNumber, int pageSize, params string[] eagerLoadingPaths)
        {
            return DoFindAll(GetExpression(null), DefaultSortPredicate, SortOrder.Unspecified, pageNumber, pageSize,
                eagerLoadingPaths);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <returns></returns>
        public IEnumerable<TEntity> FindAll<TKey>(Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            return DoFindAll(GetExpression(null), sortPredicate, sortOrder, eagerLoadingProperties);
        }

        public IEnumerable<TEntity> FindAll(string sortField, string sortOrder,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            return DoFindAll(GetExpression(null), sortField, sortOrder, eagerLoadingProperties);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <returns></returns>
        public IEnumerable<TEntity> FindAll<TKey>(Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            params string[] eagerLoadingPaths)
        {
            return DoFindAll(GetExpression(null), sortPredicate, sortOrder, eagerLoadingPaths);
        }

        public IEnumerable<TEntity> FindAll(string sortField, string sortOrder, params string[] eagerLoadingPaths)
        {
            return DoFindAll(GetExpression(null), sortField, sortOrder, eagerLoadingPaths);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <returns></returns>
        public PagedResult<TEntity> FindAll<TKey>(Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            int pageNumber, int pageSize,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            return DoFindAll(GetExpression(null), sortPredicate, sortOrder, pageNumber, pageSize, eagerLoadingProperties);
        }

        public PagedResult<TEntity> FindAll(string sortField, string sortOrder, int pageNumber, int pageSize,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            return DoFindAll(GetExpression(null), sortField, sortOrder, pageNumber, pageSize, eagerLoadingProperties);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <returns></returns>
        public PagedResult<TEntity> FindAll<TKey>(Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            int pageNumber, int pageSize,
            params string[] eagerLoadingPaths)
        {
            return DoFindAll(GetExpression(null), sortPredicate, sortOrder, pageNumber, pageSize, eagerLoadingPaths);
        }

        public PagedResult<TEntity> FindAll(string sortField, string sortOrder, int pageNumber, int pageSize,
            params string[] eagerLoadingPaths)
        {
            return DoFindAll(GetExpression(null), sortField, sortOrder, pageNumber, pageSize, eagerLoadingPaths);
        }

        public IEnumerable<TEntity> FindAllByLimit<TKey>(Expression<Func<TEntity, TKey>> sortPredicate,
            SortOrder sortOrder, int offset, int limit,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            return DoFindAllByLimit(GetExpression(null), sortPredicate, sortOrder, offset, limit, eagerLoadingProperties);
        }

        public IEnumerable<TEntity> FindAllByLimit(string sortField, string sortOrder, int offset, int limit,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            return DoFindAllByLimit(GetExpression(null), sortField, sortOrder, offset, limit, eagerLoadingProperties);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <returns></returns>
        public IEnumerable<TEntity> FindAllByLimit<TKey>(Expression<Func<TEntity, TKey>> sortPredicate,
            SortOrder sortOrder, int offset, int limit,
            params string[] eagerLoadingPaths)
        {
            return DoFindAllByLimit(GetExpression(null), sortPredicate, sortOrder, offset, limit, eagerLoadingPaths);
        }

        public IEnumerable<TEntity> FindAllByLimit(string sortField, string sortOrder, int offset, int limit,
            params string[] eagerLoadingPaths)
        {
            return DoFindAllByLimit(GetExpression(null), sortField, sortOrder, offset, limit, eagerLoadingPaths);
        }

        public IEnumerable<TEntity> FindAll(Expression<Func<TEntity, bool>> queryPredicate,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            return DoFindAll(GetExpression(queryPredicate), DefaultSortPredicate, SortOrder.Unspecified,
                eagerLoadingProperties);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <returns></returns>
        public IEnumerable<TEntity> FindAll(Expression<Func<TEntity, bool>> queryPredicate,
            params string[] eagerLoadingPaths)
        {
            return DoFindAll(GetExpression(queryPredicate), DefaultSortPredicate, SortOrder.Unspecified,
                eagerLoadingPaths);
        }

        public PagedResult<TEntity> FindAll(Expression<Func<TEntity, bool>> queryPredicate, int pageNumber, int pageSize,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            return DoFindAll(GetExpression(queryPredicate), DefaultSortPredicate, SortOrder.Unspecified, pageNumber,
                pageSize, eagerLoadingProperties);
        }

        public PagedResult<TEntity> FindAll(Expression<Func<TEntity, bool>> queryPredicate, int pageNumber, int pageSize,
            params string[] eagerLoadingPaths)
        {
            return DoFindAll(GetExpression(queryPredicate), DefaultSortPredicate, SortOrder.Unspecified, pageNumber,
                pageSize, eagerLoadingPaths);
        }

        public IEnumerable<TEntity> FindAllByLimit(Expression<Func<TEntity, bool>> queryPredicate, int offset, int limit,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            return DoFindAllByLimit(GetExpression(queryPredicate), DefaultSortPredicate, SortOrder.Unspecified, offset,
                limit,
                eagerLoadingProperties);
        }

        public IEnumerable<TEntity> FindAllByLimit(Expression<Func<TEntity, bool>> queryPredicate, int offset, int limit,
            params string[] eagerLoadingPaths)
        {
            return DoFindAllByLimit(GetExpression(queryPredicate), DefaultSortPredicate, SortOrder.Unspecified, offset,
                limit,
                eagerLoadingPaths);
        }

        public IEnumerable<TEntity> FindAll<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            return DoFindAll(GetExpression(queryPredicate), sortPredicate, sortOrder, eagerLoadingProperties);
        }

        public IEnumerable<TEntity> FindAll(Expression<Func<TEntity, bool>> queryPredicate, string sortField,
            string sortOrder,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            return DoFindAll(GetExpression(queryPredicate), sortField, sortOrder, eagerLoadingProperties);
        }

        public IEnumerable<TEntity> FindAll<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            params string[] eagerLoadingPaths)
        {
            return DoFindAll(GetExpression(queryPredicate), sortPredicate, sortOrder, eagerLoadingPaths);
        }

        public IEnumerable<TEntity> FindAll(Expression<Func<TEntity, bool>> queryPredicate, string sortField,
            string sortOrder,
            params string[] eagerLoadingPaths)
        {
            return DoFindAll(GetExpression(queryPredicate), sortField, sortOrder, eagerLoadingPaths);
        }

        public PagedResult<TEntity> FindAll<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, int pageNumber,
            int pageSize, params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            return DoFindAll(GetExpression(queryPredicate), sortPredicate, sortOrder, pageNumber, pageSize,
                eagerLoadingProperties);
        }

        public PagedResult<TEntity> FindAll(Expression<Func<TEntity, bool>> queryPredicate, string sortField,
            string sortOrder, int pageNumber,
            int pageSize, params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            return DoFindAll(GetExpression(queryPredicate), sortField, sortOrder, pageNumber, pageSize,
                eagerLoadingProperties);
        }

        public PagedResult<TEntity> FindAll(Expression<Func<TEntity, bool>> queryPredicate, string sortString,
            int pageNumber, int pageSize,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            return DoFindAll(GetExpression(queryPredicate), sortString, pageNumber, pageSize, eagerLoadingProperties);
        }

        public PagedResult<TEntity> FindAll<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, int pageNumber,
            int pageSize, params string[] eagerLoadingPaths)
        {
            return DoFindAll(GetExpression(queryPredicate), sortPredicate, sortOrder, pageNumber, pageSize,
                eagerLoadingPaths);
        }

        public PagedResult<TEntity> FindAll<TKey1, TKey2>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey1>> sortPredicate, SortOrder sortOrder,
            Expression<Func<TEntity, TKey2>> thenBySortPredicate, SortOrder thenBySortOrder, int pageNumber,
            int pageSize,
            params string[] eagerLoadingPaths)
        {
            return DoFindAll(GetExpression(queryPredicate), sortPredicate, sortOrder, thenBySortPredicate,
                thenBySortOrder, pageNumber, pageSize, eagerLoadingPaths);
        }

        public PagedResult<TEntity> FindAll(Expression<Func<TEntity, bool>> queryPredicate, string sortField,
            string sortOrder, int pageNumber,
            int pageSize, params string[] eagerLoadingPaths)
        {
            return DoFindAll(GetExpression(queryPredicate), sortField, sortOrder, pageNumber, pageSize,
                eagerLoadingPaths);
        }

        public IEnumerable<TEntity> FindAllByLimit<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, int offset,
            int limit, params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            return DoFindAllByLimit(GetExpression(queryPredicate), sortPredicate, sortOrder, offset, limit,
                eagerLoadingProperties);
        }

        public IEnumerable<TEntity> FindAllByLimit(Expression<Func<TEntity, bool>> queryPredicate, string sortField,
            string sortOrder, int offset,
            int limit, params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties)
        {
            return DoFindAllByLimit(GetExpression(queryPredicate), sortField, sortOrder, offset, limit,
                eagerLoadingProperties);
        }

        public IEnumerable<TEntity> FindAllByLimit<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, int offset,
            int limit, params string[] eagerLoadingPaths)
        {
            return DoFindAllByLimit(GetExpression(queryPredicate), sortPredicate, sortOrder, offset, limit,
                eagerLoadingPaths);
        }

        public IEnumerable<TEntity> FindAllByLimit(Expression<Func<TEntity, bool>> queryPredicate, string sortField,
            string sortOrder, int offset,
            int limit, params string[] eagerLoadingPaths)
        {
            return DoFindAllByLimit(GetExpression(queryPredicate), sortField, sortOrder, offset, limit,
                eagerLoadingPaths);
        }

        public bool Add(TEntity entity, bool isCommit = true)
        {
            entity.CreationTime = DateTime.Now;
            entity.ModifyTime = DateTime.Now;
            entity.Record = 1;
            return DoAdd(entity, isCommit);
        }

        public bool Update(TEntity entity, bool isCommit = true)
        {
            entity.ModifyTime = DateTime.Now;
            return DoUpdate(entity, isCommit);
        }

        public bool Delete(TEntity entity, bool isCommit = true)
        {
            entity.ModifyTime = DateTime.Now;
            entity.Record = -1;
            return DoUpdate(entity, isCommit);
        }
        public bool DeleteById(Guid id, bool isCommit = true)
        {
            var entity = FindById(id);
            if (entity == null) return false;
            entity.ModifyTime = DateTime.Now;
            entity.Record = -1;
            return DoUpdate(entity, isCommit);
        }

        public bool DeletePhysicalById(Guid id, bool isCommit = true)
        {
            var entity = FindById(id);
            if (entity == null) return false;
            return DoDelete(entity, isCommit);
        }
        public bool DeletePhysical(TEntity entity, bool isCommit = true)
        {
            return DoDelete(entity, isCommit);
        }

        public bool Add(IEnumerable<TEntity> entities, bool isCommit = true)
        {
            var enumerable = entities as TEntity[] ?? entities.ToArray();
            foreach (var entity in enumerable)
            {
                if (entity.CreationTime == DateTime.MinValue)
                    entity.CreationTime = DateTime.Now;
                if (entity.ModifyTime == DateTime.MinValue)
                    entity.ModifyTime = DateTime.Now;
                if (entity.Record == 0)
                    entity.Record = 1;
            }
            return DoAdd(enumerable, isCommit);
        }

        public bool Delete(IEnumerable<TEntity> entities, bool isCommit = true)
        {
            var enumerable = entities as TEntity[] ?? entities.ToArray();
            foreach (var entity in enumerable)
            {
                entity.ModifyTime = DateTime.Now;
                entity.Record = -1;
            }
            return DoUpdate(enumerable, isCommit);
        }

        public bool DeletePhysical(IEnumerable<TEntity> entities, bool isCommit = true)
        {
            return DoDelete(entities, isCommit);
        }


        public bool Update(IEnumerable<TEntity> entities, bool isCommit = true)
        {
            var enumerable = entities as TEntity[] ?? entities.ToArray();
            foreach (var entity in enumerable)
            {
                entity.ModifyTime = DateTime.Now;
            }
            return DoUpdate(enumerable, isCommit);
        }
    }
}
