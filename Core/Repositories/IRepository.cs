﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;

namespace Core.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public interface IRepository { }
    /// <summary>
    /// 仓储接口
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IRepository<TEntity> : IRepository where TEntity : IEntity
    {
        /// <summary>
        /// 仓储上下文
        /// </summary>
        IRepositoryContext Context { get; }

        #region 查询

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TEntity FindById(Guid id);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        TEntity Find(Expression<Func<TEntity, bool>> queryPredicate);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        TEntity FindNoRecord(Expression<Func<TEntity, bool>> queryPredicate);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        TEntity Find(Expression<Func<TEntity, bool>> queryPredicate,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <returns></returns>
        TEntity Find(Expression<Func<TEntity, bool>> queryPredicate,
            params string[] eagerLoadingPaths);

        TEntity FindFirst<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        TEntity FindLast<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        TResult FindSelect<TResult>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TResult>> selector,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        int GetCount();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        int GetCount(Expression<Func<TEntity, bool>> queryPredicate);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        List<T> Group<T>(Func<TEntity, T> expression, Expression<Func<TEntity, bool>> queryPredicate = null);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        bool Exists(Expression<Func<TEntity, bool>> queryPredicate);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        long Sum(Expression<Func<TEntity, long>> expression,
Expression<Func<TEntity, bool>> queryPredicate = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        long? Sum(Expression<Func<TEntity, long?>> expression,
Expression<Func<TEntity, bool>> queryPredicate = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        int Sum(Expression<Func<TEntity, int>> expression,
    Expression<Func<TEntity, bool>> queryPredicate = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        int? Sum(Expression<Func<TEntity, int?>> expression,
    Expression<Func<TEntity, bool>> queryPredicate = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        decimal Sum(Expression<Func<TEntity, decimal>> expression,
    Expression<Func<TEntity, bool>> queryPredicate = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        decimal? Sum(Expression<Func<TEntity, decimal?>> expression,
            Expression<Func<TEntity, bool>> queryPredicate = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <returns></returns>
        PagedResult<Guid> FindIds<TKey>(Expression<Func<TEntity, bool>> queryPredicate, Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, int pageNumber, int pageSize);
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<TEntity> FindAll();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        PagedResult<TEntity> FindAll(int pageNumber, int pageSize);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAllByLimit(int offset, int limit);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAll<TKey>(Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortField"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAll(string sortField, string sortOrder);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        PagedResult<TEntity> FindAll<TKey>(Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            int pageNumber, int pageSize);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortField"></param>
        /// <param name="sortOrder"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        PagedResult<TEntity> FindAll(string sortField, string sortOrder,
    int pageNumber, int pageSize);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAllByLimit<TKey>(Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            int offset, int limit);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortField"></param>
        /// <param name="sortOrder"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAllByLimit(string sortField, string sortOrder,
          int offset, int limit);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        /// 
        /// 
        PagedResult<TEntity> FindAllNoRecord(Expression<Func<TEntity, bool>> queryPredicate, string sortField, string sortOrder, int pageNumber, int pageSize);

        IEnumerable<TEntity> FindAllNoRecord(Expression<Func<TEntity, bool>> queryPredicate);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAll(Expression<Func<TEntity, bool>> queryPredicate);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        PagedResult<TEntity> FindAll(Expression<Func<TEntity, bool>> queryPredicate, int pageNumber, int pageSize);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="selector"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <typeparam name="TResult"></typeparam>
        /// <returns></returns>
        PagedResult<TResult> FindAll<TResult>(Expression<Func<TEntity, bool>> queryPredicate, Expression<Func<TEntity, TResult>> selector, int pageNumber, int pageSize);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAllByLimit(Expression<Func<TEntity, bool>> queryPredicate, int offset, int limit);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAll<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortField"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAll(Expression<Func<TEntity, bool>> queryPredicate, string sortField,
            string sortOrder);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        PagedResult<TEntity> FindAll<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, int pageNumber, int pageSize);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="selector"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <returns></returns>
        PagedResult<TResult> FindAll<TKey, TResult>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, Expression<Func<TEntity, TResult>> selector,
            int pageNumber, int pageSize);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortField"></param>
        /// <param name="sortOrder"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        PagedResult<TEntity> FindAll(Expression<Func<TEntity, bool>> queryPredicate,
            string sortField, string sortOrder, int pageNumber, int pageSize);

        PagedResult<TResult> FindAll<TResult>(Expression<Func<TEntity, bool>> queryPredicate, string sortField,
            string sortOrder, Expression<Func<TEntity, TResult>> selector, int pageNumber, int pageSize);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAllByLimit<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, int offset, int limit);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="selector"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <returns></returns>
        IEnumerable<TResult> FindAllByLimit<TKey, TResult>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, Expression<Func<TEntity, TResult>> selector, int offset, int limit);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortField"></param>
        /// <param name="sortOrder"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAllByLimit(Expression<Func<TEntity, bool>> queryPredicate, string sortField,
            string sortOrder, int offset, int limit);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAll(params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="eagerLoadingPaths"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAll(params string[] eagerLoadingPaths);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAll(int pageNumber, int pageSize,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAll(int pageNumber, int pageSize, params string[] eagerLoadingPaths);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAll<TKey>(Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortField"></param>
        /// <param name="sortOrder"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAll(string sortField, string sortOrder,
    params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <returns></returns>
        IEnumerable<TEntity> FindAll<TKey>(Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            params string[] eagerLoadingPaths);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortField"></param>
        /// <param name="sortOrder"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAll(string sortField, string sortOrder,
    params string[] eagerLoadingPaths);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        PagedResult<TEntity> FindAll<TKey>(Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            int pageNumber, int pageSize, params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortField">排序字段</param>
        /// <param name="sortOrder">排序发送</param>
        /// <param name="pageNumber">当前页</param>
        /// <param name="pageSize">每页数量</param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        PagedResult<TEntity> FindAll(string sortField, string sortOrder,
    int pageNumber, int pageSize, params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <returns></returns>
        PagedResult<TEntity> FindAll<TKey>(Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            int pageNumber, int pageSize, params string[] eagerLoadingPaths);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortField"></param>
        /// <param name="sortOrder"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <returns></returns>
        PagedResult<TEntity> FindAll(string sortField, string sortOrder,
    int pageNumber, int pageSize, params string[] eagerLoadingPaths);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAllByLimit<TKey>(Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            int offset, int limit, params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortField"></param>
        /// <param name="sortOrder"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAllByLimit(string sortField, string sortOrder,
    int offset, int limit, params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <returns></returns>
        IEnumerable<TEntity> FindAllByLimit<TKey>(Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            int offset, int limit, params string[] eagerLoadingPaths);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sortField"></param>
        /// <param name="sortOrder"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAllByLimit(string sortField, string sortOrder,
    int offset, int limit, params string[] eagerLoadingPaths);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAll(Expression<Func<TEntity, bool>> queryPredicate,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAll(Expression<Func<TEntity, bool>> queryPredicate, params string[] eagerLoadingPaths);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        PagedResult<TEntity> FindAll(Expression<Func<TEntity, bool>> queryPredicate, int pageNumber, int pageSize,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate">查询条件</param>
        /// <param name="pageNumber">当前页</param>
        /// <param name="pageSize">页数</param>
        /// <param name="eagerLoadingPaths"></param>
        /// <returns></returns>
        PagedResult<TEntity> FindAll(Expression<Func<TEntity, bool>> queryPredicate, int pageNumber, int pageSize,
            params string[] eagerLoadingPaths);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAllByLimit(Expression<Func<TEntity, bool>> queryPredicate, int offset, int limit,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAllByLimit(Expression<Func<TEntity, bool>> queryPredicate, int offset, int limit,
            params string[] eagerLoadingPaths);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAll<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortField"></param>
        /// <param name="sortOrder"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAll(Expression<Func<TEntity, bool>> queryPredicate,
            string sortField, string sortOrder,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <returns></returns>
        IEnumerable<TEntity> FindAll<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder,
            params string[] eagerLoadingPaths);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortField"></param>
        /// <param name="sortOrder"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAll(Expression<Func<TEntity, bool>> queryPredicate,
            string sortField, string sortOrder,
            params string[] eagerLoadingPaths);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        PagedResult<TEntity> FindAll<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, int pageNumber, int pageSize,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortField"></param>
        /// <param name="sortOrder"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        PagedResult<TEntity> FindAll(Expression<Func<TEntity, bool>> queryPredicate,
            string sortField, string sortOrder, int pageNumber, int pageSize,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortString"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        PagedResult<TEntity> FindAll(Expression<Func<TEntity, bool>> queryPredicate,
            string sortString, int pageNumber, int pageSize,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <returns></returns>
        PagedResult<TEntity> FindAll<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, int pageNumber,
            int pageSize, params string[] eagerLoadingPaths);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="thenBySortOrder"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <param name="thenBySortPredicate"></param>
        /// <typeparam name="TKey1"></typeparam>
        /// <typeparam name="TKey2"></typeparam>
        /// <returns></returns>
        PagedResult<TEntity> FindAll<TKey1, TKey2>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey1>> sortPredicate, SortOrder sortOrder, Expression<Func<TEntity, TKey2>> thenBySortPredicate, SortOrder thenBySortOrder, int pageNumber,
            int pageSize, params string[] eagerLoadingPaths);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortField"></param>
        /// <param name="sortOrder"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <returns></returns>
        PagedResult<TEntity> FindAll(Expression<Func<TEntity, bool>> queryPredicate,
            string sortField, string sortOrder, int pageNumber,
            int pageSize, params string[] eagerLoadingPaths);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAllByLimit<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, int offset, int limit,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortField"></param>
        /// <param name="sortOrder"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="eagerLoadingProperties"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAllByLimit(Expression<Func<TEntity, bool>> queryPredicate,
            string sortField, string sortOrder, int offset, int limit,
            params Expression<Func<TEntity, dynamic>>[] eagerLoadingProperties);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortPredicate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <typeparam name="TKey"></typeparam>
        /// <returns></returns>
        IEnumerable<TEntity> FindAllByLimit<TKey>(Expression<Func<TEntity, bool>> queryPredicate,
            Expression<Func<TEntity, TKey>> sortPredicate, SortOrder sortOrder, int offset,
            int limit, params string[] eagerLoadingPaths);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryPredicate"></param>
        /// <param name="sortField"></param>
        /// <param name="sortOrder"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="eagerLoadingPaths"></param>
        /// <returns></returns>
        IEnumerable<TEntity> FindAllByLimit(Expression<Func<TEntity, bool>> queryPredicate,
    string sortField, string sortOrder, int offset,
    int limit, params string[] eagerLoadingPaths);

        #endregion

        #region 修改

        ///// <summary>
        ///// 修改实体状态以及提交
        ///// </summary>
        ///// <param name="entity">实体</param>
        ///// <param name="isCommit">是否提交</param>
        ///// <returns>是否成功</returns>
        //bool SetEntity(TEntity entity, bool isCommit = true);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        bool Add(TEntity entity, bool isCommit = true);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        bool Update(TEntity entity, bool isCommit = true);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        bool Delete(TEntity entity, bool isCommit = true);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        bool DeleteById(Guid id, bool isCommit = true);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        bool DeletePhysicalById(Guid id, bool isCommit = true);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        bool DeletePhysical(TEntity entity, bool isCommit = true);

        ///// <summary>
        ///// 修改实体状态以及提交
        ///// </summary>
        ///// <param name="entities">实体集合</param>
        ///// <param name="isCommit">IsCommit</param>
        ///// <returns>是否成功</returns>
        //bool SetEntities(IEnumerable<TEntity> entities, bool isCommit = true);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        bool Add(IEnumerable<TEntity> entities, bool isCommit = true);

        /// <summary>
        /// 更新实体
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        bool Update(IEnumerable<TEntity> entities, bool isCommit = true);

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="entities"></param>
        ///// <param name="isCommit"></param>
        ///// <param name="excludedProperties"></param>
        ///// <returns></returns>
        //bool UpdateExclude(IEnumerable<TEntity> entities, bool isCommit = true,
        //    params Expression<Func<TEntity, dynamic>>[] excludedProperties);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        bool Delete(IEnumerable<TEntity> entities, bool isCommit = true);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="isCommit"></param>
        /// <returns></returns>
        bool DeletePhysical(IEnumerable<TEntity> entities, bool isCommit = true);



        #endregion
    }
}