﻿using System;
using System.Collections.Generic;

namespace Core.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public interface IRepositoryContext : IUnitOfWork, IDisposable
    {
        /// <summary>
        /// Gets the unique-identifier of the repository context.
        /// </summary>
        string Id { get; }
        /// <summary>
        /// Registers a new object to the repository context.
        /// </summary>
        /// <typeparam name="TEntity">The type of the aggregate root.</typeparam>
        /// <param name="obj">The object to be registered.</param>
        void RegisterNew<TEntity>(TEntity obj)
            where TEntity : class, IEntity;
        /// <summary>
        /// Registers new objects to the repositories context.
        /// </summary>
        /// <param name="entities"></param>
        /// <typeparam name="TEntity"></typeparam>
        void RegisterNew<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IEntity;
        /// <summary>
        /// Registers a modified object to the repository context.
        /// </summary>
        /// <typeparam name="TEntity">The type of the aggregate root.</typeparam>
        /// <param name="obj">The object to be registered.</param>
        void RegisterModified<TEntity>(TEntity obj)
            where TEntity : class, IEntity;
        /// <summary>
        /// Registers a deleted object to the repository context.
        /// </summary>
        /// <typeparam name="TEntity">The type of the aggregate root.</typeparam>
        /// <param name="obj">The object to be registered.</param>
        void RegisterDeleted<TEntity>(TEntity obj)
            where TEntity : class, IEntity;

        /// <summary>
        /// Registers deleted objects to the repository context.
        /// </summary>
        /// <typeparam name="TEntity">The type of the aggregate root.</typeparam>
        void RegisterDeleted<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IEntity;
    }
}
