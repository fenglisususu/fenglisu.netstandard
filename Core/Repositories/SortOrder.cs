﻿using Core.Extensions;

namespace Core.Repositories
{
    /// <summary>
    /// Represents the sorting style.
    /// </summary>
    public enum SortOrder
    {
        /// <summary>
        /// Indicates that the sorting style is not specified.
        /// </summary>
        Unspecified = -1,
        /// <summary>
        /// Indicates an ascending sorting.
        /// </summary>
        Ascending = 0,
        /// <summary>
        /// Indicates a descending sorting.
        /// </summary>
        Descending = 1
    }

    /// <summary>
    /// 字符串转枚举
    /// </summary>
    public static class SortOrderHandler
    {
        public static SortOrder ToSortOrder(this string sortOrder)
        {
            return sortOrder.ToStringExpand().ToLower() != "desc" ? SortOrder.Ascending : SortOrder.Descending;
        }
    }
}
