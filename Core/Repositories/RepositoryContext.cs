﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Core.Repositories
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class RepositoryContext : DisposableObject, IRepositoryContext
    {
        private readonly string _id = Guid.NewGuid().ToString();
        private readonly ThreadLocal<Dictionary<Guid, object>> _localNewCollection = new ThreadLocal<Dictionary<Guid, object>>(() => new Dictionary<Guid, object>());
        private readonly ThreadLocal<Dictionary<Guid, object>> _localModifiedCollection = new ThreadLocal<Dictionary<Guid, object>>(() => new Dictionary<Guid, object>());
        private readonly ThreadLocal<Dictionary<Guid, object>> _localDeletedCollection = new ThreadLocal<Dictionary<Guid, object>>(() => new Dictionary<Guid, object>());
        private readonly ThreadLocal<bool> _localCommitted = new ThreadLocal<bool>(() => true);
        /// <summary>
        /// Clears all the registration in the repository context.
        /// </summary>
        /// <remarks>Note that this can only be called after the repository context has successfully committed.</remarks>
        protected void ClearRegistrations()
        {
            _localNewCollection.Value.Clear();
            _localModifiedCollection.Value.Clear();
            _localDeletedCollection.Value.Clear();
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _localCommitted.Dispose();
                _localDeletedCollection.Dispose();
                _localModifiedCollection.Dispose();
                _localNewCollection.Dispose();
            }
        }
        /// <summary>
        /// Gets an enumerator which iterates over the collection that contains all the objects need to be added to the repository.
        /// </summary>
        protected IEnumerable<KeyValuePair<Guid, object>> NewCollection
        {
            get { return _localNewCollection.Value; }
        }
        /// <summary>
        /// Gets an enumerator which iterates over the collection that contains all the objects need to be modified in the repository.
        /// </summary>
        protected IEnumerable<KeyValuePair<Guid, object>> ModifiedCollection
        {
            get { return _localModifiedCollection.Value; }
        }
        /// <summary>
        /// Gets an enumerator which iterates over the collection that contains all the objects need to be deleted from the repository.
        /// </summary>
        protected IEnumerable<KeyValuePair<Guid, object>> DeletedCollection
        {
            get { return _localDeletedCollection.Value; }
        }

        public bool Committed
        {
            get { return _localCommitted.Value; }
            protected set { _localCommitted.Value = value; }
        }

        public abstract bool Commit();

        public abstract void Rollback();

        public string Id
        {
            get { return _id; }
        }

        public virtual void RegisterNew<TEntity>(TEntity obj) where TEntity : class, IEntity
        {
            //if (modifiedCollection.ContainsKey(obj.ID))
            if (_localModifiedCollection.Value.ContainsKey(obj.Id))
            {
                throw new InvalidOperationException("The object cannot be registered as a new object since it was marked as modified.");

            }
            if (_localNewCollection.Value.ContainsKey(obj.Id))
            {
                throw new InvalidOperationException("The object has already been registered as a new object.");
            }

            _localNewCollection.Value.Add(obj.Id, obj);
            _localCommitted.Value = false;
        }

        public abstract void RegisterNew<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IEntity;

        public virtual void RegisterModified<TEntity>(TEntity obj) where TEntity : class, IEntity
        {
            if (_localDeletedCollection.Value.ContainsKey(obj.Id))
                throw new InvalidOperationException("The object cannot be registered as a modified object since it was marked as deleted.");
            if (!_localModifiedCollection.Value.ContainsKey(obj.Id) && !_localNewCollection.Value.ContainsKey(obj.Id))
                _localModifiedCollection.Value.Add(obj.Id, obj);
            _localCommitted.Value = false;
        }

        public virtual void RegisterDeleted<TEntity>(TEntity obj) where TEntity : class, IEntity
        {
            if (_localNewCollection.Value.ContainsKey(obj.Id))
            {
                if (_localNewCollection.Value.Remove(obj.Id))
                    return;
            }
            bool removedFromModified = _localModifiedCollection.Value.Remove(obj.Id);
            bool addedToDeleted = false;
            if (!_localDeletedCollection.Value.ContainsKey(obj.Id))
            {
                _localDeletedCollection.Value.Add(obj.Id, obj);
                addedToDeleted = true;
            }
            _localCommitted.Value = !(removedFromModified || addedToDeleted);
        }

        public abstract void RegisterDeleted<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IEntity;
    }
}
