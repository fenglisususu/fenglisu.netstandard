﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Core.Extensions
{
    public enum Relation
    {
        Equal,
        NotEqual,
        GreaterThan,
        GreaterThanOrEqual,
        LessThan,
        LessThanOrEqual,
        Contains,
        In,
        InOr,
        InAnd,
    }
    /// <summary>
    /// 动态表达式辅助类
    /// </summary>
    public static class ExpressionHelper
    {


        /// <summary>
        /// 参数表达式缓存
        /// </summary>
        private static readonly Dictionary<Type, ParameterExpression> TypeParamExpMap = new Dictionary<Type, ParameterExpression>();

        /// <summary>
        /// 合并两个表达式树
        /// </summary>
        /// <typeparam name="T"><peparam>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public static Expression<T> Compose<T>(this Expression<T> first, Expression<T> second, Func<Expression, Expression, Expression> merge)
        {
            if (second == null)
                return first;
            // build parameter map (from parameters of second to parameters of first)
            var map = first.Parameters.Select((f, i) => new { f, s = second.Parameters[i] }).ToDictionary(p => p.s, p => p.f);

            // replace parameters in the second lambda expression with parameters from the first
            var secondBody = ParameterRebinder.ReplaceParameters(map, second.Body);

            // apply composition of lambda expression bodies to parameters from the first expression 
            return Expression.Lambda<T>(merge(first.Body, secondBody), first.Parameters);
        }



        /// <summary>
        /// 构建单个表达式操作
        /// </summary>
        /// <typeparam name="T">实体类</typeparam>
        /// <param name="columnName">列名</param>
        /// <param name="val">值</param>
        /// <param name="operation">操作字符串：支持==、!=、>、>=、< 、<= 、Contains</param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> Operation<T>(string columnName, object val, string operation, string name = "p")
        {
            switch (operation)
            {
                case "==":
                    return BinaryLogic<T>(Expression.Equal, columnName, val);
                case "!=":
                    return BinaryLogic<T>(Expression.NotEqual, columnName, val);
                case ">":
                    return BinaryLogic<T>(Expression.GreaterThan, columnName, val);
                case ">=":
                    return BinaryLogic<T>(Expression.GreaterThanOrEqual, columnName, val);
                case "<":
                    return BinaryLogic<T>(Expression.LessThan, columnName, val);
                case "<=":
                    return BinaryLogic<T>(Expression.LessThanOrEqual, columnName, val);
                case "Contains":
                    try
                    {
                        return Call<T>(columnName, "Contains", name, val);
                    }
                    catch (Exception)
                    {
                        return BinaryLogic<T>(Expression.Equal, columnName, val);
                    }
                    break;
                case "In":
                    if (val is string)
                    {
                        var listval = val.ToStringExpand().Split(',').ToList();
                        return In<T>(columnName, listval);
                    }
                    else if (val is List<Guid>)
                    {
                        var listval = val as List<Guid>;
                        return In<T>(columnName, listval);
                    }
                    else if (val is List<Guid?>)
                    {
                        var listval = val as List<Guid?>;
                        return In<T>(columnName, listval);
                    }
                    else if (val is List<int>)
                    {
                        var listval = val as List<int>;
                        return In<T>(columnName, listval);
                    }
                    else if (val is List<int?>)
                    {
                        var listval = val as List<int?>;
                        return In<T>(columnName, listval);
                    }
                    return In<T>(columnName, val);
                case "InOr":
                    Expression<Func<T, bool>> orExpression = null;
                    val.ToStringExpand().Split(',').ToList().ForEach(s =>
                    {
                        orExpression = orExpression == null ? GetCondition<T>(columnName, s, "Contains") : orExpression.Compose(GetCondition<T>(columnName, s, "Contains"), Expression.Or);
                    });
                    return orExpression ?? (e => true);
                case "InAnd":
                    Expression<Func<T, bool>> andExpression = null;
                    val.ToStringExpand().Split(',').ToList().ForEach(s =>
                    {
                        andExpression = andExpression == null ? GetCondition<T>(columnName, s, "Contains") : andExpression.Compose(GetCondition<T>(columnName, s, "Contains"), Expression.And);
                    });
                    return andExpression ?? (e => true);
                default:
                    throw new Exception("operation Exception");
            }
        }

        /// <summary>
        /// 排序表达式
        /// </summary>
        /// <typeparam name="T">实体类</typeparam>
        /// <typeparam name="TKey">排序字段属性</typeparam>
        /// <param name="property">排序属性名称</param>
        /// <returns></returns>
        public static Expression<Func<T, TKey>> OrderBy<T, TKey>(string property, string name = "p")
        {
            var type = typeof(T);

            var parameter = GetParameterByType(type, name);
            var propertyAccess = Expression.MakeMemberAccess(parameter, type.GetProperty(property));
            return (Expression<Func<T, TKey>>)Expression.Lambda(propertyAccess, parameter);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="logic"></param>
        /// <param name="columnName"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        private static Expression<Func<T, bool>> BinaryLogic<T>(Func<Expression, Expression, BinaryExpression> logic, string columnName, object val, string name = "p")
        {
            var entityType = typeof(T);
            var p1 = GetParameterByType(entityType, name);
            var propertyType = entityType.GetProperty(columnName);
            var ma = Expression.MakeMemberAccess(p1, propertyType);
            var eq = logic(ma, Expression.Constant(val, propertyType.PropertyType));
            return (Expression<Func<T, bool>>)Expression.Lambda(eq, new[] { p1 });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="logic"></param>
        /// <param name="columnName"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        private static Expression<Func<T, bool>> BinaryLogicEextend<T>(Func<Expression, Expression, BinaryExpression> logic, string columnName, string columnName2, string name = "p")
        {
            var entityType = typeof(T);
            var p1 = GetParameterByType(entityType, name);
            var propertyType = entityType.GetProperty(columnName);
            var propertyType2 = entityType.GetProperty(columnName2);

            var ma = Expression.MakeMemberAccess(p1, propertyType);
            var ma2 = Expression.MakeMemberAccess(p1, propertyType2);


            var eq = logic(ma, ma2);
            return (Expression<Func<T, bool>>)Expression.Lambda(eq, new[] { p1 });
        }


        /// <summary>
        /// 函数调用，例如 Contains
        /// </summary>
        /// <typeparam name="T">实体类泛型</typeparam>
        /// <param name="columnName">数据库字段名</param>
        /// <param name="methodName">需要调用的方法名</param>
        /// <param name="args">调用方法时需要的参数</param>
        /// <returns>调用方法的返回值</returns>
        public static Expression<Func<T, bool>> Call<T>(string columnName, string methodName, string name = "p", params object[] args)
        {
            //Logger.Write(string.Format("Expression call columnName:{0},methodName:{1}", columnName, methodName));

            var entityType = typeof(T);
            var p1 = GetParameterByType(entityType, name);
            var ma = Expression.MakeMemberAccess(p1, entityType.GetProperty(columnName));
            var call = Expression.Call(ma, ma.Type.GetMethod(methodName),
                args.Select(a => (Expression)Expression.Constant(a)));
            return (Expression<Func<T, bool>>)Expression.Lambda(call, new[] { p1 });


        }
        public static Expression<Func<T, bool>> In<T>(string columnName, object args, string name = "p")
        {
            //Logger.Write(string.Format("Expression call columnName:{0},methodName:{1}", columnName, methodName));

            var entityType = typeof(T);
            var p1 = GetParameterByType(entityType, name);
            var ma = Expression.MakeMemberAccess(p1, entityType.GetProperty(columnName));
            var call = Expression.Call(Expression.Constant(args), args.GetType().GetMethod("Contains"), ma);
            return (Expression<Func<T, bool>>)Expression.Lambda(call, new[] { p1 });


        }

        /// <summary>
        /// 获取参数表达式
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        private static ParameterExpression GetParameterByType(Type t, string name = "p")
        {
            ParameterExpression cached;
            if (TypeParamExpMap.TryGetValue(t, out cached)) return cached;
            return TypeParamExpMap[t] = Expression.Parameter(t, name);
        }


        /// <summary>
        /// 权限匹配
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="values"></param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> GetACFieldCondition<T>(List<string> values)
        {
            if (values == null || values.Count == 0)
            {
                return e => false;
            }
            Expression<Func<T, bool>> expression = null;
            foreach (var value in values)
            {
                expression = expression != null ? expression.Compose(GetCondition<T>("ACField", value, "Contains"), Expression.Or) : GetCondition<T>("ACField", value, "Contains");
            }
            return expression;
        }
        /// <summary>
        /// 根据控件Id获取和值生成查询条件
        /// </summary>
        /// <typeparam name="T">Entity</typeparam>
        /// <param name="controlId">前台控件的ID或者数据库中的实际字段 </param>
        /// <param name="value">Value</param>
        /// <param name="queryMatch">匹配符 支持==、!=、>、>=、< 、<= 、Contains </param>
        /// <returns></returns>
        private static Expression<Func<T, bool>> GetCondition<T>(string controlId, string value, string queryMatch)
        {

            var propertyInfo = typeof(T).GetProperties().FirstOrDefault(u => controlId.Matching(u.Name));

            if (propertyInfo == null) return null;

            //判断字段类型是否可以为Nullable
            var isNullable = propertyInfo.PropertyType.GenericTypeArguments.Length > 0;//数据类型是否可以为Nullable

            //如果有Nullable 就取下层的数据类型 否则取本身的数据类型
            var propertyTypeName = isNullable ? propertyInfo.PropertyType.GenericTypeArguments[0].Name : propertyInfo.PropertyType.Name;

            return GetCondition<T>(propertyInfo.Name, value, queryMatch, propertyTypeName, isNullable);


        }
        public static Expression<Func<T, bool>> GetSearchTextCondition<T>(List<string> names, string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return e => true;
            }
            else
            {
                value = value.Trim();
            }
            value = value.Trim();
            Expression<Func<T, bool>> expression = null;
            foreach (var name in names)
            {
                expression = expression != null ? expression.Compose(GetCondition<T>(name, value, "Contains"), Expression.Or) : GetCondition<T>(name, value, "Contains");
            }
            return expression;
        }
        /// <summary>
        /// 根据控件Id获取和值生成查询条件
        /// </summary>
        /// <typeparam name="T">Entity</typeparam>
        /// <param name="controlId">前台控件的ID或者数据库中的实际字段 </param>
        /// <param name="value">Value</param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> GetCondition<T>(string controlId, string value, Relation relation)
        {
            return GetCondition<T>(controlId, value, GetMatch(relation));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="r"></param>
        /// <returns></returns>
        private static string GetMatch(Relation r)
        {
            switch (r)
            {
                case Relation.Equal:
                    return "==";
                case Relation.NotEqual:
                    return "!=";
                case Relation.GreaterThan:
                    return ">";
                case Relation.GreaterThanOrEqual:
                    return ">=";
                case Relation.LessThan:
                    return "<";
                case Relation.LessThanOrEqual:
                    return "<=";
                case Relation.Contains:
                case Relation.In:
                case Relation.InOr:
                case Relation.InAnd:
                    return r.ToString();
                default:
                    throw new ArgumentOutOfRangeException("r");
            }
            return "==";
        }

        /// <summary>
        /// 根据控件Id获取和值生成查询条件
        /// </summary>
        /// <typeparam name="T">Entity</typeparam>
        /// <param name="propertyName">字段名</param>
        /// <param name="value">Value</param>
        /// <param name="queryMatch">匹配符 支持==、!=、>、>=、< 、<= 、Contains</param>
        /// <param name="propertyTypeName"></param>
        /// <param name="isNull"></param>
        public static Expression<Func<T, bool>> GetCondition<T>(string propertyName, string value, string queryMatch, string propertyTypeName, bool isNull)
        {
            Expression<Func<T, bool>> condition = null;

            switch (propertyTypeName)
            {
                case "Double":
                case "double":
                    condition = isNull ? Operation<T>(propertyName, value.ToNullableDouble(), queryMatch) : Operation<T>(propertyName, value.ToDouble(), queryMatch);
                    break;
                case "Decimal":
                case "decimal":
                    condition = isNull ? Operation<T>(propertyName, value.ToNullableDecimal(), queryMatch) : Operation<T>(propertyName, value.ToDecimal(), queryMatch);
                    break;
                case "Int16":
                case "Int32":
                case "Int64":
                case "int":
                    if (queryMatch.StartsWith("In"))
                        condition = isNull ? Operation<T>(propertyName, value.ToIntList().Select(c => (int?)c).ToList(), "In") : Operation<T>(propertyName, value.ToIntList(), "In");
                    else
                        condition = isNull ? Operation<T>(propertyName, value.ToNullableInt(), queryMatch) : Operation<T>(propertyName, value.ToInt(), queryMatch);
                    break;
                case "byte":
                case "Byte":
                    condition = isNull ? Operation<T>(propertyName, value.ToNullableByte(), queryMatch) : Operation<T>(propertyName, value.ToByte(), queryMatch);
                    break;
                case "Guid":
                    if (queryMatch == "In")
                        condition = isNull ? Operation<T>(propertyName, value.ToGuidList().Select(c => (Guid?)c).ToList(), queryMatch) : Operation<T>(propertyName, value.ToGuidList(), queryMatch);
                    else
                        condition = isNull ? Operation<T>(propertyName, value.ToNullableGuid(), queryMatch) : Operation<T>(propertyName, value.ToGuid(), queryMatch);
                    break;
                case "DateTime":
                    condition = isNull ? Operation<T>(propertyName, value.ToNullableDateTime(), queryMatch) : Operation<T>(propertyName, value.ToDateTime(), queryMatch);
                    break;
                case "bool":
                case "Boolean":
                    condition = isNull ? Operation<T>(propertyName, value.ToNullableBool(), queryMatch) : Operation<T>(propertyName, value.ToBool(), queryMatch);
                    break;
                default:
                    condition = Operation<T>(propertyName, value, queryMatch, "pp");
                    break;
            }

            return condition;
        }




    }
}
