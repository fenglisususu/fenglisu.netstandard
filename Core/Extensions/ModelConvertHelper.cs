﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace Core.Extensions
{
    public static class ModelConvertHelper
    {
        public static IList<T> ConvertToModel<T>(this DataTable dt) where T : new()
        {
            IList<T> ts = new List<T>();// 定义集合
            Type type = typeof(T); // 获得此模型的类型
            string tempName = "";
            foreach (DataRow dr in dt.Rows)
            {
                T t = new T();
                System.Reflection.PropertyInfo[] propertys = t.GetType().GetProperties();// 获得此模型的公共属性
                foreach (System.Reflection.PropertyInfo pi in propertys)
                {
                    tempName = pi.Name;
                    if (dt.Columns.Contains(tempName))
                    {
                        if (!pi.CanWrite) continue;
                        object value = dr[tempName];
                        if (value != DBNull.Value)
                            pi.SetValue(t, value, null);
                    }
                }
                ts.Add(t);
            }
            return ts;
        }

        public static T MapTo<T>(this object src) where T : new()
        {
            Type typeOne = src.GetType();
            var tar = new T();
            Type typeTwo = typeof(T);

            List<PropertyInfo> pisOne = typeOne.GetProperties().ToList();
            List<PropertyInfo> pisTwo = typeTwo.GetProperties().ToList();

            foreach (var pi1 in pisOne)
            {
                foreach (var pi2 in pisTwo)
                {
                    var w = false;
                    if (!pi2.CanWrite) continue;
                    if (pi2.Name != pi1.Name) continue;
                    if (pi1.PropertyType == pi2.PropertyType) w = true;
                    if (pi2.PropertyType.Name == "Nullable`1" && pi2.PropertyType.GenericTypeArguments[0] == pi1.PropertyType) w = true;
                    if (w)
                    {
                        try
                        {
                            pi2.SetValue(tar, pi1.GetValue(src));
                        }
                        catch
                        {
                            ;
                        }
                    }
                }
            }

            return tar;
        }

        public static T MapTo<T>(this object src, T tar, bool overwrite = true) where T : new()
        {
            Type typeOne = src.GetType();
            Type typeTwo = typeof(T);

            List<PropertyInfo> pisOne = typeOne.GetProperties().ToList();
            List<PropertyInfo> pisTwo = typeTwo.GetProperties().ToList();

            foreach (var pi1 in pisOne)
            {
                foreach (var pi2 in pisTwo)
                {
                    var w = false;
                    if (!pi2.CanWrite) continue;
                    if (pi2.Name != pi1.Name) continue;
                    if (pi1.PropertyType == pi2.PropertyType) w = true;
                    if (pi2.PropertyType.Name == "Nullable`1" && pi2.PropertyType.GenericTypeArguments[0] == pi1.PropertyType) w = true;
                    if (w)
                    {
                        try
                        { 
                            if (overwrite)
                            {
                                pi2.SetValue(tar, pi1.GetValue(src));
                            }
                            else
                            {
                                var oldvalue = pi2.GetValue(tar);
                                if (oldvalue == null || string.IsNullOrEmpty(oldvalue.ToString()))
                                {
                                    pi2.SetValue(tar, pi1.GetValue(src));
                                }
                            }

                        }
                        catch
                        {
                            ;
                        }
                    }
                }
            }

            return tar;
        }
    }

}
