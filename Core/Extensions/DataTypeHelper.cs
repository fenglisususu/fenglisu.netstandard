﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Core.Extensions
{

    /// <summary>
    /// 数据类型辅助类
    /// </summary>
    public static class DataTypeHelper
    {


        /// <summary>
        /// 字符串转byte[]
        /// </summary>
        /// <returns></returns>
        public static byte[] ToDefaultEncodingBytes(this string str)
        {
            return System.Text.Encoding.Default.GetBytes(str);

        }

        /// <summary>
        /// byte[]转字符串
        /// </summary>
        /// <returns></returns>
        public static string ToDefaultEncodingString(this byte[] byteArray)
        {
            return System.Text.Encoding.Default.GetString(byteArray);
        }



        /// <summary>
        /// 生成按时间排序的GUID号
        /// </summary>
        /// <returns></returns>
        public static Guid ToRank(this Guid id)
        {

            var guidArray = id.ToByteArray();
            var now = DateTime.Now;

            var baseDate = new DateTime(1900, 1, 1);

            var days = new TimeSpan(now.Ticks - baseDate.Ticks);

            var msecs = new TimeSpan(now.Ticks - (new DateTime(now.Year, now.Month, now.Day).Ticks));
            var daysArray = BitConverter.GetBytes(days.Days);
            var msecsArray = BitConverter.GetBytes((long)(msecs.TotalMilliseconds / 3.333333));

            Array.Copy(daysArray, 0, guidArray, 2, 2);

            //毫秒高位
            Array.Copy(msecsArray, 2, guidArray, 0, 2);

            //毫秒低位
            Array.Copy(msecsArray, 0, guidArray, 4, 2);

            return new Guid(guidArray);
        }

        /// <summary>
        /// 转换为时间类型
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTime ToDateTime(this object value)
        {
            DateTime date;
            DateTime.TryParse(value.ToNullableString(), out date);
            return date;

        }
        /// <summary>
        ///  转换可为空的时间类型
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTime? ToNullableDateTime(this object value)
        {
            DateTime date;
            if (DateTime.TryParse(value.ToNullableString(), out date)) return date;

            return null;
        }

        /// <summary>
        ///  转换类型
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int ToInt(this object value)
        {
            int intValue;
            int.TryParse(value.ToNullableString(), out intValue);
            return intValue;
        }

        /// <summary>
        ///  转换类型
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int ToInt(this Enum value)
        {
            return (int)Enum.Parse(value.GetType(), value.ToString());
        }

        public static List<int> ToIntList(this object value)
        {

            var mylist = new List<int>();

            if (value == null) return mylist;
            foreach (var str in value.ToStringExpand().Split(','))
            {
                int intValue;
                int.TryParse(str.ToNullableString(), out intValue);
                {
                    mylist.Add(intValue);
                }
            }
            return mylist;

        }

        /// <summary>
        ///  转换可为空类型
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int? ToNullableInt(this object value)
        {
            int intValue;

            if (int.TryParse(value.ToNullableString(), out intValue)) return intValue;

            return null;
        }
        /// <summary>
        ///  转换类型
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double ToDouble(this object value)
        {
            double doubleValue;
            double.TryParse(value.ToNullableString(), out doubleValue);
            return doubleValue;
        }

        /// <summary>
        ///  转换可为空类型
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double? ToNullableDouble(this object value)
        {
            double v;
            if (double.TryParse(value.ToNullableString(), out v))
                return v;
            return null;
        }

        /// <summary>
        ///  转换类型,null为"";
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToStringOrEmpty(this object value)
        {
            if (value == null) return "";

            return value.ToString();
        }
        /// <summary>
        ///  转换类型;
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToNullableString(this object value)
        {
            if (value == null) return null;

            return value.ToString();
        }

        /// <summary>
        ///  转换类型
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Guid ToGuid(this object value)
        {
            Guid id;
            Guid.TryParse(value.ToNullableString(), out id);
            return id;
        }

        /// <summary>
        ///  转换可为空类型
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Guid? ToNullableGuid(this object value)
        {
            Guid id;
            if (Guid.TryParse(value.ToNullableString(), out id)) return id;

            return null;
        }

        /// <summary>
        ///  是否为空
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsEmpty(this Guid value)
        {
            return (value == Guid.Empty);
        }
        /// <summary>
        ///  是否为空
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsEmpty(this Guid? value)
        {
            return (value == null || value == Guid.Empty);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToStringExpand(this object value)
        {
            if (value == null) return "";

            return value.ToString();
        }
        /// <summary>
        /// 将 Guid 结构转换为等效的 GUID 的字符串表示形式,Guid.Empty转换为空字符串。
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToStringExpand(this Guid value)
        {
            return value.IsEmpty() ? "" : value.ToString();
        }
        /// <summary>
        /// 前台控件和属性去匹配
        /// 匹配规则分二种
        /// 1、前台控件和后面数据库中的字段完全一样
        /// 2、有前缀的字段 规则必须是 XXXX_字段名
        /// 
        /// 除此二种情况， 将会视为没有匹配上
        /// 如果匹配上 会返回true ,否则为false
        /// </summary>
        /// <param name="controlId">前台控件Id</param>
        /// <param name="property">Entity中的属性名</param>
        /// <returns></returns>
        public static bool Matching(this string controlId, string property)
        {
            return controlId == property || controlId.Substring(controlId.LastIndexOf('_') + 1) == property;
        }
        /// <summary>
        /// 将 Guid 结构转换为等效的 GUID 的字符串表示形式,Null和Guid.Empty转换为空字符串。
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToStringExpand(this Guid? value)
        {
            return value.IsEmpty() ? "" : value.ToString();
        }

        /// <summary>
        ///  指示当前字符串是 null、空还是仅由空白字符组成。
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsNullOrWhiteSpace(this object value)
        {
            return string.IsNullOrWhiteSpace(value.ToNullableString());
        }

        public static bool ToBool(this object value)
        {
            bool v;
            bool.TryParse(value.ToNullableString(), out v);
            return v;
        }

        /// <summary>
        /// 将当前字符串转换为bool?，转换失败返回null
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool? ToNullableBool(this object value)
        {

            bool v;
            if (bool.TryParse(value.ToNullableString(), out v))
                return v;
            return null;
        }


        public static decimal ToDecimal(this object value)
        {

            decimal v;
            decimal.TryParse(value.ToNullableString(), out v);
            return v;

        }


        public static decimal? ToNullableDecimal(this object value)
        {
            decimal v;
            if (decimal.TryParse(value.ToNullableString(), out v))
                return v;
            return null;
        }


        public static byte ToByte(this object value)
        {
            byte v;
            byte.TryParse(value.ToNullableString(), out v);
            return v;
        }

        public static byte? ToNullableByte(this object value)
        {
            byte v;
            if (byte.TryParse(value.ToNullableString(), out v))
                return v;
            return null;
        }

        public static int GetCount(this string src, char ch = ',')
        {
            if (string.IsNullOrWhiteSpace(src)) return 0;
            return src.Split(ch).Length;
        }

        public static List<Guid> ToGuidList(this string list)
        {
            var mylist = new List<Guid>();

            if (list == null) return mylist;
            foreach (var guidItem in list.Split(',').Select(item => item.ToGuid()).Where(guidItem => !guidItem.IsEmpty() && !mylist.Any(u => u.Equals(guidItem))))
            {
                mylist.Add(guidItem);
            }

            return mylist;
        }

        public static string ToString(this IDictionary<string, object> list, string listName = "", string keyName = "Name", string valueName = "Value")
        {
            var values = new StringBuilder();
            values.Append(listName);
            foreach (var variable in list)
            {
                values.AppendFormat(" {0} : {1}  {2} : {3},", keyName, variable.Key, valueName, variable.Value);
            }

            return values.ToString();

        }


        public static object ChangeType(this object value, Type type)
        {
            if (value == null && type.IsGenericType) return Activator.CreateInstance(type);
            if (value == null) return null;
            if (type == value.GetType()) return value;
            if (type.IsEnum)
            {
                if (value is string)
                    return Enum.Parse(type, value as string);
                else
                    return Enum.ToObject(type, value);
            }
            if (!type.IsInterface && type.IsGenericType)
            {
                Type innerType = type.GetGenericArguments()[0];
                object innerValue = ChangeType(value.ToNullableString(), innerType);
                return Activator.CreateInstance(type, new object[] { innerValue });
            }

            if (value is string && type == typeof(Guid)) return new Guid(value as string);

            if (value is string && type == typeof(Version)) return new Version(value as string);

            if (!(value is IConvertible)) return value;

            return Convert.ChangeType(value.ToNullableString(), type);
        }

        public static string ToDateTimeString(this DateTime date)
        {
            return date.ToString("yyyy-MM-dd HH:mm:ss");
        }
        public static string ToDateTimeString(this DateTime? date)
        {
            if (date == null) return "";
            return ((DateTime)date).ToString("yyyy-MM-dd HH:mm:ss");
        }

        public static string ToFullName(this Type type)
        {
            var fullName = type.FullName;
            if (!type.IsGenericType) return fullName + "," + type.Assembly.FullName.Split(',')[0];
            if (fullName == null)
                return type.Namespace + "." + type.Name;
            var fullNames = fullName.Split(',');
            fullName = "";
            foreach (var name in fullNames)
            {
                if (name.Contains("Version="))
                {
                }
                else if (name.Contains("Culture=neutral"))
                {
                }
                else if (name.Contains("PublicKeyToken=null"))
                {
                    fullName = fullName.TrimEnd(',') + name.Replace("PublicKeyToken=null", "");
                }
                else
                    fullName += name + ",";

            }
            return fullName.TrimEnd(',') + "," + type.Assembly.FullName.Split(',')[0];
        }

        public static string GetMessage(this Exception ex)
        {
            var msg = new System.Text.StringBuilder();
            msg.AppendLine(ex.Message);
            msg.AppendLine(ex.Message);
            if (ex.InnerException != null)
            {
                if (ex.InnerException is ReflectionTypeLoadException)
                {
                    var rtle = ex.InnerException as ReflectionTypeLoadException;
                    foreach (var rtlex in rtle.LoaderExceptions)
                    {
                        msg.AppendLine(GetMessage(rtlex));
                    }

                }
                else
                    msg.AppendLine(GetMessage(ex.InnerException));
            }
            return msg.ToString();
        }


    }
}
