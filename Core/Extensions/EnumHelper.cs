﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Core.Extensions
{
    /// <summary>
    /// 枚举辅助类
    /// </summary>
    public static class EnumHelper
    {
        #region 缓存


        /// <summary>
        /// 枚举缓存
        /// </summary>
        public static readonly Dictionary<string, Dictionary<int, string>> KeyDescriptionCache = new Dictionary<string, Dictionary<int, string>>();

        public static readonly Dictionary<string, Dictionary<int, string>> KeyValueCache = new Dictionary<string, Dictionary<int, string>>();

        public static readonly Dictionary<string, Dictionary<string, string>> ValueDescriptionCache = new Dictionary<string, Dictionary<string, string>>();
        #endregion

        #region 单值问题


        /// <summary>
        /// 值转枚举
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ConvertToEnumString<T>(this int? value) where T : struct
        {
            T local = default(T);
            if (!Enum.TryParse<T>(value.ToString(), out local))
            {
                return string.Empty;
            }
            return local.ToString();
        }
        /// <summary>
        /// 值转枚举
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ConvertToEnumString<T>(this int value) where T : struct
        {
            T local = default(T);
            if (!Enum.TryParse<T>(value.ToString(), out local))
            {
                return string.Empty;
            }
            return local.ToString();
        }
        /// <summary>
        /// 值转枚举
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T ConvertToEnum<T>(this int? value) where T : struct
        {
            T local = default(T);
            if (!Enum.TryParse<T>(value.ToString(), out local))
            {
                throw new ArgumentException(string.Format("Enum not defined this value {0}!", value));
            }
            return local;
        }

        /// <summary>
        /// 值转枚举
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T ConvertToEnum<T>(this int value) where T : struct
        {
            T local = default(T);
            if (!Enum.TryParse<T>(value.ToString(), out local))
            {
                throw new ArgumentException(string.Format("Enum not defined this value {0}!", value));
            }
            return local;
        }
        /// <summary>
        /// 名称转枚举
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public static T ConvertToEnum<T>(this string name) where T : struct
        {
            if (string.IsNullOrEmpty(name))
                return default(T);
            if (!Enum.IsDefined(typeof(T), name))
            {
                int value = 0;
                if (int.TryParse(name, out value))
                {
                    return ConvertToEnum<T>(value);
                }
                throw new ArgumentException(string.Format("Enum not defined this name {0}!", name));
            }
            return (T)Enum.Parse(typeof(T), name.ToString(), true);
        }

        /// <summary>
        /// 通过值获取枚举的描述
        /// </summary>
        /// <param name="enumType"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetDescriptionByIntValue(this Type enumType, int value)
        {
            var dic = GetKeyDescription(enumType);

            return dic.FirstOrDefault(u => u.Key == value).Value;
        }

        public static string GetDescriptionByName(this Type enumType, string value)
        {
            var dic = GetValueDescription(enumType);

            return dic.FirstOrDefault(u => u.Key == enumType.FullName).Value;
        }

        public static bool GetEnumItemBoolean(this int? stateEnum, Enum enumItem)
        {
            object obj2 = Enum.Parse(enumItem.GetType(), enumItem.ToString(), true);
            return ((stateEnum & (((int)1) << ((int)Math.Log((double)((int)obj2), 2.0)))) != 0);
        }

        public static string GetFullString(this Enum enumItem)
        {
            return (enumItem.GetType().Name + "." + enumItem);
        }


        #endregion

        #region 获取字典列表的方法


        public static Dictionary<string, int> GetDescriptionKey(this Type enumType)
        {
            return GetKeyDescription(enumType).ToDictionary(p => p.Value, p => p.Key);
        }

        public static Dictionary<string, string> GetValueDescription(this Type enumType)
        {
            var dic = ValueDescriptionCache.FirstOrDefault(e => e.Key == enumType.FullName);

            return dic.Key != null ? dic.Value : GetValueDescriptionByEnumType(enumType);
        }

        public static Dictionary<int, string> GetKeyValue(this Type enumType)
        {
            var dic = KeyValueCache.FirstOrDefault(e => e.Key == enumType.FullName);

            return dic.Key != null ? dic.Value : GetKeyValueByEnumType(enumType);
        }

        /// <summary>
        /// 通过枚举类型获取枚举值和描述的字典
        /// </summary>
        /// <param name="enumType"></param>
        /// <returns></returns>
        public static Dictionary<int, string> GetKeyDescription(this Type enumType)
        {
            if (enumType == null) return null;

            var dic = KeyDescriptionCache.FirstOrDefault(e => e.Key == enumType.FullName);

            return dic.Key != null ? dic.Value : GetKeyDescriptionByEnumType(enumType);

        }

        /// <summary>
        /// 获取枚举值和描述字典
        /// </summary>
        /// <param name="enumFullName">枚举名称格式为：GY.BasicPlatform.Infrastructure.Enum.EnumName，简写格式为：BasicPlatform.EnumName,且GY.BasicPlatform.Infrastructure必须为程序集,</param>
        /// <returns></returns>
        public static Dictionary<int, string> GetKeyDescription(this string enumFullName)
        {
            //如何为简写格式：BasicPlatform.EnumName，将枚举转换为全称
            var names = enumFullName.Split('.');
            if (names.Count() == 2)
            {
                enumFullName = string.Format("GY.{0}.Infrastructure.Enum.{1}", names[0], names[1]);
            }


            var dic = KeyDescriptionCache.FirstOrDefault(e => e.Key == enumFullName);

            return dic.Key != null ? dic.Value : GetKeyDescription(GetEnumType(enumFullName));

        }

        #endregion

        //public static string ToEnumNameFromBitArray(Type enumType, bool[] dbReturnBitArray)
        //{
        //    if (dbReturnBitArray == null)
        //    {
        //        return "";
        //    }
        //    int length = Enum.GetValues(enumType).Length;
        //    var list = new List<string>(dbReturnBitArray.Length);
        //    for (int i = 0; i < dbReturnBitArray.Length; i++)
        //    {
        //        if (dbReturnBitArray[i] && (i <= length))
        //        {
        //            list.Add(Enum.GetName(enumType, i));
        //        }
        //    }
        //    list.TrimExcess();
        //    return string.Join(",", list.ToArray());
        //}



        //public static string GetKeyDescription(Type enumType, bool[] dbReturnBitArray)
        //{
        //    if (dbReturnBitArray == null)
        //    {
        //        return "";
        //    }
        //    int length = Enum.GetValues(enumType).Length;
        //    var list = new List<string>(dbReturnBitArray.Length);
        //    for (int i = 0; i < dbReturnBitArray.Length; i++)
        //    {
        //        if (dbReturnBitArray[i] && (i <= length))
        //        {
        //            list.Add(Enum.GetName(enumType, i));
        //        }
        //    }
        //    list.TrimExcess();
        //    return string.Join(",", list.ToArray());
        //}



        /// <summary>
        /// 通过枚举名称获取枚举Type
        /// </summary>
        /// <param name="enumFullName"></param>
        /// <returns></returns>
        public static Type GetEnumType(string enumFullName)
        {
            try
            {
                var names = enumFullName.Split('.');

                if (names.Count() < 3) return null;

                var assembly = Assembly.Load(names[0] + "." + names[1] + "." + names[2]);

                return assembly == null ? null : assembly.GetType(enumFullName);

            }
            catch (Exception)
            {
                throw new Exception(string.Format("Cant Find Enum {0} ", enumFullName));
            }

        }

        public static T GetValue<T>(Enum enumValue) where T : struct
        {
            return (T)Enum.Parse(enumValue.GetType(), enumValue.ToString());
        }

        #region private

        /// <summary>
        /// 将枚举转换为字段，并缓存
        /// </summary>
        /// <param name="enumType"></param>
        /// <returns></returns>
        private static Dictionary<int, string> GetKeyDescriptionByEnumType(Type enumType)
        {

            var dictionary = new Dictionary<int, string>();

            if (enumType == null) return dictionary;

            var fields = enumType.GetFields();

            var values = (int[])Enum.GetValues(enumType);

            var index = 0;

            foreach (var info in fields)
            {
                if (!info.FieldType.IsEnum) continue;

                var customAttributes = info.GetCustomAttributes(typeof(DescriptionAttribute), true);

                dictionary.Add(values[index], customAttributes.Length > 0 ? ((DescriptionAttribute)customAttributes[0]).Description : info.Name);

                index++;

            }

            KeyDescriptionCache[enumType.FullName] = dictionary;

            return dictionary;
        }



        private static Dictionary<int, string> GetKeyValueByEnumType(Type enumType)
        {
            var dictionary = new Dictionary<int, string>();

            var fields = enumType.GetFields();

            var values = (int[])Enum.GetValues(enumType);

            var index = 0;
            foreach (var info in fields.Where(info => info.FieldType.IsEnum))
            {
                dictionary.Add(values[index], info.Name);

                index++;
            }

            KeyValueCache.Add(enumType.FullName, dictionary);

            return dictionary;
        }



        /// <summary>
        /// 将枚举转换为字段，并缓存
        /// </summary>
        /// <param name="enumType"></param>
        /// <returns></returns>
        private static Dictionary<string, string> GetValueDescriptionByEnumType(Type enumType)
        {

            var dictionary = new Dictionary<string, string>();

            if (enumType == null) return dictionary;

            var fields = enumType.GetFields();

            var index = 0;

            foreach (var field in fields)
            {
                if (!field.FieldType.IsEnum) continue;

                var customAttributes = field.GetCustomAttributes(typeof(DescriptionAttribute), true);

                dictionary.Add(field.Name, customAttributes.Length > 0 ? ((DescriptionAttribute)customAttributes[0]).Description : field.Name);

                index++;

            }


            ValueDescriptionCache.Add(enumType.FullName, dictionary);

            return dictionary;
        }


        #endregion

      
    }


}
