﻿using System.Text.RegularExpressions;

namespace Core.Extensions
{

    /// <summary>
    /// 正则表达式辅助类
    /// </summary>
    public static class RegexHelper
    {

        /// <summary>
        /// 获取字符串中两个字符间的值
        /// </summary>
        /// <param name="value">源字符串</param>
        /// <param name="beginChar">开始字符</param>
        /// <param name="endChar">结束字符</param>
        /// <returns>结果</returns>
        public static string ToBetweenValue(this string value, string beginChar, string endChar)
        {
            Match m = Regex.Match(value, string.Format(@"{0}([\s\S]*?){1}", beginChar, endChar));
            return m.Success ? m.Result("$1") : string.Empty;
        }

        /// <summary>
        /// 是否手机号码
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsMobile(this string value)
        {
            string poneReg = "^1[345789]\\d{9}$";
            return Regex.IsMatch(value, poneReg);
        }



    }
}
