﻿using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace Core.Extensions
{
    /// <summary>
    /// 序列号辅助类
    /// </summary>
    public static class SerializeHelper
    {
        /// <summary>
        /// 深度克隆
        /// </summary>
        /// <typeparam name="T">类型</typeparam>
        /// <param name="oSource">源值</param>
        /// <returns></returns>
        public static T DeepClone<T>(this T oSource)
        {
            T oClone;
            var dcs = new DataContractSerializer(typeof(T));
            using (var ms = new MemoryStream())
            {
                dcs.WriteObject(ms, oSource);
                ms.Position = 0;
                oClone = (T)dcs.ReadObject(ms);
            }
            return oClone;
        }

        /// <summary>
        ///深度克隆 
        /// </summary>
        /// <typeparam name="T">类型</typeparam>
        /// <param name="oSource">源值</param>
        /// <returns></returns>
        public static T DeepClone<T>(this object oSource)
        {
            T oClone;
            var dcs = new DataContractSerializer(typeof(T));
            using (var ms = new MemoryStream())
            {
                dcs.WriteObject(ms, oSource);
                ms.Position = 0;
                oClone = (T)dcs.ReadObject(ms);
            }
            return oClone;
        }


        /// <summary>
        ///反序列化 
        /// </summary>
        /// <typeparam name="T">返回值类型</typeparam>
        /// <param name="input">二进制字符串</param>
        /// <returns>T对象</returns>
        public static T Deserialize<T>(string input)
        {
            var arr = input.Split(',');
            var arrBin = new byte[arr.Length];
            for (int i = 0; i < arr.Length; i++)
                arrBin[i] = byte.Parse(arr[i]);
            var ms = new MemoryStream(arrBin);
            var xml = new XmlSerializer(typeof(T));
            return (T)xml.Deserialize(ms);
        }
       
        /// <summary>
        /// 序列化
        /// </summary>
        /// <typeparam name="T">序列化类型</typeparam>
        /// <param name="obj">对象值</param>
        /// <returns>二进制字符串</returns>
        public static string Serialize<T>(object obj)
        {
            var ms = new MemoryStream();
            var xml = new XmlSerializer(typeof(T));
            xml.Serialize(ms, obj);//将对象序列化为流
            var arr = ms.ToArray();
            var sb = new StringBuilder();
            foreach (byte t in arr)
            {
                sb.Append(t + ",");
            }
            return sb.ToString().Trim(',');
        }

        /// <summary>
        /// xml反序列化
        /// </summary>
        /// <typeparam name="T">反序序列化类型</typeparam>
        /// <param name="input">二进制字符串</param>
        /// <returns></returns>
        public static T DeserializeXml<T>(this string input)
        {
            var arr = input.Split(',');
            var arrBin = new byte[arr.Length];
            for (int i = 0; i < arr.Length; i++)
                arrBin[i] = byte.Parse(arr[i]);
            var ms = new MemoryStream(arrBin);
            var xml = new XmlSerializer(typeof(T));
            return (T)xml.Deserialize(ms);
        }

        /// <summary>
        ///明文序列化为xml
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string SerializeXml<T>(this object obj)
        {
            var ms = new MemoryStream();
            var xml = new XmlSerializer(typeof(T));
            xml.Serialize(ms, obj);//将对象序列化为流
            var arr = ms.ToArray();
            var sb = new StringBuilder();
            foreach (byte t in arr)
            {
                sb.Append(t + ",");
            }
            return sb.ToString().Trim(',');
        }

        /// <summary>
        ///反序列化明文序xml 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="input"></param>
        /// <returns></returns>
        public static T XmlDeserialize<T>(this string input)
        {
            var arr = Encoding.UTF8.GetBytes(input);
            var ms = new MemoryStream(arr);
            var xml = new XmlSerializer(typeof(T));
            return (T)xml.Deserialize(ms);
        }

        /// <summary>
        /// xml序列化
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string XmlSerialize<T>(this object obj)
        {
            var ms = new MemoryStream();
            var xml = new XmlSerializer(typeof(T));
            xml.Serialize(ms, obj);//将对象序列化为流
            var arr = ms.ToArray();
            return Encoding.UTF8.GetString(arr, 0, arr.Length);
        }

        /// <summary>
        /// 数据契约反序列化 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="input"></param>
        /// <returns></returns>
        public static T DataContractDeserialize<T>(this string input)
        {
            T dobj;
            var dcs = new DataContractSerializer(typeof(T));
            using (var ms = new MemoryStream())
            {
                var arr = Encoding.UTF8.GetBytes(input);
                dcs.WriteObject(ms, arr);
                ms.Position = 0;
                dobj = (T)dcs.ReadObject(ms);
            }
            return dobj;
        }

        /// <summary>
        ///数据契约序列化 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string DataContractSerialize<T>(this object obj)
        {
            string dobj;
            var dcs = new DataContractSerializer(typeof(T));
            using (var ms = new MemoryStream())
            {
                dcs.WriteObject(ms, obj);
                ms.Position = 0;
                var arr = ms.ToArray();
                dobj = Encoding.UTF8.GetString(arr, 0, arr.Length);
            }
            return dobj;
        }
    }
}
