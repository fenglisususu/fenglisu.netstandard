﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Extensions.DistinctExtension
{

    /// <summary>
    /// 通过使用指定的 System.Collections.Generic.IEqualityComparer<T>的扩展。
    /// </summary>
    public static class ExceptExtensions
    {
        /// <summary>
        /// 通过使用指定的 System.Collections.Generic.IEqualityComparer<T> 对值进行比较产生两个序列的差集。
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="V"></typeparam>
        /// <param name="source"></param>
        /// <param name="source2"></param>
        /// <param name="keySelector"></param>
        /// <returns></returns>
       public static IEnumerable<T> Except<T, V>(this IEnumerable<T> source, IEnumerable<T> source2, Func<T, V> keySelector)
       {
           return source.Except(source2,new CommonEqualityComparer<T, V>(keySelector));
       }
    }
}
