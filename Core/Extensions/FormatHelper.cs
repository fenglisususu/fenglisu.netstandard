﻿using System.IO;
using System.Text;
using System.Xml;

namespace Core.Extensions
{

    /// <summary>
    /// 格式化器，用于格式化xml和json
    /// </summary>
    public class Formater
    {

        /// <summary>
        /// 格式化xml
        /// </summary>
        /// <param name="xmlString"></param>
        /// <returns></returns>
        public static string FormatXml(string xmlString)
        {
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(xmlString);
            StringBuilder sb = new StringBuilder();
            StringWriter writer = new StringWriter(sb);
            XmlTextWriter xmlTxtWriter = null;
            try
            {
                xmlTxtWriter = new XmlTextWriter(writer);
                xmlTxtWriter.Formatting = Formatting.Indented;
                xmlTxtWriter.Indentation = 1;
                xmlTxtWriter.IndentChar = '\t';
                xd.WriteTo(xmlTxtWriter);
            }
            finally
            {
                if (xmlTxtWriter != null)
                    xmlTxtWriter.Close();
            }
            return sb.ToString();
        }



    }
}
