﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace Core.Extensions
{
    public static class JsonHelper
    {
        //public static string ToJson(this object obj)
        //{
        //    var dt = new IsoDateTimeConverter { DateTimeFormat = DateTimeFormat };
        //    return JsonConvert.SerializeObject(obj, new JsonSerializerSettings() { Converters = new JsonConverter[] { dt } });
        //}
        public static string ToJson(this object obj, IEnumerable<string> excludedProperties, List<string> includeProperties = null)
        {


            var dt = new IsoDateTimeConverter { DateTimeFormat = DateTimeFormat };
            return JsonConvert.SerializeObject(obj, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore,
                Converters = new JsonConverter[] { dt },
                ContractResolver = new ExcludePropertiesContractResolver(obj, excludedProperties, includeProperties),
            });
        }
        public static string ToJson(this object obj)
        {

            var dt = new IsoDateTimeConverter { DateTimeFormat = DateTimeFormat };
            return JsonConvert.SerializeObject(obj, new JsonSerializerSettings()
            {
                ReferenceLoopHandling= ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore,
                Converters = new JsonConverter[] { dt },
            });
        }

        public static T ToObject<T>(this string json)
        {
            return json == null ? default(T) : JsonConvert.DeserializeObject<T>(json);
        }


        public static T ToObject<T>(this string json, T obj)
        {
            return JsonConvert.DeserializeObject<T>(json, new ObjectJc(obj));
        }


        public static string DateTimeFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss";

        public static string AddJson(this string josn, string josnstr)
        {
            string newJosn;

            var json1 = string.Empty;
            if (josn != null)
                json1 = josn.Trim().Substring(1, josn.Length - 2);

            var json2 = string.Empty;
            if (josnstr != null)
                json2 = josnstr.Trim().Substring(1, josnstr.Length - 2);

            if ((string.IsNullOrEmpty(json2) || "null".Equals(josnstr)) && !"null".Equals(josn))
                newJosn = josn;
            else if ((string.IsNullOrEmpty(json1) || "null".Equals(josn)) && !"null".Equals(josnstr))
                newJosn = josnstr;
            else
            {
                if (josn != null && josnstr != null && josn.StartsWith("[") && josnstr.StartsWith("["))
                {
                    newJosn = string.Format("[ {0},{1}]", json1, json2);
                }
                else if (josn != null && josn.StartsWith("["))
                {
                    newJosn = josn;
                }
                else
                {
                    newJosn = string.Format("{{ {0},{1}}}", json1, json2);
                }
            }
            return newJosn;
        }
    }

    public class ExcludePropertiesContractResolver : DefaultContractResolver
    {
        readonly IEnumerable<string> _lstExclude;
        readonly Dictionary<string, List<string>> _lstTypeExclude;

        readonly IEnumerable<string> _lstinclude;
        readonly Dictionary<string, List<string>> _lstTypeinclude;

        readonly List<string> _excludedProperties = new List<string>
            {
                "EntityBaseFullName",
                "ExpandoObjects",
                "EntityBases",
                "EntityState",
                "CreationTime",
                "CreatorId",
                "UpdateTime",
                "UpdateUserId",
                "OrgId",
                "PermissionValue",
                "PermissionsValue",
                "LogMessage",
                "Result;HostDate"
            };
        public ExcludePropertiesContractResolver(object obj, IEnumerable<string> excludedProperties, IEnumerable<string> includeProperties)
        {
            if (excludedProperties == null) excludedProperties = new List<string>();
            if (includeProperties == null) includeProperties = new List<string>();
            if (!excludedProperties.Any() && !includeProperties.Any())
            {
                if (obj != null)
                {

                }

            }
            var lstExclude = excludedProperties as IList<string> ?? excludedProperties.ToList();
            _lstExclude = lstExclude.Select(c => c.Split(';')[c.Split(';').Length - 1]);
            _lstTypeExclude = lstExclude.GroupBy(c => c.Split(';')[0]).ToDictionary(c => c.Key, c => c.ToList());

            var lstinclude = includeProperties as IList<string> ?? includeProperties.ToList();
            _lstinclude = lstinclude.Select(c => c.Split(';')[c.Split(';').Length - 1]);
            _lstTypeinclude = lstinclude.GroupBy(c => c.Split(';')[0]).ToDictionary(c => c.Key, c => c.ToList());

        }
        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            var name = type.Name;
            var lstInclude = new List<string>();
            lstInclude.AddRange(_lstinclude);
            if (_lstTypeinclude.ContainsKey(name))
                lstInclude.AddRange(_lstTypeinclude[name]);
            if (lstInclude.Any(c => !_excludedProperties.Contains(c)))
                return base.CreateProperties(type, memberSerialization).ToList().FindAll(p => lstInclude.Contains(p.PropertyName));
            var lstExclude = new List<string>();
            lstExclude.AddRange(_lstExclude);
            lstExclude.AddRange(_excludedProperties.Where(c => !lstInclude.Contains(c)));
            if (_lstTypeExclude.ContainsKey(name))
                lstExclude.AddRange(_lstTypeExclude[name]);
            return base.CreateProperties(type, memberSerialization).ToList().FindAll(p => !lstExclude.Contains(p.PropertyName));
        }
    }


    public class ObjectJc : JsonConverter
    {

        public ObjectJc(Object obj)
        {
            _obj = obj;
        }

        private Object _obj;


        public override bool CanConvert(Type objectType)
        {
            return (objectType.IsClass && objectType.BaseType != null && objectType.FullName == _obj.GetType().FullName);
        }


        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (_obj != null)
            {
                serializer.Populate(reader, _obj);
                return _obj;
            }
            return null;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            //if (writer.Path.Contains("Full"))
            //    writer.WriteEnd();
            //else
            //{
            //    writer.WriteValue(value);
            //}
        }

    }

}
