﻿using System.Linq;

namespace Core.Extensions
{
    /// <summary>
    /// 对象辅助类
    /// </summary>
    public static class ObjectHelper
    {

        /// <summary>
        /// 对象属性同步类
        /// </summary>
        /// <param name="target">被更新对象</param>
        /// <param name="source">数据来源</param>
        public static object UpdateProperties(this object target, object source)
        {
            var targetProperties = target.GetType().GetProperties();
            var sourceProperties = source.GetType().GetProperties();

            foreach (var targetProperty in targetProperties)
            {

                if (targetProperty.CanWrite == false) continue;

                if (targetProperty.PropertyType.Name == "ICollection`1") continue;

                if ((targetProperty.PropertyType.IsClass && targetProperty.PropertyType.Name != "String")
                    || targetProperty.PropertyType.IsArray
                    || targetProperty.PropertyType.IsInterface)
                    continue;

                var sourceProperty = sourceProperties.SingleOrDefault(c => c.Name == targetProperty.Name);


                if (sourceProperty == null) continue;

                if (sourceProperty.CanRead == false) continue;

                if (targetProperty.PropertyType != sourceProperty.PropertyType) continue;


                var value = sourceProperty.GetValue(source, null);

                targetProperty.SetValue(target, value, null);


            }

            return target;
        }



    }
}
