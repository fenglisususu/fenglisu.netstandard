﻿using System;
using System.IO;
using System.Text;

namespace Core.Extensions
{
    /// <summary>
    /// 内存流辅助类
    /// </summary>
    public static class MemoryStreamHelper
    {
        private static readonly UTF8Encoding Utf8Encoding = new UTF8Encoding();

        /// <summary>
        /// 转换为内存流
        /// </summary>
        /// <param name="value">字符串值</param>
        /// <returns>结果</returns>
        public static MemoryStream ToMemoryStream(this string value)
        {
            try
            {
                return new MemoryStream(Utf8Encoding.GetBytes(value));

            }
            catch (Exception e)
            {
                //Logger.Write(string.Format("string To MemoryStream Error ,Exception {0}", e.Message));
                throw;
            }

        }

    }
}
