﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Extensions
{
    public static class GuidHelper
    {
        public static readonly System.Guid SystemGuid = new System.Guid("11111111-1111-1111-1111-111111111111");

        public static string ToString(this List<Guid> list)
        {

            return String.Concat(list.Select(s => s.ToString() + ",")).TrimEnd(',');
        }
    }
}
