﻿using System;
using System.IO;

namespace Core.Extensions
{

    /// <summary>
    /// 文件辅助类
    /// </summary>
    public class FileHelper
    {
        public String Read(string path)
        {

            var streamReader = new StreamReader(path);

            var content = streamReader.ReadToEnd();

            streamReader.Close();

            return content;

        }

        public bool Writer(string path, String content)
        {
            var fileStream = new FileStream(path, FileMode.OpenOrCreate);

            var streamWriter = new StreamWriter(fileStream);

            streamWriter.Write(content);

            streamWriter.Flush();

            streamWriter.Close();

            fileStream.Close();

            return true;
        }


    }
}
