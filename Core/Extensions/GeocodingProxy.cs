﻿using System;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

//using Aspirin.Extensions.Helper;

namespace Core.Extensions
{
    /// <summary>
    /// 经纬度代理器
    /// </summary>
    public class GeocodingProxy
    {
        private static Encoding _encode = null;

        public GeocodingProxy()
        {
            if (_encode == null) LoadingEncoding();
        }

        /// <summary>
        /// 通过百度经纬度坐标获取对应的地址信息
        /// </summary>
        /// <param name="latitudeAndlongitude"></param>
        /// <returns></returns>
        public string GetAddress(string latitudeAndlongitude)
        {
            var url = "http://api.map.baidu.com/geocoder/v2/?ak=6WzGgPALgpIABDhCmjjyMNxq&location=" + latitudeAndlongitude + "&output=json&pois=0";
            var value = Download(url, 10, _encode);
            //value	"{\"status\":0,\"result\":{\"location\":{\"lng\":111.31199598367,\"lat\":23.475699985555},\"formatted_address\":\"广西壮族自治区梧州市蝶山区西堤一路24号\",\"business\":\"\",\"addressComponent\":{\"city\":\"梧州市\",\"district\":\"蝶山区\",\"province\":\"广西壮族自治区\",\"street\":\"西堤一路\",\"street_number\":\"24号\"},\"cityCode\":304}}"	string

            return value.Replace("\"", "").Replace(":", "").Replace(",", "").ToBetweenValue("formatted_address", "business");
        }




        /// <summary>
        /// 加载地图网站编码
        /// </summary>
        private void LoadingEncoding()
        {
            try
            {
                var url = "http://api.map.baidu.com/geocoder/v2/?ak=6WzGgPALgpIABDhCmjjyMNxq&location=23.475700,111.311996&output=json&pois=0";

                string myPage = Download(url, 10, Encoding.Default);

                Match m = Regex.Match(myPage, @"(?<=charset=("")?)[^""]+(?="")");

                _encode = m.Success ? Encoding.GetEncoding(m.Value) : Encoding.UTF8;
            }
            catch (Exception)
            {
            }
        }



        /// <summary>
        /// 下载html并转换为字符串 
        /// </summary>
        /// <param name="url">url路径</param>
        /// <param name="Timeout">超时时间</param>
        /// <param name="MyEncoding">编码方式</param>
        /// <returns></returns>
        private string Download(string url, int Timeout, Encoding MyEncoding)
        {
            try
            {
                var webRequest = (HttpWebRequest)WebRequest.Create(url);

                webRequest.Timeout = Timeout * 1000;

                var httpWebResponse = (HttpWebResponse)webRequest.GetResponse();

                Stream stream = httpWebResponse.GetResponseStream();

                // 如果要下载的页面经过压缩，则先解压
                if (httpWebResponse.ContentEncoding.ToLower().IndexOf("gzip") >= 0)
                {
                    stream = new GZipStream(stream, CompressionMode.Decompress);
                }

                if (MyEncoding == null)
                {
                    MyEncoding = Encoding.Default;
                }

                var sReader = new StreamReader(stream, MyEncoding);

                return sReader.ReadToEnd();
            }
            catch (Exception)
            {
                return string.Empty;

            }


        }
    }
}
