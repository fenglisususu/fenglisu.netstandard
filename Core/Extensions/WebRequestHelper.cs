﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace Core.Extensions
{

    /// <summary>
    /// Web请求辅助类
    /// </summary>
    public static class WebRequestHelper
    {

        /// <summary>
        /// post数据到服务器
        /// 默认编码是utf-8
        /// </summary>
        /// <param name="url">服务器路径</param>
        /// <param name="param">参数：格式为：hl=zh-CN&newwindow=1&q=HttpWebClient</param>
        /// <returns>post返回的值</returns>
        public static string Post(this string url, string param)
        {
            // http 100-continue用于客户端在发送POST数据给服务器前，征询服务器情况，看服务器是否处理POST的数据，如果不处理，客户端则不上传POST数据，如果处理，则POST上传数据。在现实应用中，通过在POST大数据时，才会使用100-continue协议。
            //2、客户端策略。
            //  1）如果客户端有POST数据要上传，可以考虑使用100-continue协议。加入头{"Expect":"100-continue"}
            //  2）如果没有POST数据，不能使用100-continue协议，因为这会让服务端造成误解。
            //  3）并不是所有的Server都会正确实现100-continue协议，如果Client发送Expect:100-continue消息后，在timeout时间内无响应，Client需要立马上传POST数据。
            //  4）有些Server会错误实现100-continue协议，在不需要此协议时返回100，此时客户端应该忽略。
            //3、服务端策略。
            //  1）正确情况下，收到请求后，返回100或错误码。
            //  2）如果在发送100-continue前收到了POST数据（客户端提前发送POST数据），则不发送100响应码(略去)。

            string result = null;
            try
            {
               // Logger.Write(string.Format("url:{0},param:{1}", url, param), "Post");
                url = url.Replace("//", "/").Replace(":/", "://");
                var paramByteArray = Encoding.UTF8.GetBytes(param); //转化
                var urlobj = new Uri(url);
                var webReq = (HttpWebRequest)WebRequest.Create(urlobj);

                webReq.Method = "POST";
                webReq.ContentType = "application/x-www-form-urlencoded";
                webReq.ContentLength = paramByteArray.Length;
                //var skey = SysContext.Current.CurrentUser.ToJson();
                //webReq.Headers.Add("skey", skey.DesEncrypt());
                // webReq.Headers.Add("ASP.NET_SessionId", SysContext.SessionId);

                //var cookieContainer = new CookieContainer();

                var cookieContainer = new CookieContainer();
               // var cookie = new Cookie("ASP.NET_SessionId", SysContext.SessionId);
                //cookie.Path = "/";
                //cookie.Domain = urlobj.Host;
               // cookieContainer.Add(urlobj, cookie);

                webReq.CookieContainer = cookieContainer;

                Stream newStream = webReq.GetRequestStream();
                newStream.Write(paramByteArray, 0, paramByteArray.Length);//写入参数
                newStream.Close();
                var response = (HttpWebResponse)webReq.GetResponse();
                var sr = new StreamReader(stream: response.GetResponseStream(), encoding: Encoding.UTF8);
                result = sr.ReadToEnd();
                sr.Close();
                response.Close();
                //Logger.Write(string.Format("url:{0},param:{1},result:{2}", url, param, result), "Post");
            }
            catch (Exception ex)
            {
               // Logger.Write(ex.Message, "Post", ex);
              //  result = new Result(ResultType.Error, ex.Message).ToJson();
            }
            return result;
        }

                /// <summary>
        /// post数据到服务器
        /// 默认编码是utf-8
        /// </summary>
        /// <param name="url">服务器路径</param>
        /// <param name="param">参数：格式为：hl=zh-CN&newwindow=1&q=HttpWebClient</param>
        /// <returns>post返回的值</returns>
        public static string PostJson(this string url, string param)
        {
            // http 100-continue用于客户端在发送POST数据给服务器前，征询服务器情况，看服务器是否处理POST的数据，如果不处理，客户端则不上传POST数据，如果处理，则POST上传数据。在现实应用中，通过在POST大数据时，才会使用100-continue协议。
            //2、客户端策略。
            //  1）如果客户端有POST数据要上传，可以考虑使用100-continue协议。加入头{"Expect":"100-continue"}
            //  2）如果没有POST数据，不能使用100-continue协议，因为这会让服务端造成误解。
            //  3）并不是所有的Server都会正确实现100-continue协议，如果Client发送Expect:100-continue消息后，在timeout时间内无响应，Client需要立马上传POST数据。
            //  4）有些Server会错误实现100-continue协议，在不需要此协议时返回100，此时客户端应该忽略。
            //3、服务端策略。
            //  1）正确情况下，收到请求后，返回100或错误码。
            //  2）如果在发送100-continue前收到了POST数据（客户端提前发送POST数据），则不发送100响应码(略去)。

            string result = null;
            try
            {
               // Logger.Write(string.Format("url:{0},param:{1}", url, param), "Post");
               
                var paramByteArray = Encoding.UTF8.GetBytes(param); //转化
                var urlobj = new Uri(url);
                var webReq = (HttpWebRequest)WebRequest.Create(urlobj);

                webReq.Method = "POST";
                webReq.ContentType = "application/json";
                webReq.ContentLength = paramByteArray.Length;
                //var skey = SysContext.Current.CurrentUser.ToJson();
                //webReq.Headers.Add("skey", skey.DesEncrypt());
                // webReq.Headers.Add("ASP.NET_SessionId", SysContext.SessionId);

                //var cookieContainer = new CookieContainer();

                var cookieContainer = new CookieContainer();
               // var cookie = new Cookie("ASP.NET_SessionId", SysContext.SessionId);
                //cookie.Path = "/";
                //cookie.Domain = urlobj.Host;
               // cookieContainer.Add(urlobj, cookie);

                webReq.CookieContainer = cookieContainer;

                Stream newStream = webReq.GetRequestStream();
                newStream.Write(paramByteArray, 0, paramByteArray.Length);//写入参数
                newStream.Close();
                var response = (HttpWebResponse)webReq.GetResponse();
                var sr = new StreamReader(stream: response.GetResponseStream(), encoding: Encoding.UTF8);
                result = sr.ReadToEnd();
                sr.Close();
                response.Close();
                //Logger.Write(string.Format("url:{0},param:{1},result:{2}", url, param, result), "Post");
            }
            catch (Exception ex)
            {
               // Logger.Write(ex.Message, "Post", ex);
              //  result = new Result(ResultType.Error, ex.Message).ToJson();
            }
            return result;
        }

   
    
        /// <summary>
        /// Get数据
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string Get(this string url)
        {
            var request = HttpWebRequest.Create(url) as HttpWebRequest;

            var response = request.GetResponse() as HttpWebResponse;

            var resStream = response.GetResponseStream();

            var sr = new StreamReader(resStream, System.Text.Encoding.UTF8);

            return sr.ReadToEnd();

        }

    }
}
