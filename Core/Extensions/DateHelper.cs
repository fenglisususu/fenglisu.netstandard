﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Text;

namespace Core.Extensions
{
    public static class DateHelper
    {
        public static DateTime Max(DateTime date1, DateTime date2)
        {
            if (date1.CompareTo(date2) > 0)
            {
                return date1;
            }
            return date2;
        }
        public static DateTime Min(DateTime date1, DateTime date2)
        {
            if (date1.CompareTo(date2) < 0)
            {
                return date1;
            }
            return date2;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <param name="displayMode">1 For Day,2 For Week ,3 For Month ,4 For Season ,5 For Year</param>
        /// <returns></returns>
        public static List<FromTo> SplitDate(DateTime beginTime, DateTime endTime, int displayMode)
        {
            beginTime = beginTime.Date;
            endTime = endTime.Date;

            var data = new List<FromTo>();
            switch (displayMode)
            {
                //日
                case 1:
                    var days = (endTime - beginTime).TotalDays;
                    for (int i = 0; i <= days; i++)
                    {
                        data.Add(new FromTo
                        {
                            From = Max(beginTime.Date.AddDays(i), beginTime),
                            To = Min(beginTime.AddDays(i), endTime),
                        });
                    }
                    break;
                //周
                case 2:
                    var weeks = (endTime - beginTime).TotalDays / 7;
                    var firstWDay = beginTime.Date.AddDays(((int)beginTime.DayOfWeek) * -1);
                    var lastWDay = endTime.Date.AddDays(7 - (int)endTime.DayOfWeek - 1);
                    weeks = (lastWDay - firstWDay).TotalDays / 7;
                    for (int i = 0; i < weeks; i++)
                    {
                        data.Add(new FromTo
                        {
                            From = Max(firstWDay.AddDays(i * 7), beginTime),
                            To = Min(firstWDay.AddDays((i + 1) * 7 - 1), endTime),
                        });

                    }
                    break;
                //月
                case 3:
                    var months = (endTime.Year - beginTime.Year) * 12 + (endTime.Month - beginTime.Month) + 1;
                    var firstMDay = beginTime.Date.AddDays((beginTime.Day - 1) * -1);
                    for (int i = 0; i < months; i++)
                    {
                        data.Add(new FromTo
                        {
                            From = Max(firstMDay.AddMonths(i), beginTime),
                            To = Min(firstMDay.AddMonths(i + 1).AddDays(-1), endTime),
                        });
                    }
                    break;
                //季
                case 4:
                    var seasons = (endTime.Year - beginTime.Year) * 4 + (endTime.Month - beginTime.Month) / 3 + 1;
                    var firstSDay = beginTime.Date.AddDays((beginTime.Day - 1) * -1).AddMonths((beginTime.Month % 3 - 1) * -1);
                    for (int i = 0; i < seasons; i++)
                    {
                        data.Add(new FromTo
                        {
                            From = Max(firstSDay.AddMonths(i * 3), beginTime),
                            To = Min(firstSDay.AddMonths((i + 1) * 3).AddDays(-1), endTime),
                        });
                    }
                    break;
                //年
                case 5:
                    var years = (endTime.Year - beginTime.Year) + 1;
                    var firstYDay = beginTime.Date.AddDays((beginTime.DayOfYear - 1) * -1);
                    for (int i = 0; i < years; i++)
                    {
                        data.Add(new FromTo
                        {
                            From = Max(firstYDay.AddYears(i), beginTime),
                            To = Min(firstYDay.AddYears(i + 1).AddDays(-1), endTime),
                        });
                    }
                    break;

            }

            return data;
        }
        public static bool IsNullOrEmpty(this DateTime? dateTime)
        {
            return !dateTime.HasValue || dateTime.Value == new DateTime(1901, 1, 1) ||
                   dateTime.Value == DateTime.MinValue ||
                   dateTime.Value == SqlDateTime.MinValue.Value;
        }
        public static bool IsEmpty(this DateTime dateTime)
        {
            return dateTime == new DateTime(1901, 1, 1) ||
                   dateTime == DateTime.MinValue ||
                   dateTime == SqlDateTime.MinValue.Value;
        }

        public static string ToChineseString(this TimeSpan span)
        {
            var sb = new StringBuilder();
            if (span.Days > 0) sb.AppendFormat("{0}天", span.Days);
            if (span.Hours > 0) sb.AppendFormat("{0}小时", span.Hours);
            if (span.Minutes > 0) sb.AppendFormat("{0}分", span.Minutes);
            if (span.Days == 0 && span.Hours == 0 && span.Seconds > 0) sb.AppendFormat("{0}秒", span.Seconds);

            return sb.ToString();
        }

        public static string ToChineseString(this TimeSpan? span)
        {
            return span.GetValueOrDefault().ToChineseString();
        }

        public static string ToDateString(this DateTime date, string format = "yyyy-MM-dd", string nullText = "")
        {
            if (date == DateTime.MinValue || date == DateTime.MaxValue) return nullText;
            return date.ToString(format);

        }

        public static string ToDateString(this DateTime? date, string format = "yyyy-MM-dd", string nullText = "")
        {
            if (date.HasValue) return date.Value.ToString(format);
            return nullText;
        }

        public static string ToDateTimeString(this DateTime date, string format = "yyyy-MM-dd HH:mm:ss")
        {
            if (date == DateTime.MinValue || date == DateTime.MaxValue) return string.Empty;
            return date.ToString(format);
        }

        public static string ToDateTimeString(this DateTime? date, string format = "yyyy-MM-dd HH:mm:ss")
        {
            if (date.HasValue) return date.Value.ToString(format);
            return string.Empty;
        }

        /// <summary>  
        /// 时间格式转换为Unix时间戳格式  
        /// </summary>  
        /// <param name="time">时间</param>  
        /// <returns>long</returns>  
        public static long ConvertToInt(this DateTime time)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1, 0, 0, 0, 0));
            long t = (time.Ticks - startTime.Ticks) / 10000;   //除10000调整为13位      
            return t;
        }
    }
    public class FromTo
    {
        public DateTime From { get; set; }

        private DateTime to;
        public DateTime To
        {
            get { return to.Date.AddDays(1).AddMilliseconds(-3); }
            set { to = value; }
        }
    }
}
