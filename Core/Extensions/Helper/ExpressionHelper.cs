﻿/*
* ----------------------------------------------------------------------------
* Copyright (c)  AspirinDT
* 创建人:Elices
* 日期:2013-10-27
* 修改人: 
* 日期:
* 描述: 动态表达式辅助类
*
* 版本:1.0
*-----------------------------------------------------------------------------
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Core.Extensions.Helper
{
    /// <summary>
    /// 动态表达式辅助类
    /// </summary>
    public static class ExpressionHelper
    {
        /// <summary>
        /// 参数表达式缓存
        /// </summary>
        private static readonly Dictionary<Type, ParameterExpression> TypeParamExpMap = new Dictionary<Type, ParameterExpression>();




        /// <summary>
        /// 并且
        /// </summary>
        /// <typeparam name="T">实体类泛型</typeparam>
        /// <param name="a">条件A</param>
        /// <param name="b">条件B</param>
        /// <returns>条件A 且 条件B 的结果</returns>
        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> a, Expression<Func<T, bool>> b)
        {
            if (a == null && b != null) return b;
            var tp = a.Parameters.FirstOrDefault(c => c.Type == typeof(T));
            return b == null ? a : Expression.Lambda<Func<T, bool>>(Expression.AndAlso(a.Body, b.Body), tp ?? GetParameterByType(typeof(T)));//老的方法改成新的了以便支持强类型写法

            //return b == null ? a : a.Compose(b, Expression.Add);
        }

        /// <summary>
        /// 或
        /// </summary>
        /// <typeparam name="T">实体类泛型</typeparam>
        /// <param name="a">条件A</param>
        /// <param name="b">条件B</param>
        /// <returns>条件A 或 条件B 的结果</returns>
        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> a, Expression<Func<T, bool>> b)
        {
            if (a == null && b != null) return b;
            var tp = a.Parameters.FirstOrDefault(c => c.Type == typeof(T));

            return b == null ? a : Expression.Lambda<Func<T, bool>>(Expression.OrElse(a.Body, b.Body), tp ?? GetParameterByType(typeof(T)));//老的方法改成新的了以便支持强类型写法
            //return b == null ? a : a.Compose(b, Expression.Or);
        }

        // public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> first, Expression<Func<T, bool>> second)
        //{
        //    return first.Compose(second, Expression.Or);
        //}

        /// <summary>
        /// 合并两个表达式树
        /// </summary>
        /// <typeparam name="T"><peparam>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public static Expression<T> Compose<T>(this Expression<T> first, Expression<T> second, Func<Expression, Expression, Expression> merge)
        {
            // build parameter map (from parameters of second to parameters of first)
            var map = first.Parameters.Select((f, i) => new { f, s = second.Parameters[i] }).ToDictionary(p => p.s, p => p.f);

            // replace parameters in the second lambda expression with parameters from the first
            var secondBody = ParameterRebinder.ReplaceParameters(map, second.Body);

            // apply composition of lambda expression bodies to parameters from the first expression 
            return Expression.Lambda<T>(merge(first.Body, secondBody), first.Parameters);
        }


        /// <summary>
        /// 表达式连接操作
        /// </summary>
        /// <typeparam name="T">实体类泛型</typeparam>
        /// <param name="a">表达式a</param>
        /// <param name="b">表达式b</param>
        /// <param name="operation">操作字符串</param>
        /// <exception cref="Exception">操作付异常描述</exception>
        /// <returns></returns>
        public static Expression<Func<T, bool>> Operation<T>(this Expression<Func<T, bool>> a, Expression<Func<T, bool>> b, string operation)
        {
            switch (operation)
            {
                case "And":
                    return And(a, b);
                case "Or":
                    return Or(a, b);
                default:
                    throw new Exception("operation Exception");
            }
        }



        /// <summary>
        /// 非
        /// </summary>
        /// <typeparam name="T">实体类泛型</typeparam>
        /// <param name="a">条件A</param>
        /// <returns>非条件A 的结果</returns>
        public static Expression<Func<T, bool>> Not<T>(this Expression<Func<T, bool>> a)
        {
            var tp = a.Parameters.FirstOrDefault(c => c.Type == typeof(T));
            return Expression.Lambda<Func<T, bool>>(Expression.Not(a.Body), tp ?? GetParameterByType(typeof(T)));
        }

        /// <summary>
        /// 相等
        /// </summary>
        /// <typeparam name="T">实体类泛型</typeparam>
        /// <param name="columnName">数据库字段名</param>
        /// <param name="val">与字段比较的值</param>
        /// <returns>是否相等</returns>
        public static Expression<Func<T, bool>> Equal<T>(string columnName, object val)
        {
            return BinaryLogic<T>(Expression.Equal, columnName, val);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="columnName"></param>
        /// <param name="columnName2"></param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> EqualEextend<T>(string columnName, string columnName2)
        {
            return BinaryLogicEextend<T>(Expression.Equal, columnName, columnName2);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="columnName"></param>
        /// <param name="columnName2"></param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> NotEqualEextend<T>(string columnName, string columnName2)
        {
            return BinaryLogicEextend<T>(Expression.NotEqual, columnName, columnName2);
        }

        /// <summary>
        /// 不相等
        /// </summary>
        /// <typeparam name="T">实体类泛型</typeparam>
        /// <param name="columnName">数据库字段名</param>
        /// <param name="val">与字段比较的值</param>
        /// <returns>是否不相等</returns>
        public static Expression<Func<T, bool>> NotEqual<T>(string columnName, object val)
        {
            return BinaryLogic<T>(Expression.NotEqual, columnName, val);
        }

        /// <summary>
        /// 小于
        /// </summary>
        /// <typeparam name="T">实体类泛型</typeparam>
        /// <param name="columnName">数据库字段名</param>
        /// <param name="val">与字段比较的值</param>
        /// <returns>是否小于</returns>
        public static Expression<Func<T, bool>> LessThan<T>(string columnName, object val)
        {
            return BinaryLogic<T>(Expression.LessThan, columnName, val);
        }

        /// <summary>
        /// 小于等于
        /// </summary>
        /// <typeparam name="T">实体类泛型</typeparam>
        /// <param name="columnName">数据库字段名</param>
        /// <param name="val">与字段比较的值</param>
        /// <returns>是否小于等于</returns>
        public static Expression<Func<T, bool>> LessThanOrEqual<T>(string columnName, object val)
        {
            return BinaryLogic<T>(Expression.LessThanOrEqual, columnName, val);
        }

        /// <summary>
        /// 大于
        /// </summary>
        /// <typeparam name="T">实体类泛型</typeparam>
        /// <param name="columnName">数据库字段名</param>
        /// <param name="val">与字段比较的值</param>
        /// <returns>是否大于</returns>
        public static Expression<Func<T, bool>> GreaterThan<T>(string columnName, object val)
        {
            return BinaryLogic<T>(Expression.GreaterThan, columnName, val);
        }

        /// <summary>
        /// 大于等于
        /// </summary>
        /// <typeparam name="T">实体类泛型</typeparam>
        /// <param name="columnName">数据库字段名</param>
        /// <param name="val">与字段比较的值</param>
        /// <returns>是否大于等于</returns>
        public static Expression<Func<T, bool>> GreaterThanOrEqual<T>(string columnName, object val)
        {
            return BinaryLogic<T>(Expression.GreaterThanOrEqual, columnName, val);
        }

        public static Expression<Func<T, bool>> Contains<T>(string columnName, object val)
        {
            return Operation<T>(columnName, val, "Contains");
        }


        /// <summary>
        /// 构建单个表达式操作
        /// </summary>
        /// <typeparam name="T">实体类</typeparam>
        /// <param name="columnName">列名</param>
        /// <param name="val">值</param>
        /// <param name="operation">操作字符串：支持==、!=、>、>=、< 、<= 、Contains</param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> Operation<T>(string columnName, object val, string operation, string name = "p")
        {
            switch (operation)
            {
                case "==":
                    return BinaryLogic<T>(Expression.Equal, columnName, val);
                case "!=":
                    return BinaryLogic<T>(Expression.NotEqual, columnName, val);
                case ">":
                    return BinaryLogic<T>(Expression.GreaterThan, columnName, val);
                case ">=":
                    return BinaryLogic<T>(Expression.GreaterThanOrEqual, columnName, val);
                case "<":
                    return BinaryLogic<T>(Expression.LessThan, columnName, val);
                case "<=":
                    return BinaryLogic<T>(Expression.LessThanOrEqual, columnName, val);
                case "Contains":
                    try
                    {
                        return Call<T>(columnName, "Contains", name, val);
                    }
                    catch (Exception)
                    {
                        return BinaryLogic<T>(Expression.Equal, columnName, val);
                    }
                   
                case "In":
                    if (val is string)
                    {
                        var listval = val.ToStringOrEmpty().Split(',').ToList();
                        return In<T>(columnName, listval);
                    }
                    else if (val is List<Guid>)
                    {
                        var listval = val as List<Guid>;
                        return In<T>(columnName, listval);
                    }
                    else if (val is List<Guid?>)
                    {
                        var listval = val as List<Guid?>;
                        return In<T>(columnName, listval);
                    }
                    return In<T>(columnName, val);
                default:
                    throw new Exception("operation Exception");
            }
        }

        /// <summary>
        /// 排序表达式
        /// </summary>
        /// <typeparam name="T">实体类</typeparam>
        /// <typeparam name="TKey">排序字段属性</typeparam>
        /// <param name="property">排序属性名称</param>
        /// <returns></returns>
        public static Expression<Func<T, TKey>> OrderBy<T, TKey>(string property, string name = "p")
        {
            var type = typeof(T);

            var parameter = GetParameterByType(type, name);
            var propertyAccess = Expression.MakeMemberAccess(parameter, type.GetProperty(property));
            return (Expression<Func<T, TKey>>)Expression.Lambda(propertyAccess, parameter);

        }

        /// <returns></returns>
        //public static Expression<Func<T, TKey>> OrderBy<T>(string property)
        //{
        //    var type = typeof(T);

        //    var parameter = GetParameterByType(type);

        //    type.GetProperty(property).GetType()
        //    var propertyAccess = Expression.MakeMemberAccess(parameter, type.GetProperty(property));
        //    return (Expression<Func<T>>)Expression.Lambda(propertyAccess, parameter);

        //}



        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="logic"></param>
        /// <param name="columnName"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        private static Expression<Func<T, bool>> BinaryLogic<T>(Func<Expression, Expression, BinaryExpression> logic, string columnName, object val, string name = "p")
        {
            var entityType = typeof(T);
            var p1 = GetParameterByType(entityType, name);
            var propertyType = entityType.GetProperty(columnName);
            var ma = Expression.MakeMemberAccess(p1, propertyType);
            var eq = logic(ma, Expression.Constant(val, propertyType.PropertyType));
            return (Expression<Func<T, bool>>)Expression.Lambda(eq, new[] { p1 });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="logic"></param>
        /// <param name="columnName"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        private static Expression<Func<T, bool>> BinaryLogicEextend<T>(Func<Expression, Expression, BinaryExpression> logic, string columnName, string columnName2, string name = "p")
        {
            var entityType = typeof(T);
            var p1 = GetParameterByType(entityType, name);
            var propertyType = entityType.GetProperty(columnName);
            var propertyType2 = entityType.GetProperty(columnName2);

            var ma = Expression.MakeMemberAccess(p1, propertyType);
            var ma2 = Expression.MakeMemberAccess(p1, propertyType2);


            var eq = logic(ma, ma2);
            return (Expression<Func<T, bool>>)Expression.Lambda(eq, new[] { p1 });
        }


        /// <summary>
        /// 函数调用，例如 Contains
        /// </summary>
        /// <typeparam name="T">实体类泛型</typeparam>
        /// <param name="columnName">数据库字段名</param>
        /// <param name="methodName">需要调用的方法名</param>
        /// <param name="args">调用方法时需要的参数</param>
        /// <returns>调用方法的返回值</returns>
        public static Expression<Func<T, bool>> Call<T>(string columnName, string methodName, string name = "p", params object[] args)
        {
            //Logger.Write(string.Format("Expression call columnName:{0},methodName:{1}", columnName, methodName));

            var entityType = typeof(T);
            var p1 = GetParameterByType(entityType, name);
            var ma = Expression.MakeMemberAccess(p1, entityType.GetProperty(columnName));
            var call = Expression.Call(ma, ma.Type.GetMethod(methodName),
                args.Select(a => (Expression)Expression.Constant(a)));
            return (Expression<Func<T, bool>>)Expression.Lambda(call, new[] { p1 });


        }
        public static Expression<Func<T, bool>> In<T>(string columnName, object args, string name = "p")
        {
            //Logger.Write(string.Format("Expression call columnName:{0},methodName:{1}", columnName, methodName));

            var entityType = typeof(T);
            var p1 = GetParameterByType(entityType, name);
            var ma = Expression.MakeMemberAccess(p1, entityType.GetProperty(columnName));
            var call = Expression.Call(Expression.Constant(args), args.GetType().GetMethod("Contains"), ma);
            return (Expression<Func<T, bool>>)Expression.Lambda(call, new[] { p1 });


        }

        /// <summary>
        /// 获取参数表达式
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        private static ParameterExpression GetParameterByType(Type t, string name = "p")
        {
            ParameterExpression cached;
            if (TypeParamExpMap.TryGetValue(t, out cached)) return cached;
            return TypeParamExpMap[t] = Expression.Parameter(t, name);
        }





    }


    /// <summary>
    /// 
    /// </summary>
    public class ParameterRebinder : ExpressionVisitor
    {
        private readonly Dictionary<ParameterExpression, ParameterExpression> map;

        public ParameterRebinder(Dictionary<ParameterExpression, ParameterExpression> map)
        {
            this.map = map ?? new Dictionary<ParameterExpression, ParameterExpression>();
        }

        public static Expression ReplaceParameters(Dictionary<ParameterExpression, ParameterExpression> map, Expression exp)
        {
            return new ParameterRebinder(map).Visit(exp);
        }

        protected override Expression VisitParameter(ParameterExpression p)
        {
            ParameterExpression replacement;
            if (map.TryGetValue(p, out replacement))
            {
                p = replacement;
            }
            return base.VisitParameter(p);
        }
    }
}
