﻿using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace Core.Extensions
{
    public static class NetworkHelper
    {
        public static List<string> GetLocalAddresses()
        {
            var localIpAddresses = new List<string>();
            foreach (var networkInterface in NetworkInterface.GetAllNetworkInterfaces().Where(i => i.NetworkInterfaceType == NetworkInterfaceType.Ethernet || i.NetworkInterfaceType == NetworkInterfaceType.Wireless80211))
            {
                var unicastAddresses = networkInterface.GetIPProperties().UnicastAddresses.Where(i => i.Address.AddressFamily == AddressFamily.InterNetwork && i.Address.ToString().StartsWith("192."));
                localIpAddresses.AddRange(unicastAddresses.Select(i => i.Address.ToString()));
            }

            return localIpAddresses;
        }
    }
}
