﻿using Microsoft.Extensions.DependencyInjection;

namespace Core.Services
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddService<T>(this IServiceCollection services) where T : class, IService
        {
            return services.AddTransient(provider =>
            {
                var clientFactory = provider.GetRequiredService<IClientFactory>();
                return clientFactory.Create<T>().GetAwaiter().GetResult();
            });
        }
    }
}
