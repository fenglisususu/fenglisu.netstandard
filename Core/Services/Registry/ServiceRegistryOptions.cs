﻿namespace Core.Services.Registry
{
    public class ServiceRegistryOptions
    {
        public string ServiceName { get; set; }
        public string[] ServiceEndpoints { get; set; }
        public int ServicePort { get; set; }
        public string HealthCheckUrl { get; set; }
    }
}
