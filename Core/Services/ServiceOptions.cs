﻿namespace Core.Services
{
    public class ServiceOptions
    {
        public LoadBalancers LoadBalancer { get; set; }
    }

    public enum LoadBalancers
    {
        /// <summary>
        /// 随机
        /// </summary>
        Random,
        /// <summary>
        /// 轮询
        /// </summary>
        RoundRobin,
        /// <summary>
        /// 本地优先
        /// </summary>
        LocalFirst
    }
}
