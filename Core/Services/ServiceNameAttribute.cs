﻿using System;

namespace Core.Services
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface, Inherited = false)]
    public class ServiceNameAttribute : Attribute
    {
        private string _name;

        public string Name { get { return _name; } }

        public ServiceNameAttribute(string name)
        {
            _name = name;
        }
    }
}
