﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Core.Extensions;
using Core.Services.Registry;

namespace Core.Services.LoadBalancer
{
    public class LocalFirstLoadBalancer : ILoadBalancer
    {
        private readonly Random _random = new Random();

        public Task<ServiceInformation> Select(IList<ServiceInformation> services)
        {
            if (services == null || services.Count == 0) return null;

            var localIpAddresses = NetworkHelper.GetLocalAddresses();

            var localService = services.FirstOrDefault(i => localIpAddresses.Contains(i.Address));
            if (localService != null) return Task.FromResult(localService);

            return Task.FromResult(services[_random.Next(services.Count)]);
        }
    }
}
