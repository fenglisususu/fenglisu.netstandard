﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Services.Registry;

namespace Core.Services.LoadBalancer
{
    public interface ILoadBalancer
    {
        Task<ServiceInformation> Select(IList<ServiceInformation> services);
    }
}
