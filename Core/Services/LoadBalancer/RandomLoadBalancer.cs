﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Services.Registry;

namespace Core.Services.LoadBalancer
{
    public class RandomLoadBalancer : ILoadBalancer
    {
        private readonly Random _random = new Random();

        public Task<ServiceInformation> Select(IList<ServiceInformation> services)
        {
            return Task.FromResult(services == null || services.Count == 0 ? null : services[_random.Next(services.Count)]);
        }
    }
}
