﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Core.Services.Registry;

namespace Core.Services.LoadBalancer
{
    public class RoundRobinLoadBalancer : ILoadBalancer
    {
        private readonly SemaphoreSlim _lock = new SemaphoreSlim(1, 1);
        private int _index;

        public async Task<ServiceInformation> Select(IList<ServiceInformation> services)
        {
            if (services == null || services.Count == 0)
                return null;

            await _lock.WaitAsync().ConfigureAwait(false);
            try
            {
                if (_index >= services.Count)
                    _index = 0;
                var service = services[_index];
                _index++;

                return service;
            }
            finally
            {
                _lock.Release();
            }
        }
    }
}
