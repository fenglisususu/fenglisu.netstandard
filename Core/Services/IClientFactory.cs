﻿using System.Threading.Tasks;

namespace Core.Services
{
    public interface IClientFactory
    {
        Task<T> Create<T>() where T : IService;
    }
}
