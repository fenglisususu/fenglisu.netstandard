﻿using System;

namespace Core.Models
{
    public class BaseGuidModel
    {
        public Guid Id { get; set; }
    }
}
