﻿using System.Collections.Generic;

namespace Core.Models
{
    /// <summary>
    /// 查询参数对象
    /// </summary>
    public class QueryParam
    {
        public QueryParam()
        {
            _offset = 0;
            _count = 20;
            _pageIndex = 1;
            _pageSize = 20;
            Conditions = new Dictionary<string, object>();

        }
        private int _count;
        /// <summary>
        /// 数量
        /// </summary>
        public int Count
        {
            get { return _count; }
            set
            {
                _count = value <= 0 ? 20 : value;
            }
        }

        private int _offset;
        /// <summary>
        /// 偏移
        /// </summary>
        public int Offset
        {
            get { return _offset; }
            set
            {
                _offset = value <= 0 ? 0 : value;
            }
        }

        private int _pageIndex;
        /// <summary>
        /// 页码
        /// </summary>
        public int PageIndex
        {
            get { return _pageIndex; }
            set
            {
                _pageIndex = value <= 0 ? 0 : value;
            }
        }
        private int _pageSize;

        /// <summary>
        /// 页面显示数量
        /// </summary> 
        public int PageSize
        {
            get { return _pageSize; }
            set
            {
                _pageSize = value <= 0 ? 20 : value;
            }
        }

        /// <summary>
        /// 排序字段
        /// </summary>
        public string sortField { get; set; } = "ModifyTime";

        /// <summary>
        /// 排序方式：Asc:递增，Des:递减
        /// </summary>
        public string sortType { get; set; } = "Desc";

        /// <summary>
        /// App请求数据模块
        /// </summary>
        public string AppModule { get; set; }

        /// <summary>
        /// 查询条件
        /// </summary>
        public Dictionary<string, object> Conditions { get; set; }

        public bool ConditionsExist(string key)
        {
            return Conditions != null && Conditions.ContainsKey(key);
        }

        //public int GetPageIndex()
        //{
        //    return PageIndex + 1;
        //}



        //public Aspirin.Api.IRepository.SortOrder GetSortOrder()
        //{
        //    return sortOrder != "asc" ? Toogps.IRepository.SortOrder.Descending : Toogps.IRepository.SortOrder.Ascending;
        //}
    }
}
