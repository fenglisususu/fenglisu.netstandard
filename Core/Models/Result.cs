﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Core.Models
{
    [DataContract]
    public class Result
    {
        public Result()
        {

        }

        public Result(bool sucess)
        {
            Success = sucess;
        }

        public Result(bool sucess, string msg)
        {
            Success = sucess;
            Msg = msg;

        }

        public Result(bool sucess, string msg, string errerCode)
        {
            Success = sucess;
            Msg = msg;
            ErrCode = errerCode;
        }


        /// <summary>
        /// 信息
        /// </summary>
        [DataMember]
        public string Msg { get; set; }

        /// <summary>
        /// 请求是否成功状态,true
        /// </summary>
        /// <value>true成功</value>
        /// <value>false失败</value>
        [DataMember]
        public bool Success { get; set; }


        /// <summary>
        /// 返回结果
        /// </summary>
        [DataMember]
        public object Model { get; set; }


        /// <summary>
        /// 错误码
        /// </summary>
        [DataMember]
        public string ErrCode { get; set; }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static Result OutputSuccess(object model = null, string msg = "")
        {
            return new Result() { Success = true, Msg = msg, Model = model };
        }

        public static Result OutputFail(string msg = "", string errCode = "", object model = null)
        {
            return new Result() { Success = false, Msg = msg, ErrCode = errCode, Model = model };
        }

        public static Result<T> OutputSuccess<T>(T model = default(T), string msg = "")
        {
            return new Result<T> {Success = true, Msg = msg, Model = model};
        }

        public static Result<T> OutputFail<T>(string msg = "", string errCode = "", T model = default(T))
        {
            return new Result<T> {Success = false, Msg = msg, ErrCode = errCode, Model = model};
        }
    }

    [DataContract]
    public class Result<T>
    {
        /// <summary>
        /// 信息
        /// </summary>
        [DataMember]
        public string Msg { get; set; }
        /// <summary>
        /// 请求是否成功状态,true
        /// </summary>
        /// <value>true成功</value>
        /// <value>false失败</value>
        [DataMember]
        public bool Success { get; set; }
        /// <summary>
        /// 返回结果
        /// </summary>
        [DataMember]
        public T Model { get; set; }
        /// <summary>
        /// 错误码
        /// </summary>
        [DataMember]
        public string ErrCode { get; set; }
    }


    /// <summary>
    /// 列表数据
    /// </summary>
    [DataContract]
    public class ListModel
    {
        public ListModel()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="total"></param>
        /// <param name="list"></param>
        public ListModel(int total, object list)
        {
            Total = total;
            List = list;
        }
        /// <summary>
        /// 返回结果
        /// </summary>
        [DataMember]
        public object List { get; set; }

        /// <summary>
        /// 总记录数
        /// </summary>
        [DataMember]
        public int Total { get; set; }
    }

    [DataContract]
    public class ListModel<T> : ListModel
    {
        [DataMember]
        public new List<T> List { get; set; }

        public ListModel(int total, List<T> list)
        {
            Total = total;
            List = list;
        }
    }
}
