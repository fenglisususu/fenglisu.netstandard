﻿using System.Collections.Generic;

namespace Core.Models
{
    public class StandardColor
    {
        public static string Black = "#000000";
        public static string Write = "#FFFFFF";
        public static string Red = "#FF0000";
        public static string Green = "#008000";

        /// <summary>
        /// 草绿色 	#7CFC00
        /// </summary>
        public static string LawnGreen = "#7CFC00";
    }

    public class InitQueryView
    {
        /// <summary>
        /// 快速搜索模块
        /// </summary>
        public List<string> QuickSearch { get; set; }

        /// <summary>
        /// 筛选字段
        /// </summary>
        public List<SearchField> Fields { get; set; }

        /// <summary>
        /// 排序规则
        /// </summary>
        //public List<Selection> SortBy { get; set; }
    }

    public class SearchField
    {
        /// <summary>
        /// 过滤标题（界面显示）
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 过滤字段（提交传值）
        /// </summary>
        public string Key { get; set; }
        /// <summary>
        /// 是否地区
        /// </summary>
        public bool IsArea { get; set; }
        /// <summary>
        /// 选项
        /// </summary>
        public List<Selection> Selections { get; set; }


    }

    public class Selection
    {
        /// <summary>
        /// 选项名称（界面显示）
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 选项值（提交传值）
        /// </summary>
        public object Value { get; set; }
        /// <summary>
        /// 级联选项
        /// </summary>
        public List<SearchField> Fields { get; set; }
    }

    /// <summary>
    /// 列表数据对象
    /// </summary>
    public class AppQueryView
    {
        /// <summary>
        /// 实例化
        /// </summary>
        public AppQueryView()
        {
            Items = new List<QueryItem>();
        }

        /// <summary>
        /// 实例化
        /// </summary>
        /// <param name="module"></param>
        public AppQueryView(string module)
            : this()
        {
            Module = module;
        }
        /// <summary>
        /// 数据模块
        /// </summary>
        public string Module { get; set; }

        /// <summary>
        /// 选择项数据
        /// </summary>
        public List<QueryItem> Items { get; set; }

    }

    /// <summary>
    /// 选择项
    /// </summary>
    public class QueryItem
    {
        /// <summary>
        /// 去从Id
        /// </summary>
        public string Id { get; set; }

        ///// <summary>
        ///// 图标（星级:0-5）
        ///// </summary>
        //public int Icon { get; set; }
        ///// <summary>
        ///// 行显示数据
        ///// </summary>
        //public List<QueryRow> Rows { get; set; }

        public M1 Data { get; set; }

        /// <summary>
        /// 点击事件
        /// </summary>
        public AppEvent Click { get; set; }
    }

    /// <summary>
    /// 第一代模板
    /// </summary>
    public class M1
    {
        public string Field1 { get; set; }
        public string Field2 { get; set; }
        public string Field3 { get; set; }
        public string Field4 { get; set; }
        public string Field5 { get; set; }
        public string Field6 { get; set; }
        public string Field7 { get; set; }
        public string Field8 { get; set; }
    }

 
    /// <summary>
    /// 事件内容
    /// </summary>
    public class AppEvent
    {
        /// <summary>
        /// 描述提示
        /// </summary>
        public string Tips { get; set; }

        /// <summary>
        /// 跳转模块
        /// </summary>
        public string RedirectModule { get; set; }

        /// <summary>
        /// 跳转参数
        /// </summary>
        public string Params { get; set; }

        /// <summary>
        /// 跳转参数
        /// </summary>
        public string Params2 { get; set; }
    }
}
