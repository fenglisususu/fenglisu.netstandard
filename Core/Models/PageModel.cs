﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models
{
    public class PageModel
    {
        private string sortType = "Desc";

        public string SortType
        {
            get { return sortType; }
            set { sortType = value; }
        }

        private string sortField = "ModifyTime";

        public string SortField
        {
            get { return sortField; }
            set { sortField = value; }
        }

        private int pageIndex = 1;

        public int PageIndex
        {
            get { return pageIndex; }
            set { pageIndex = value; }
        }

        private int pageSize = 20;

        public int PageSize
        {
            get { return pageSize; }
            set { pageSize = value; }
        }

    }
}
