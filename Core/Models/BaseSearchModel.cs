﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models
{
    public class BaseSearchModel : PageModel
    {
        public string SearchText { get; set; }
    }
}
